## TODOs
* !!ACHTUNG!! TapeR::E_DHx_HmDm_HT.f sortiert HX um: von klein nach groß!!!
** OK, it's a feature, not a bug
* helpfile tprAssortment bzw. parSort
* helpfile of tprTrees (can omit list() in Dm and Hm)
* maximale Stammholzlänge bei Laubholz anpassen
* check units in helpfiles and vignettes

* rBDATPRO::getDiameter in astprtrees (optional)
** done already, updated documentation to refer to rBDAT only
* in TapeR: pass R0 through E_HDx_HmDm_HT.f > Hx_root.f > E_DHx_HmDm_HT.f(, R0)
** done already
* check the TapeR-data for monotonicity and evtl. adjust code?
  => problem is: R0=TRUE with too many diameters is critical
** done already
* check the behaviour of R0=TRUE, maybe use only if 1 or 2 diameter are given?
* evtl. R0-Option in Funktionen ergänzen
** done already
* plot with mono=TRUE (cf. tprDiameter)
** done
* plot does not recognise obs=TRUE (not yet implemented!)
** done
* plot(tprObject, main="string") throws an error, because main is not known in tprDiameterCpp
** done
* write method to update a tpr-object, including a monotonicity check problem is, that
  updating via slots, does not force a check automatically, and hence wrong results
  might appear). That check must be placed in e.g. Ht<- function.

* in tprVolume: select correct variance when calculating volume depending on chosen interval
** done
* in constructor: add sHt with default value=0
** done
* in constructor: add parameter "inv" to allow for simple selection of form according to specified inventory
** done
* in tprDiameter and tprVolume add option "MSE" for parameter interval
** done for tprDiameter
** done for tprVolume
* CHECK: corrected biomass estimation: tprBiomass now returns aboveground
  biomass  *without* component needles as the underlying functions do not include needles.
  ## tprBiomass returns abg without needles; but I think TI assumes agb of their function
  includes needles ## cf. Abschlussbericht_Biomassefunktionen_BWI_3_v2b.docx and
  BWI_3_Schlussbericht_FVA_Biomasseerhebung 2010.pdf or
  BWI_3_Methoden_Modul_4_Schlussbericht_mod_v2.doc
** checked: indeed, "total biomass"-functions (agb) of NFI include needles => changed!
* check: return of biomass function for 1 tree, possibly small
  tprBiomass(tprTrees(spp=15, Dm=10, Hm=1.3, Ht=7), component = "all")
  tprBiomass(simTrees(), component = "all")
** corrected (using simplify=FALSE in apply())
* if package is not loaded/attached explicitely but used with '::'. data is not
  available. => Need to check and possibly temporarily load data in calling function
* if sHt is given, not the full information of E_Dhx* is returned by TapeR; The
  vol-integration returns different results

* error in example of nsur-function (C++): wrong species number passed (15)
  => add check on species number
* biomass estimation not 100% identical to rBDAT
* plot.tprTrees does not respect option mono
* add option "all-in-one" to plot: place all taper curves in one plot
* check estimate:
  tprVolume(tpr, AB = list(A=0, B=27), iAB = "H", interval = "confidence")
  tprVolume(tpr, AB = list(A=0, B=27), iAB = "H")
* if tprVolume(default) is called on too small tree, error (all dm < 7); but should be 0
* add estHCB(obj=tpr) (gnls1 and gam1) from package ini4C
* add option NSUR=TRUE to tprBiomass to directly get the NSUR-biomass components estimate,
	instead the NFI-Biomass estimates
* add function biomass into extern, to make it directly available
* tprTrees(..., inv=5) returns
  R-package 'rBDAT' not installed; please install.packages('rBDAT');
  Tarif set to 0
  although rBDAT *is* installed.
  even more:
  Vfm(tprTrees(spp = 1, Dm=30, Hm=1.3, Ht=27, inv=0))
  Vfm(tprTrees(spp = 1, Dm=30, Hm=1.3, Ht=27, inv=5))
  is not identical although indicated!

* add Massentafel-Äquivalence into package, cf. BDAT
* update interface in calling
** allow for data.frame to be handed into tprTrees()
** improve where the situation is as Thomas Riedel experienced:
   Formigkeit wird nicht beachtet, wenn ein Durchmesser pro Baum mit as.list()
   übergeben wird (cf. Email vom Di 12.12.2023 19:44)
* tprBiomass(): add parameter scale=TRUE; if set to FALSE return Biomass component 
  estimates from NSUR directly
* think about implementing NSUR2 (dbh+height-only functions).

* check again the compatability of tprBiomass (useNFI=TRUE) with rBDAT::getBiomass()
