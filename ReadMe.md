<!-- README.md is generated from README.Rmd. Please edit that file -->



# TapeS
## Installation
You can install TapeS from gitlab via:


```r
if (! require("remotes")) install.packages("remotes")
remotes::install_gitlab("vochr/TapeS", build_vignettes = TRUE)
```

## Introduction
After installation, see the help page:

```r
help(package = "TapeS")
```

```
#> TapeS: german NFI-taper functions evaluation.
#> 
#> Description:
#> 
#>      Implementing new taper functions for german NFI and making
#>      available associated function to evaluate diameter, height of
#>      diameter, bark thickness, biomass and volume of these taper
#>      functions. Also providing wrappers to calculate specific diameters
#>      like dbh, d005, d03 and d7 as well as specific volume measures
#>      like Vfm, Efm, VolR, VolE, Vol_FAO, and physical stem volume.
```

Or read the vignette (not yet correctly implemented)

```r
vignette("tapes", package = "TapeS")
```
