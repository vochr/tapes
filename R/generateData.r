#' with this file one can produce part of the data in the TapeS-package
#'
#'
## SKpar = schaftkurven parameter ####
#' ## this code generated a temporary data set for package development
#' ## has been replaced by final taper models via ./devel/buildSKPar.r
#' \dontrun{
#' data(SK.par.lme, package = "TapeR")
#' SK.par.lme$pad_knt_x <- TapeR:::TransKnots(SK.par.lme$knt_x)
#' SK.par.lme$pad_knt_z <- TapeR:::TransKnots(SK.par.lme$knt_z)
#' SK <- 1:8
#' spp <- c(1,3,8,5,10,15,17,18)
#' name <- c("Fi", "Ta", "Dgl", "Kie", "Lae", "Bu", "Ei", "REi")
#' SKPar <- list()
#' for(a in seq(along=spp)){
#'   attr(SK.par.lme, "spp") <- as.integer(spp[a])
#'   attr(SK.par.lme, "name") <- name[a]
#'   SKPar[[a]] <- SK.par.lme
#' }
#' save(SKPar, file="data/SKPar.rdata")
#' }
## volfaodlt7 ####
#' volfaodlt7 <-
#'   structure(list(spp = c("Fichte", "Tanne", "Douglasie", "Kiefer",
#'                          "Laerche", "Buche", "Eiche", "aLh", "aLn"),
#'                  d0 = c(0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L),
#'                  d25 = c(0.000959284, 0.0007824, 0.000968387, 0.001195477,
#'                          0.001265588, 0.001146081, 0.001065996, 0.001112065,
#'                          0.001211409),
#'                  d55 = c(0.008075253, 0.007183764, 0.007765834, 0.008981243,
#'                          0.009745962, 0.009202038, 0.008451154, 0.008760558,
#'                          0.009051539),
#'                  d65 = c(0.0126819, 0.01149135, 0.01207125, 0.01376877,
#'                          0.0150196, 0.01430739, 0.01310464, 0.01356614,
#'                          0.01386055)),
#'             class = "data.frame",
#'             row.names = c(NA, -9L))
#' save(volfaodlt7, file="data/volfaodlt7.rdata")
#'
## double bark thickness parameter ####
#' \dontrun{
#' dbtp <- array(NA, c(28, 4, 3))
#' # 'Fichte'
#' dbtp[1,,]<-matrix(c(1.55540,.55475,-0.00255,
#'                     0.82652,.59424,-0.00212,
#'                     0.17440,.67905,-0.00247,
#'                     0.85149,.60934,-0.00228),
#'                   nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Tanne Mittenst.S.'
#' dbtp[2,,]<-matrix(c(1.67703,.56074,0.,
#'                     0.82802,.62504,0.,
#'                     0.67058,.68492,0.,
#'                     1.76896,.59175,0.),
#'                   nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Douglasie Mittenst.S. BRD'
#' dbtp[3,,]<-matrix(c(.26442,.84153,-.00233,
#'                     -.78130,.80713,-.00290,
#'                     -1.28853,.82882,-.00354,
#'                     -2.13785,.91597,-.00375),
#'                   nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Kiefer'
#' dbtp[4,,]<-matrix(c(5.43367,.62571,0.,
#'                     .05652,.56149,0.,
#'                     4.17891,.22292,0.,
#'                     1.59099,.50146,0.),
#'                   nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Sitka-Fichte'
#' dbtp[5,,]<-matrix(c(1.63833,.79330,-.01024,
#'                     1.87531,.66048,-.00701,
#'                     -3.23264,.99143,-.01183,
#'                     0.05167,.81782,-.00968),
#'                   nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Europ.-Laerche'
#' dbtp[6,,]<-matrix(c(4.17426,.99857,0.,
#'                     1.21230,1.08527,0.,
#'                     1.24887,1.20858,0.,
#'                     3.58012,1.03147,0.),
#'                   nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Jap.Laerche'
#' dbtp[7,,]<-matrix(c(-8.04088,1.38585,0.,
#'                     -3.47512,1.23849,0.,
#'                     -15.26157,1.94338,0.,
#'                     -3.77073,1.29960,0.),
#'                   nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Schwarzkiefer'
#' dbtp[8,,]<-matrix(c(10.72482,1.02208,0.,
#'                     11.00729,.80471,0.,
#'                     10.30825,.72255,0.,
#'                     5.27169,1.12602,0.),
#'                   nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Weymouthskiefer'
#' dbtp[9,,]<-matrix(c(4.67218,.52074,0.,
#'                     4.83026,.44885,0.,
#'                     4.06786,.46533,0.,
#'                     3.63666,.50782,0.),
#'                   nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Buche'
#' dbtp[10,,]<-matrix(c(1.97733,.28119,0.,
#'                      2.25734,.29724,0.,
#'                      2.69794,.31096,0.,
#'                      2.61029,.28522,0.),
#'                    nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Stiel-Eiche'
#' dbtp[11,,]<-matrix(c(9.10974,.66266,0.,
#'                      8.94454,.71505,0.,
#'                      9.88377,.75877,0.,
#'                      10.18342,.68997,0.),
#'                    nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Trauben-Eiche (Bauland)'
#' dbtp[12,,]<-matrix(c(11.90259,.74264,0.,
#'                      12.80219,.75623,0.,
#'                      13.54592,.78294,0.,
#'                      14.31589,.72699,0.),
#'                    nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Trauben-Eiche (Vorbergzone)'
#' dbtp[13,,]<-matrix(c(6.53655,.58931,0.,
#'                      7.41153,.61954,0.,
#'                      9.30269,.64363,0.,
#'                      9.88855,.56734,0.),
#'                    nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Rot-Eiche'
#' dbtp[14,,]<-matrix(c(-1.84576,.86920,-.00523,
#'                      -3.98295,.95987,-.00616,
#'                      -3.44335,.97132,-.00630,
#'                      -3.19581,.93891,-.00596),
#'                    nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Bergahorn'
#' dbtp[15,,]<-matrix(c(-0.60951,.64014,-.00329,
#'                      -3.53373,.91611,-.00707,
#'                      -4.57300,1.06506,-.00929,
#'                      -0.62466,.73312,-.00482),
#'                    nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Linde'
#' dbtp[16,,]<-matrix(c(2.26031,.60113,0.,
#'                      .38145,.68665,0.,
#'                      .32491,.72416,0.,
#'                      1.39565,.65024,0.),
#'                    nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Esche'
#' dbtp[17,,]<-matrix(c(-1.14181,.96466,-.00432,
#'                      -8.40201,1.41083,-.00964,
#'                      -3.62803,1.21051,-.00777,
#'                      -7.97623,1.40182,-.01011),
#'                    nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Hainbuche'
#' dbtp[18,,]<-matrix(c(5.03406,.28277,0.,
#'                      9.12572,.16002,0.,
#'                      6.96653,.24105,0.,
#'                      7.47159,.20957,0.),
#'                    nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Robinie'
#' dbtp[19,,]<-matrix(c(-0.27505,1.58820,0.,
#'                      -3.25513,1.70546,0.,
#'                      -3.85748,1.81024,0.,
#'                      -2.57381,1.69622,0.),
#'                    nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Bergulme'
#' dbtp[20,,]<-matrix(c(8.18895,.84696,0.,
#'                      8.61699,.88780,0.,
#'                      5.61794,1.06130,0.,
#'                      8.26574,.89505,0.),
#'                    nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Birke'
#' dbtp[21,,]<-matrix(c(-2.11471,0.94927,0.,
#'                      1.59351,.77363,0.,
#'                      3.77295,.69716,0.,
#'                      1.63138,.78958,0.),
#'                    nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Maril.Pappel'
#' dbtp[22,,]<-matrix(c(14.26657,.77462,0.,
#'                      6.75576,.90682,0.,
#'                      8.66612,.90762,0.,
#'                      12.74678,.79624,0.),
#'                    nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Robusta-Pappel'
#' dbtp[23,,]<-matrix(c(.59848,1.20446,-.00622,
#'                      .66922,1.09135,-.00480,
#'                      -5.09109,1.35474,-.00771,
#'                      -1.26961,1.21661,-.00624),
#'                    nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Neupotz-Pappel'
#' dbtp[24,,]<-matrix(c(-3.63150,1.20021,-0.00481,
#'                      -4.36578,1.13876,-0.00394,
#'                      -8.28526,1.42238,-0.00720,
#'                      -3.68200,1.21090,-0.00514),
#'                    nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Regenerata-Pappel'
#' dbtp[25,,]<-matrix(c(10.09257,0.65481,0.,
#'                      7.88366,0.63141,0.,
#'                      3.63461,0.72700,0.,
#'                      7.20165,0.67110,0.),
#'                    nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Kirsche'
#' dbtp[26,,]<-matrix(c(4.54768,0.52967,0.,
#'                      2.02813,0.66068,0.,
#'                      1.54289,0.73426,0.,
#'                      4.05603,0.58080,0.),
#'                    nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Spitzahorn'
#' dbtp[27,,]<-matrix(c(6.68053,0.42792,0.,
#'                      4.81679,0.50288,0.,
#'                      6.39906,0.50162,0.,
#'                      7.43957,0.43244,0.),
#'                    nrow = 4, ncol = 3, byrow = TRUE)
#' # 'Roterle'
#' dbtp[28,,]<-matrix(c(7.33868,.82437,0.,
#'                      8.02746,.83030,0.,
#'                      8.35663,.90946,0.,
#'                      8.05239,.84910,0.),
#'                    nrow = 4, ncol = 3, byrow = TRUE)
#' save(dbtp, file="data/dbtp.rdata")
#' }
#'
## cutting diameter parameter (AufarbeitungZopfParameter) ####
#' \dontrun{
#' azp <- matrix(data=c(.639936E+0, .663158E+0, -.380240E-1, 0., # spruce (S-Dt)
#'  -.162417E+1, .230909E+1, -.349112E+0, .913825E-002, # spruce (N-Dt)
#'  .923207E+0, .615834E+0, -.688764E-1, .781537E-2, # silver fir
#'  -.170313E+1, .238869E+1, -.396125, .241501E-1, # pine
#'  .113353E+1, .167188, .113906, -.161586E-1, # larch
#'  .241433E+1, -.330161E+0, .129946E+0, -.112017E-1, # beech
#'  -.358315E+0, .113226E+1, -.208216E-1 , -.150484E-1), # oak
#'  nrow = 7, ncol = 4, byrow = TRUE)
#' save(azp, file="data/azp.rdata")
#' }
#'
#'
#' load("data/azp.rdata")
#' load("data/crownExpansion.rdata")
#' load("data/dbtp.rdata")
#' load("data/SKPar.rdata")
#' load("data/unvd.rdata")
#' load("data/volfaodlt7.rdata")
#'
#' save("azp", "crownExpansion", "dbtp", "SKPar", "unvd", "volfaodlt7",
#'      file = "R/sysdata.rda")
#'
#' ADD BIOMASS MODELS from TapeR-Project
#' \dontrun{
#'   e <- new.env() # BioMassModels
#'   BMM <- list()
#'   p <- "J:/FVA-Projekte/P01697_BWI_TapeR/Daten/Ergebnisse/biomass/nsur"
#'   fls <- c("fi/pooledNSUR_dhd03_mNSUR_Fi.RData",
#'            "ta/pooledNSUR_dhd03_NSUR_Ta.RData",
#'            "kie/pooledNSUR_dhd03_mNSUR_Kie.RData",
#'            "dgl/pooledNSUR_dhd03_mNSUR_Dgl.RData",
#'            "bu/pooledNSUR_dhd03_mNSUR_Bu.RData",
#'            "ei/pooledNSUR_dhd03_mNSUR_Ei.RData",
#'            "ba/pooledNSUR_dhd03_NSUR_Ba.RData",
#'            "es/pooledNSUR_dhd03_NSUR_Es.RData")
#'   for(f in fls){
#'     # f <- fls[1]
#'     spp <- dirname(f)
#'     load(file.path(p, f), envir = e)
#'     BMM[[spp]] <- e$pooledfit
#'     e$pooledfit <- NULL
#'   }
#'   str(BMM, max.level = 1)
#' }
#'
#' load("R/sysdata.rda")
#' save("crownExpansion", file="devel/crownExpansion.rda")
#' save("azp", "dbtp", "SKPar", "unvd", "volfaodlt7", "BMM",
#'      file = "R/sysdata.rda")
#' tools::resaveRdaFiles("R/sysdata.rda", compress = "xz")
