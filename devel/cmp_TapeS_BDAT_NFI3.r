#' compare volume estimation by TapeS (with spp=15=>relBDAT_Bu, spp=17=>relBDAT_Ei
#' and spp=18=>relBDAT_REi)

## load NFI data
load(file.path("H:/FVA-Projekte/P01697_BWI_TapeR/Daten/aufbereiteteDaten",
               "BWIreferenceData/bwi20150320_alle_daten2012.rdata"))

## get data
bas <- c(1,3,5,8,9,15,17,18)
pdf("devel/cmp_TapeS_BDAT_NFI3.pdf", width=20, height = 10, paper="a4r")
# par(mfrow=c(4,2), cex=1.2, mar=c(3,3,2,.1))
# par(cex=1.2, mar=c(3,3,2,.1))
p <- "H:/FVA-Projekte/P01697_BWI_TapeR/Daten/Ergebnisse/BWIcomparison"
bas <- c(1,3,5,8,9,15,17,18)
agg1 <- list()
agg2 <- list()
for(ba in bas){
  # ba <- 1

  f <- file.path(p, paste0("BA=",ba,".rdata"))
  if(file.exists(f)){
    load(f)
  } else {
    load(file.path(p, paste0("BA=",ba,"_n=1000.rdata")))
  }
  # remove old results
  sdat$vfm_all <- sdat$vfm_bdat <- sdat$vfm_vfl <- NULL
  sdat$mono_all <- sdat$mono_bdat <- sdat$mono_vfl <- NULL
  sdat$vfm_tapes <- TapeS::tprVolume(bdat_as_tprtrees(
    rBDAT::buildTree(sdat[, c("spp", "D1", "H1", "D2", "H2", "H")])))
  sdat$Bclass <- sdat$D1 %/% 5
  nms <- paste0(sort(unique(sdat$Bclass))*5, "+")
  sdat$e_all <- (sdat$vfm_tapes - sdat$vfm) / sdat$vfm * 100

  sp <- rBDAT::getSpeciesCode(ba, "kurz")
  # todo: constrain interval to 99% of data???
  boxplot(e_all ~ Bclass, data = sdat, main=paste0("Baumart = ", sp), las=1,
          ylab="Vfm (TapeS) - Vfm (BDAT) [%]", pch=".", names=nms,
          ylim=c(-40, 40))
  abline(h=0)
  sdat <- merge(sdat, wzp[, c("Tnr", "Enr", "Bnr", "N_ha")], by = c("Tnr", "Enr", "Bnr"))
  sdat$vfm_ha <- sdat$vfm * sdat$N_ha
  sdat$vfm_tapes_ha <- sdat$vfm_tapes * sdat$N_ha
  agg1[[ba]] <- tmp <- aggregate(sdat[, c("vfm_ha", "vfm_tapes_ha")],
                                 by=list(spp=sdat$spp), FUN="sum")
  agg2[[ba]] <- tmp <- aggregate(sdat[, c("vfm_ha", "vfm_tapes_ha")],
                                 by=list(spp=sdat$spp, D1=sdat$Bclass), FUN="sum")
  barplot(height = as.matrix(t(tmp[, c("vfm_ha", "vfm_tapes_ha")])), beside = TRUE,
          names.arg = nms, legend.text = c("BDAT", "TapeS"), ylab="Vfm", xlab="Dbh class")
}
agg1 <- do.call(rbind, agg1)
barplot(height = as.matrix(t(agg1[, c("vfm_ha", "vfm_tapes_ha")])), beside = TRUE,
        names.arg = rBDAT::getSpeciesCode(agg1$spp, "kurz"), ylab="Vfm", xlab="species",
        legend.text = c("BDAT", "TapeS"))
agg2 <- do.call(rbind, agg2)
dev.off()

## taper curves
pdf("devel/cmp_TapeS_BDAT_NFI3_tapercurves.pdf", width=20, height = 10, paper="a4r")
par(mfrow=c(2,3))
p <- "H:/FVA-Projekte/P01697_BWI_TapeR/Daten/Ergebnisse/BWIcomparison"
for(ba in bas){
  # ba <- 1

  f <- file.path(p, paste0("BA=", ba, ".rdata"))
  if(file.exists(f)){
    load(f)
  } else {
    load(file.path(p, paste0("BA=",ba,"_n=1000.rdata")))
  }
  # remove old results
  sdat$vfm_all <- sdat$vfm_bdat <- sdat$vfm_vfl <- NULL
  sdat$mono_all <- sdat$mono_bdat <- sdat$mono_vfl <- NULL
  sdat$Bclass <- sdat$D1 %/% 5
  nms <- paste0(sort(unique(sdat$Bclass))*5, "+")
  agg <- aggregate(sdat[, c("spp", "D1", "H1", "D2","H2", "H")],
                   by=list(D=sdat$Bclass*5), "mean")
  agg
  for(i in 1:nrow(agg)){
    bdt <- buildTree(agg[i, c("spp", "D1", "H1", "D2","H2", "H")])
    plot(bdt, bark=TRUE)
    tpr <- bdat_as_tprtrees(bdt)
    hx <- seq(0, agg[i, "H"], 0.1)
    dx <- tprDiameterCpp(tpr, Hx=hx)
    points(x=hx, y=dx, type="l", lty=2)
    points(x=agg[i, c("H1", "H2")], y=agg[i, c("D1", "D2")], pch=3, cex=2)
    legend("topright", legend = c("BDAT", "TapeS"), lty=c(1,2))
  }
  plot.new()
}
dev.off()

## comparison of BDAT and TapeS against RBS-Data
require(RODBC)
con <- RODBC::odbcConnectAccess("H:/FVA-Projekte/P01278_EnNa/P01278_EnNa_BiomassFunc/Daten/Urdaten/BuI/Biomasse_Erg.mdb")
tblB <- RODBC::sqlFetch(con, "Baum")
RODBC::odbcClose(con)
tblB$Baumart <- ifelse(tblB$Baumart == 8, 4, tblB$Baumart)

spmap <- data.frame(BaBdat = c(1, 3, 5, 8, 9, 15, 17, 18),
                    BaRBS = c(1, 2, 4, 3, NA, 7, 6, NA))
tblB <- merge(tblB, spmap, by.x = "Baumart", "BaRBS")

tblB <- tblB[, c("Baumart", "BaBdat", "BHD", "Höhe", "b_D7", "KRO", "b_BdatVol", "b_DerbholzVol")]
tblB$spp <- tblB$BaBdat
tblB$D1 <- tblB$BHD / 10
tblB$H1 <- 1.3
tblB$D2 <- tblB$b_D7 / 10
tblB$H2 <- 7
tblB$H <- tblB$Höhe

tblB$b_BdatVol2 <- rBDAT::getVolume(tblB)
tblB$e_BdatVol <- tblB$b_BdatVol - tblB$b_DerbholzVol
tblB$b_TapeSVol <- tprVolume(bdat_as_tprtrees(rBDAT::buildTree(tblB)))
tblB$e_TapeSVol <- tblB$b_TapeSVol - tblB$b_DerbholzVol
head(tblB)

pdf("devel/cmp_TapeS_BDAT_RBS.pdf", width=20, height = 10, paper="a4r")

plot(e_BdatVol ~ b_DerbholzVol, data=tblB, pch = tblB$BaBdat,
     main="Derbholz-Schätzung RBS vs. BDAT")
abline(h=0)
lines(smooth.spline(tblB$b_DerbholzVol, tblB$e_BdatVol, spar=1), lty=2, col="red")
lines(smooth.spline(tblB$b_DerbholzVol, tblB$e_TapeSVol, spar=1), lty=2, col="blue")
legend("bottomleft", legend = TapeS::tprSpeciesCode(sort(unique(tblB$BaBdat)), "kurz"),
       pch=sort(unique(tblB$BaBdat)))
legend("topright", legend=c("BDAT", "TapeS"), lty=2, col=c("red", "blue"))
Metrics::rmse(tblB$b_BdatVol, tblB$b_DerbholzVol)
Metrics::bias(tblB$b_BdatVol, tblB$b_DerbholzVol)

plot(e_TapeSVol ~ b_DerbholzVol, data=tblB, pch = tblB$BaBdat,
     main="Derbholz-Schätzung RBS vs. TapeS")
abline(h=0)
lines(smooth.spline(tblB$b_DerbholzVol, tblB$e_BdatVol, spar=1), lty=2, col="red")
lines(smooth.spline(tblB$b_DerbholzVol, tblB$e_TapeSVol, spar=1), lty=2, col="blue")
legend("bottomleft", legend = TapeS::tprSpeciesCode(sort(unique(tblB$BaBdat)), "kurz"),
       pch=sort(unique(tblB$BaBdat)))
legend("topright", legend=c("BDAT", "TapeS"), lty=2, col=c("red", "blue"))

Metrics::rmse(tblB$b_TapeSVol, tblB$b_DerbholzVol)
Metrics::bias(tblB$b_TapeSVol, tblB$b_DerbholzVol)

dev.off()
## rmse + bias according to species
aggregate(tblB[, c("e_BdatVol", "e_TapeSVol")], by=list(spp=tblB$BaBdat),
          FUN=function(x){sqrt(mean(x^2))})
aggregate(tblB[, c("e_BdatVol", "e_TapeSVol")], by=list(spp=tblB$BaBdat),
          FUN="mean")
