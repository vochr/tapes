# remotes::install_gitlab("vochr/tapes")
require(TapeS)
TapeS::setTapeSoptions(Rfn = list(fn="sig2"))
require(rBDAT)

density <- c(Fi=378, Ta=361, Kie=431, DG=414, Bu=558, Ei=572)

for(spp in c(1, 3, 5, 8, 15, 17)){
  dm <- 15:150
  h <- iS::estHeight(d13 = dm, sp = spp)
  n <- length(dm)
  #plot(h ~ dm, main=tprSpeciesCode(spp, "lang"))
  bdt <- rBDAT::buildTree(list(spp=spp, D1=dm, H2=50, H=h))
  t <- bdat_as_tprtrees2(bdt)
  dhvol <- TapeS::tprVolume(t, bark = FALSE)
  #plot(dhvol ~ dm, main=tprSpeciesCode(spp, "lang"))
  bm <- tprBiomass(t, component = c("sw", "agb"))
  #plot(bm[, "sw"] ~ dm, main=tprSpeciesCode(spp, "lang"))
  dh <- TapeS::nsur(spp = BaMap(,6)[rep(spp, n)], dbh = dm, ht = h, sth = 0.01*h, d03 = D03(t),
                    kl = 0.7*h)
  plot(dh[, "sw"] ~ dm, main=tprSpeciesCode(spp, "lang"), type="l",
       ylab="solid wood [kg]", xlab="dbh [cm]")
  points(x=dm, y=bm[,"sw"], type="l", lty=2)
  legend("topleft", legend = c("Allometry", "Marklund"), lty=c(1,2))

  ## calculate specific gravity between volume and biomass
  grav <- dh[, "sw"] / dhvol # nsur / taper
  grav2 <- bm[,"sw"] / dhvol # marklund / taper

  bdatvol <- rBDAT::getVolume(tree = list(spp=spp, D1=dm, H2=50, H=h), bark=FALSE)
  bdtgrav <- dh[, "sw"] / bdatvol # nsur / bdat
  bdtgrav2 <- bm[,"sw"] / bdatvol # marklund / bdat

  if(spp==15){
    ## calculate specific gravity for spp==37
    bdt$Hx <- 0.3*bdt$H
    bdt$D2 <- getDiameter(bdt)
    bdt$H2 <- bdt$Hx
    bdt$Hx <- NULL
    bdt$spp <- 37
    t37 <- bdat_as_tprtrees(bdt)

    dhvol37 <- TapeS::tprVolume(t37, bark = FALSE)
    grav37 <- dh[, "sw"] / dhvol37 # nsur / taper
    grav237 <- bm[,"sw"] / dhvol37 # marklund / taper
    rangey <- c(range(c(grav, grav2, bdtgrav, bdtgrav2, grav37, grav237)))
  } else {
    rangey <- c(range(c(grav, grav2, bdtgrav, bdtgrav2)))
  }

  plot(grav ~ dm, main=tprSpeciesCode(spp, "lang"), ylim=rangey,
       ylab = " specific gravity [kg/m3]", xlab="Dbh [cm]", type="l", lwd=2, col=2)
  abline(h=density[getSpeciesCode(spp, "kurz")])
  points(x=dm, y=grav, lty=1, type="l", lwd=2, col=2)
  points(x=dm, y=grav2, lty=3, type="l", lwd=2, col=2)
  points(x=dm, y=bdtgrav, lty=1, type="l", lwd=2, col=4)
  points(x=dm, y=bdtgrav2, lty=3, type="l", lwd=2, col=4)
  if(spp==15){
    points(x=dm, y=grav37, lty=1, type="l", lwd=2, col=5)
    points(x=dm, y=grav237, lty=3, type="l", lwd=2, col=5)
    legend("bottomleft", legend=c("Marklund/TapeR", "NSUR/TapeR",
                                  "Marklund/BDAT", "NSUR/BDAT",
                                  "Marklund/TapeR-37", "NSUR/TapeR-37"),
           lty=c(3,1,3,1,3,1), col=c(2,2,4,4,5,5), lwd=2)
  } else {
    legend("bottomleft", legend=c("Marklund/TapeR", "NSUR/TapeR",
                                  "Marklund/BDAT", "NSUR/BDAT"),
           lty=c(3,1,3,1), col=c(2,2,4,4), lwd=2)
  }


}

# p <- "H:/BuI/Persoenlich/Vonderach/TapeS/devel"
# rmarkdown::render(file.path(p, "check_bdat_vol_biomass_large_trees.r"),
#                   output_format = "pdf_document",
#                   output_file = file.path(p, "check_bdat_vol_biomass_large_trees.pdf"))
