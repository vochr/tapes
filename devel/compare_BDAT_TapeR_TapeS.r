#' compare different taper models
require(TapeS)
require(rBDAT)

par(mfrow=c(1,1))
spp <- 15
d <- 150
h <- estHeight(d, spp)
hx <- seq(0, h, 0.1)
bdt <- buildTree(data.frame(spp=spp, D1=d, H=h))
ff <- rBDAT::getForm(list(spp=spp, D1=d, H=h), inv = 4) # form faktor BWI3
bdt <- buildTree(data.frame(spp=spp, D1=d, D2=-ff, H=h))
d03 <- getDiameter(bdt, Hx=0.3*bdt$H)
plot(bdt)
points(x=c(1.3, 0.3*bdt$H), y=c(d, d03))

options("TapeS_Rfn"=list(fn="dlnorm"))
tpr <- TapeS::tprDiameter(tprTrees(spp=15, Dm=c(d, d03), Hm=c(1.3, 0.3*h), Ht=h),
                          Hx=hx)
points(x=hx, y=tpr, type="l", lty=2)

tpr37 <- TapeS::tprDiameter(tprTrees(spp=37, Dm=c(d, d03), Hm=c(1.3, 0.3*h), Ht=h), Hx=hx)
points(x=hx, y=tpr37, type="l", lty=3)
