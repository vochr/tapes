#' calculate fit statistics for TapeS (v0.11.1) vs.BDAT based on presmoothed
#' BDAT-data (data used to fit the models)
#'

rm(list=ls())
require(TapeR)
require(RSQLite)
require(dplyr)
require(TapeS) # v0.11.1 required

p <- "H:/FVA-Projekte/P01697_BWI_TapeR"
f <- file.path(p, "Daten/Ergebnisse/SektionsDB.sqlite")

fitstats <- list()
for(ba in c(15L, 17L, 18L)){
# for(ba in c(17L, 18L)){
  # ba <- 17L
  if(identical(ba, 15L)){
    ban <- "Bu"
  } else if(identical(ba, 17L)){
    ban <- "Ei"
  } else if(identical(ba, 18L)){
    ban <- "REi"
  }
  conn <- RSQLite::dbConnect(RSQLite::SQLite(), f)
  RSQLite::dbListTables(conn)
  tblF <- RSQLite::dbReadTable(conn, name = "herkunft")
  sql1 <- paste0("SELECT * FROM baumdaten WHERE BA = ", ba, " AND FID IN (2) AND dataOK = 1;")
  sql1 <- paste0("SELECT * FROM baumdaten WHERE BA = ", ba, " AND FID IN (2);")
  bd <- DBI::dbGetQuery(conn, sql1) # Baumdaten
  sql2 <- paste0("SELECT sektionsdaten.BID, rel, sekD1, sekD2, sekH, H, estKA ",
                 "FROM sektionsdaten ",
                 "INNER JOIN baumdaten ",
                 "ON sektionsdaten.BID=baumdaten.BID ",
                 "WHERE BA = ", ba, " AND baumdaten.FID IN (2) AND useRec = 1;")
  sek <- DBI::dbGetQuery(conn, sql2) # Sektionsdaten
  RSQLite::dbDisconnect(conn) # closing
  head(sek)
  length(unique(sek$BID)) == nrow(bd)

  sek <- sek %>%
    rowwise() %>%
    mutate(D = ifelse(is.na(sekD1) & is.na(sekD2), NA,  # mean diameters in cm
                      mean(c(sekD1, sekD2), na.rm = TRUE) / 10),
           Hoehe = H,
           H = sekH / 100, # height in m
           Hrel = (H / (Hoehe / 10))) %>% # relative measurement height
    arrange(BID, Hrel) %>%
    dplyr::select(BID, D, H, Hrel) %>%
    as.data.frame()

  ## add relative measurement of tree top, i.e. sekH=H/H and sekD=0/D05=0
  sek <- bd %>%
    mutate(D = 0, # in cm
           H = H / 10, # in cm
           Hrel = 1) %>%
    dplyr::select(BID, D, H, Hrel) %>%
    rbind(sek) %>%
    arrange(BID, Hrel)

  rmse <- function(y, yhat){
    sqrt(mean((yhat - y)^2))
  }
  bias <- function(y, yhat){
    mean(yhat - y)
  }

  b <- sek %>%
    left_join(bd[, c("BID", "BA")], by = "BID") %>%
    group_by(BID) %>%
    summarise(spp = first(BA),
              D1 = approx(x=H, y=D, xout=1.3, rule = 2:1)$y,
              H1 = 1.3,
              D2 = approx(x=H, y=D, xout=0.3*max(H), rule = 2:1)$y,
              H2 = 0.3*max(H),
              H = max(H))
  head(b)
  yhatbdat <- lapply(unique(b$BID), function(a){
    # a <- 39916
    # print(a)
    rBDAT::getDiameter(data.frame(b[b$BID==a,], Hx=sek[sek$BID==a, "H"]))
  })
  sek$Dbdat <- unlist(yhatbdat)
  yhattpr <- lapply(unique(b$BID), function(a){
    # a <- 39916
    # print(a)
    tpr <- bdat_as_tprtrees(buildTree(b[b$BID==a,]))
    TapeS::tprDiameter(tpr, Hx = sek[sek$BID==a, "H"])
  })
  sek$Dtapes <- unlist(yhattpr)

  head(sek)
  fitstats[[ba]] <- c(rmse_bdt=rmse(sek$D, sek$Dbdat), bias_bdt=bias(sek$D, sek$Dbdat),
                    rmse_tps=rmse(sek$D, sek$Dtapes), bias_tps=bias(sek$D, sek$Dtapes))
}
do.call(rbind, fitstats)
