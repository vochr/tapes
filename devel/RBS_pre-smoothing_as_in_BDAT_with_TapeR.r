#' pre-smoothing mit

rm(list=ls())
## run DeciduousTaperCurvefromRBS_MDM.r before this script!
p <- "J:/FVA-Projekte/P01697_BWI_TapeR/Programme/Eigenentwicklung/96_publication/"
load(file.path(p, "data.rdata"))

head(baum)
head(smoothed)
nrow(merge(baum[, c("Id", "KRO")], res[, c("Id", "KRO")])) # same KRO data in res & Baum

smoothed <- merge(smoothed, baum[, c("Id", "KRO", "b_DerbholzVol")],
                  by.x = "id", by.y = "Id", all.x = TRUE)
colnames(smoothed)[colnames(smoothed) == "b_DerbholzVol"] <- "DhVol"
# one error found:
smoothed[smoothed$id==82604, "d"] # durchmesser 5 unplausibel
smoothed[smoothed$id==82604 & smoothed$relH == 0.25, "d"] <-  mean(c(23.7253548, 22.2727977))

## optimise diameter in 70% of tree height ####
i <- 11701
t <- subset(smoothed, id==i)
t <- t[order(t$relH),]
head(t)

ht <- t$ht[1]
kro <- t$KRO[1]
vol <- t$DhVol[1]
x <- 26

d13 <- approx(x = t$h, y = t$d, xout=1.3)$y
d03 <- approx(x = t$h, y = t$d, xout=0.3*ht)$y
dhcb <- approx(x = t$h, y = t$d, xout=kro)$y
if(FALSE){
  dx <- c(d13, dhcb, d03, 7)
  hx <- c(1.3, kro, 0.3*ht, x)
} else {
  dx <- c(t[t$h < kro, "d"], 7)
  hx <- c(t[t$h < kro, "h"], x)
}
dx <- dx[order(hx)]
hx <- hx[order(hx)]

require(TapeS)
tps <- tprTrees(spp=15, Dm=dx, Hm=hx, Ht=ht)

xpred <- seq(0, ht, 0.001)
dhat <- tprDiameterCpp(tps, Hx=xpred)

plot(d ~ h, data=t, type="o")
abline(v=kro)
points(x=hx, y=dx, pch=3, cex=2)
points(x=xpred, y=dhat, type="l", col="red")
legend("bottomleft", legend=c(paste0("TPS = ", round(Vfm(tps), 4)),
                               paste0("RBS = ", round(vol, 4))),
       lty=c(1), pch=c(NA, 1), col=c("red", "black"))

u <- 22102
u <- 16901
u <- 11701
t <- subset(smoothed, id==u)
t <- t[order(t$relH),]
head(t)

ht <- t$ht[1]
kro <- t$KRO[1]
vol <- t$DhVol[1]
x <- ht-2



f <- function(x, d, h, ht, vol, bnr, step=0.1, sll=2, vis=FALSE){
  # x <- 25
  # print(x)
  # step <- 1

  d13 <- approx(x = h, y = d, xout=1.3)$y
  d03 <- approx(x = h, y = d, xout=0.3*ht)$y
  dhcb <- approx(x = h, y = d, xout=kro)$y
  if(FALSE){
    dx <- c(d13, dhcb, d03, 7)
    hx <- c(1.3, kro, 0.3*ht, x)
  } else if(FALSE){
    dx <- c(t[t$h < kro, "d"], 7)
    hx <- c(t[t$h < kro, "h"], x)
  } else if(TRUE){
    dx <- c(t[t$h < kro, "d"], rep(x, 5))
    hx <- c(t[t$h < kro, "h"], rep(0.90*ht, 5))
  } else {
    dx <- c(d13, dhcb, d03, rep(x, 5))
    hx <- c(1.3, kro, 0.3*ht, rep(0.90*ht, 5))
  }
  d <- dx[order(hx)]
  h <- hx[order(hx)]

  tps <- tprTrees(spp=15, Dm=d, Hm=h, Ht=ht)

  xpred <- seq(0, ht, 0.01)
  yhat <- tprDiameterCpp(tps, Hx=xpred)

  h7 <- approx(x = yhat, y = xpred, xout = 7)$y
  # h7 <- xpred[which.min(abs(yhat-7))] * ht
  sl <- TapeS:::slfun(A = 0, B = h7, sl = sll)
  dhat <- approx(x = xpred, y = yhat, xout = sl$Hx)$y
  volhat <- sum(pi * (dhat/200)^2 * sl$L)
  volhat <- ifelse(any(yhat < 0), 0, volhat)
  if(isTRUE(vis)){
    plot(d ~ h, data=t, type="o", las=1, main=bnr, ylim=c(0, max(t$d, dhat)),
         xlim=range(c(0, ht)))

    abline(v=kro)
    abline(h=7, col="grey")
    points(x=h, y=d, pch=3)
    points(x=xpred, y=yhat, type="l", lty=2, col="red")
    # add cylinders for vol estimate
    sapply(seq(along=dhat), function(a) {
      # a <- 1
      rect(xleft = sl$Hx[a] - (sl$L[a] / 2), xright = sl$Hx[a] + (sl$L[a] / 2),
           ybottom = 0, ytop = dhat[a])
      # lines(x = hh, y=dd, col="blue")
    })
    ## add taper curves
    dbh <- approx(x = h, y = d, xout = 1.3)$y
    d03 <- approx(x = h, y = d, xout = 0.3*ht)$y
    if(all(!is.na(c(dbh, d03)))){
      # add BDAT
      dhat <- rBDAT::getDiameter(list(spp=15, D1=dbh, D2=d03, H2=0.3*ht, H=ht,
                                      Hx = xpred))
      points(x=xpred, y=dhat, type="l", col="blue")
      volbdat <- rBDAT::getVolume(list(spp=15, D1=dbh, D2=d03, H2=0.3*ht, H=ht))
      # add TapeS
      tps <- tprTrees(spp=37, Dm = c(dbh, d03), Hm = c(1.3, 0.3*ht), Ht = ht)
      xp <- seq(0, ht, 0.1)
      dhat <- TapeS::tprDiameter(tps, Hx = xp)
      points(x=xp, y=dhat, type="l", col="darkgreen")
      voltapes <- TapeS::Vfm(tps)
    }
    # legend
    legend("bottomleft", legend=c(paste("Vol_NEW =", round(c(volhat), 4)),
                                  paste("Vol_RBS =", round(c(vol), 4)),
                                  "Dm für Anpassung",
                                  paste("BDAT =", round(c(volbdat), 4)),
                                  paste("TapeS =", round(c(voltapes), 4))),
           lty=c(2, 1, NA, 1, 1), col=c("red", "black", "black", "blue", "darkgreen"),
           pch=c(NA, 1, 3, NA, NA), bg = "white")
  }
  loss <- abs(volhat - vol)
  kroDm <- approx(x = xpred, y = yhat, xout = kro)$y
  Dm01 <- approx(x = xpred, y = yhat, xout = 0.1)$y
  Dm05 <- approx(x = xpred, y = yhat, xout = 0.5)$y
  attr(loss, "info") <- list(negDm=any(yhat<0),
                             diffVol=abs((volhat - vol) / vol) > 0.05,
                             crownDm=max(yhat[xpred>kro]) > kroDm*1.1, # 10% increase ok
                             stump=Dm01 < Dm05)


  return(loss)
}

f(x=0.9*ht, d = d, h = h, ht = ht, vol = vol, sll = 1, bnr = u, vis = TRUE)
f(x=0.7*ht, d = d, h = h, ht = ht, vol = vol, step = 1, sll = 1, bnr = u, vis = TRUE)

res <- optimize(f = f(x, d, h, ht, vol), interval = c(kro, ht),
                d=d, h=h, ht=ht, vol=vol, step=.1, sll=1, bnr=u)
res
f(res$minimum, d, h, ht, vol, step=.1, sll = 1, bnr=u, vis = TRUE)


pdf("J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_TapeR_v1.pdf")
ll <- list()
uid <- sort(unique(smoothed$id))
# for(i in seq(along=uid)[1:20]){
for(i in seq(along=uid)){
  # i <- 1
  u <- uid[i]
  # u <- 16901
  t <- subset(smoothed, id==u)
  t <- t[order(t$relH),]
  # head(t)

  ht <- t$ht[1]
  kro <- t$KRO[1]
  vol <- t$DhVol[1]
  x <- ht-2

  (d13 <- approx(x = t$h, y = t$d, xout=1.3, rule=2)$y)
  (d03 <- approx(x = t$h, y = t$d, xout=0.3*ht)$y)
  (dhcb <- approx(x = t$h, y = t$d, xout=kro)$y)
  if(FALSE){
    dx <- c(d13, dhcb, d03, 7)
    hx <- c(1.3, kro, 0.3*ht, x)
  } else {
    dx <- c(t[t$h < kro, "d"], 7)
    hx <- c(t[t$h < kro, "h"], x)
  }
  d <- dx[order(hx)]
  h <- hx[order(hx)]

  # f(x-2, d, h, ht, vol, sll = 1, bnr = u, vis = TRUE)
  res <- optimize(f = f(x, d, h, ht, vol, bnr, sll), interval = c(0, d13),
                  d=d, h=h, ht=ht, vol=vol, bnr=u, step=0.5, sll=2)

  f(res$minimum, d, h, ht, vol, step=.5, sll=2, bnr=u, vis = TRUE)

  ll[[i]] <- as.data.frame(c(id=u, attr(res$objective, "info")))
  rm(res)
}
dev.off()

res <- do.call(rbind, ll)
res$sum <- rowSums(res[, -1])
colSums(res[res$sum>0, 2:5])
View(res[res$sum>0,])
View(res)

library('staplr')
staplr::select_pages(
  selpages = which(res$negDm>0),
  input_filepath = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_TapeR_v1.pdf",
  output_filepath = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_TapeR_v1-negDm.pdf")

staplr::select_pages(
  selpages = which(res$diffVol>0),
  input_filepath = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_TapeR_v1.pdf",
  output_filepath = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_TapeR_v1-diffVol.pdf")

staplr::select_pages(
  selpages = which(res$crownDm>0),
  input_filepath = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_TapeR_v1.pdf",
  output_filepath = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_TapeR_v1-crownDm.pdf")

staplr::select_pages(
  selpages = which(res$stump>0),
  input_filepath = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_TapeR_v1.pdf",
  output_filepath = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_TapeR_v1-stump.pdf")

#' problematic taper curve imputation
#' * dhat values below zero at top (eg. 66901, 78504)
#' * DhVol estimate unequal to RBS-DhVol (eg 66201)
#' * too high Dhat values at top (eg. 78404)
#' * stump not with monotonically decreasing dhat values (eg. 79704)
#' * most of DhVol is below crown base and the decrease in Dm must be strong inside crown
