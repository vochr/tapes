#' pre-smoothing mit

require(mgcv)
rm(list=ls())
## run DeciduousTaperCurvefromRBS_MDM.r before this script!
p <- "J:/FVA-Projekte/P01697_BWI_TapeR/Programme/Eigenentwicklung/96_publication/"
load(file.path(p, "data.rdata"))

head(baum)
head(smoothed)
nrow(merge(baum[, c("Id", "KRO")], res[, c("Id", "KRO")])) # same KRO data in res & Baum

smoothed <- merge(smoothed, baum[, c("Id", "KRO", "b_DerbholzVol")],
                  by.x = "id", by.y = "Id", all.x = TRUE)
colnames(smoothed)[colnames(smoothed) == "b_DerbholzVol"] <- "DhVol"

## optimise diameter in 70% of tree height ####
i <- 21002
t <- subset(smoothed, id==i)
t <- t[order(t$relH),]
head(t)

ht <- t$ht[1]
kro <- t$KRO[1]
vol <- t$DhVol[1]
x <- 25

h <- t[t$h < t$KRO,]$h
d <- approx(x = t[t$h < t$KRO,]$h, y = t[t$h < t$KRO,]$d,
            xout = seq(min(h), max(h), 0.1))$y
h <- c(seq(min(h), max(h), 0.1), rep(x, 10), rep(ht, 10))
d <- c(d, rep(7, 10), rep(0, 10))

plot(d ~ h, data=t, type="o", main=i)
points(x=h, y=d, pch=3, cex=2)
abline(v=kro)

g <- gam(d ~ s(h))
xpred <- seq(0, ht, 0.1)
dhat <- predict(g, newdata = data.frame(h=xpred))

points(x=xpred, y=dhat, type="l", lty=2, col=2)

## functionalise
u <- 21002
t <- subset(smoothed, id==u)
t <- t[order(t$relH),]
head(t)
d <- t$d
h <- t$h
ht <- t$ht[1]
kro <- t$KRO[1]
vol <- t$DhVol[1]
x <- ht-2
sll <- 1
step <- 0.1
vis <- TRUE
bnr <- u

f <- function(x, d, h, ht, vol, bnr, step=0.1, sll=2, vis=FALSE){
  # x <- 25
  # print(x)
  # step <- 1
  dd <- approx(x = h, y = d,
              xout = seq(min(h), kro, step))$y
  hh <- seq(min(h), kro, step)
  ## adjust diameter above crown to be <= diameter at crown base
  hmind <- hh[dd==min(dd)]
  dhcb <- min(dd)
  if(any(test <- (d>dhcb & h > kro))){
    kroadj <- max(h[test])
  } else {
    kroadj <- kro
  }
  d[d>dhcb & h >= hmind] <- dhcb
  kro <- kroadj
  d <- approx(x = h, y = d,
              xout = seq(min(h), kro, step))$y
  hh <- seq(min(h), kro, step)
  d <- c(d, rep(7, 10), rep(0, 10))
  h <- c(hh, rep(x, 10), rep(ht, 10))

  m <- try(gam(d ~ s(h, bs="cr", k=20)), silent = TRUE)

  if(identical(class(m), "try-error")){
    if(isTRUE(vis)){
      plot(d ~ h, data=t, type="o", las=1, main=bnr, ylim=c(0, max(t$d, dhat)),
           xlim=range(c(0, ht)))
      abline(v=kro)
      abline(h=7, col="grey")
      points(x=h, y=d, pch=3)
    }
    loss <- NA
    attr(loss, "info") <- list(negDm=NA, diffVol=NA, crownDm=NA, stump=NA)
    warning(paste0("ID: ",i , " (try-error in GAM-fit)"))
  } else {
    # summary(m)

    xpred <- seq(0, 1, 0.001)*ht
    yhat <- predict(m, newdata = data.frame(h=xpred))

    h7 <- approx(x = yhat, y = xpred, xout = 7)$y
    # h7 <- xpred[which.min(abs(yhat-7))] * ht
    sl <- TapeS:::slfun(A = 0, B = h7, sl = sll)
    dhat <- approx(x = xpred, y = yhat, xout = sl$Hx)$y
    volhat <- sum(pi * (dhat/200)^2 * sl$L)
    # volhat <- ifelse(any(yhat < 0), 0, volhat)
    if(isTRUE(vis)){
      plot(d ~ h, data=t, type="o", las=1, main=bnr, ylim=c(0, max(t$d, dhat)),
           xlim=range(c(0, ht)))
      abline(v=kro)
      abline(h=7, col="grey")
      points(x=h, y=d, pch=3)
      points(x=xpred, y=yhat, type="l", lty=2, col="red")
      # add cylinders for vol estimate
      sapply(seq(along=dhat), function(a) {
        # a <- 1
        rect(xleft = sl$Hx[a] - (sl$L[a] / 2), xright = sl$Hx[a] + (sl$L[a] / 2),
             ybottom = 0, ytop = dhat[a])
        # lines(x = hh, y=dd, col="blue")
      })
      ## add taper curves
      dbh <- approx(x = h, y = d, xout = 1.3)$y
      d03 <- approx(x = h, y = d, xout = 0.3*ht)$y
      if(all(!is.na(c(dbh, d03)))){
        # add BDAT
        dhat <- rBDAT::getDiameter(list(spp=15, D1=dbh, D2=d03, H2=0.3*ht, H=ht,
                                        Hx = xpred))
        points(x=xpred, y=dhat, type="l", col="blue")
        volbdat <- rBDAT::getVolume(list(spp=15, D1=dbh, D2=d03, H2=0.3*ht, H=ht))
        # add TapeS
        tps <- tprTrees(spp=37, Dm = c(dbh, d03), Hm = c(1.3, 0.3*ht), Ht = ht)
        dhat <- TapeS::tprDiameter(tps, Hx = xpred)
        points(x=xpred, y=dhat, type="l", col="darkgreen")
        voltapes <- TapeS::Vfm(tps)
      }

      # legend
      legend("bottomleft", legend=c(paste("Vol_NEW =", round(c(volhat), 4)),
                                    paste("Vol_RBS =", round(c(vol), 4)),
                                    "Dm für Anpassung",
                                    paste("BDAT =", round(c(volbdat), 4)),
                                    paste("TapeS =", round(c(voltapes), 4))),
             lty=c(2, 1, NA, 1, 1), col=c("red", "black", "black", "blue", "darkgreen"),
             pch=c(NA, 1, 3, NA, NA), bg = "white")
    }
    loss <- abs(volhat - vol)
    kroDm <- approx(x = xpred*ht, y = yhat, xout = kro)$y
    Dm01 <- approx(x = xpred*ht, y = yhat, xout = 0.1)$y
    Dm05 <- approx(x = xpred*ht, y = yhat, xout = 0.5)$y
    attr(loss, "info") <- list(negDm=any(yhat<0),
                               diffVol=abs((volhat - vol) / vol) > 0.05,
                               crownDm=max(yhat[xpred*ht>kro]) > kroDm*1.1, # 10% increase ok
                               stump=Dm01 < Dm05)
  }

  return(loss)
}

f(x=0.9*ht, d = t$d, h = t$h, ht = ht, vol = vol, sll = 1, bnr = u, vis = TRUE)
f(x=0.7*ht, d = t$d, h = t$h, ht = ht, vol = vol, step = 1, sll = 1, bnr = u, vis = TRUE)

h <- t$h
d <- t$d
stp <- 0.1
res <- optimize(f = f(x, d, h, ht, vol), interval = c(kro, ht),
                d=d, h=h, ht=ht, vol=vol, step=stp, sll=1, bnr=u)
res
f(res$minimum, d, h, ht, vol, step=stp, sll = 1, bnr=u, vis = TRUE)


pdf("J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_GAM_v1.pdf")
ll <- list()
uid <- sort(unique(smoothed$id))
# for(i in seq(along=uid)[1:20]){
for(i in seq(along=uid)){
  # i <- 1
  (u <- uid[i])
  t <- subset(smoothed, id==u)
  t <- t[order(t$relH),]
  # head(t)

  ## optimise
  d <- t$d
  h <- t$h
  ht <- t$ht[1]
  kro <- t$KRO[1]
  dhcb <- approx(h, d, xout=kro)$y
  if(any(d>dhcb & h > kro)){
    adjkro <- max(h[d>dhcb & h > kro])
  } else {
    adjkro <- kro
  }
  vol <- t$DhVol[1]
  x <- 0.9*ht
  h7 <- approx(x=d, y=h, xout=7)$y
  minh <- min(h7, adjkro, na.rm=TRUE)
  if(h7 <= adjkro){
    res <- list()
    res$minimum <- h7
    res$objective <- f(h7, d, h, ht, vol, sll = 1, bnr = u, vis = TRUE)
  } else {
    # f(minh, d, h, ht, vol, sll = 1, bnr = u, vis = TRUE)
    res <- try(optimize(f = f(x, d, h, ht, vol, bnr, sll), interval = c(minh, ht),
                        d=d, h=h, ht=ht, vol=vol, bnr=u, step=0.1, sll=1, ),
               silent = TRUE)
  }

  if(!identical(class(res), "try-error")){
    f(res$minimum, d, h, ht, vol, step=.1, sll=1, bnr=u, vis = TRUE)
    ll[[i]] <- as.data.frame(c(id=u, attr(res$objective, "info")))
  } else {
    h7 <- approx(d, h, xout=7)$y
    f(h7, d, h, ht, vol, step=.1, sll=1, bnr=u, vis = TRUE)
  }

  rm(res)
}
dev.off()

res <- do.call(rbind, ll)
res$sum <- rowSums(res[, -1])
colSums(res[res$sum>0, 2:5])
View(res[res$sum>0,])
View(res)

library('staplr')
staplr::select_pages(
  selpages = which(res$negDm>0),
  input_filepath = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_GAM_v1.pdf",
  output_filepath = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_GAM_v1-negDm.pdf")

staplr::select_pages(
  selpages = which(res$diffVol>0),
  input_filepath = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_GAM_v1.pdf",
  output_filepath = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_GAM_v1-diffVol.pdf")

staplr::select_pages(
  selpages = which(res$crownDm>0),
  input_filepath = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_GAM_v1.pdf",
  output_filepath = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_GAM_v1-crownDm.pdf")

staplr::select_pages(
  selpages = which(res$stump>0),
  input_filepath = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_GAM_v1.pdf",
  output_filepath = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_GAM_v1-stump.pdf")

#' problematic taper curve imputation
#' * dhat values below zero at top (eg. 66901, 78504)
#' * DhVol estimate unequal to RBS-DhVol (eg 66201)
#' * too high Dhat values at top (eg. 78404)
#' * stump not with monotonically decreasing dhat values (eg. 79704)
#' * most of DhVol is below crown base and the decrease in Dm must be strong inside crown





