#' in RBS the selected diameter is proportional to d^k, with k equal 2.67 or 2
#' depending on expansion factor, i.e. study.
#' if mean diameter along the stem is to be determined, the probability of
#' selecting a stronger branch than on average is high
#' QUESTION: How to compensate for that?
#' background: when modeling a virtual diameter from expanded volume, it is 
#' necessary to know in which height one can find the true average diameter.

x <- rnorm(100, mean = 30, sd = 4)
x <- runif(1000, min = 5, max = 50)
hist(x)
mean(x)

y <- numeric(length(x))
for(i in seq(along=x)){
  y[i] <- sample(x, size = 1, replace = F, prob = x^2.67) }
mean(y)

mx <- my <- numeric(100)
for(i in 1:100){
  x <- runif(4, min=5, max=50)
  mx[i] <- sample(x, 1, replace = F)
  my[i] <- sample(x, 1, replace = F, prob=x^2.67) }
mean(mx)
mean(my)
