#' on request of Thomas Riedel 23.07.2024 via Email
#'
require(TapeS)
require(dplyr)


## parameters ####
df <- TapeS:::dbtp

ll <- list()
for(i in 1:36){
  par <- as.data.frame(TapeS:::RiPar(ba = BaMap(Ba = i, type = 2)))
  colnames(par) <- c("a", "b", "c")
  df <- cbind(data.frame(spp=i,
                         Ba = tprSpeciesCode(i, "lang"),
                         stempart = 1:4),
              par)
  ll[[i]] <- df
}

res <- do.call(rbind, ll)
write.csv2(res, file="J:/BuI/Persoenlich/Vonderach/TapeS/devel/RiParameters.csv",
           row.names = FALSE)

## functional form ####
d <- 1:100
# doppelte Rindenstärke in mm
b <- res[1, "a"] + res[1, "b"] * d + res[1, "c"] * d^2 # doppelte Rindenstärke in mm
# Graphik
plot(b~d, type="o")

# implementierte Funktion
dbt <- bark(rep(1, length(d)), d, relH=rep(.1, length(d)))
# Graphik
plot(dbt ~ d, las=1, type="l")


# rmarkdown::render("J:/BuI/Persoenlich/Vonderach/TapeS/devel/extract_RiParameters.r",
#                   output_format = "pdf_document")
