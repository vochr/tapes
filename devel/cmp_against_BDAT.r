require(TapeS)
setTapeSoptions(R0 = TRUE) # force Taper curve trough measurements
require(rBDAT)

for(D in c(20, 50, 80, 110, 140)){
  # D <- 80
  Ht <- TapeS::petterson(sp = 15, d13 = D)
  tpr <- TapeS::bdat_as_tprtrees(bdt <- rBDAT::buildTree(list(spp=c(15, 15), D1=D, H=Ht)))
  tpr@spp <- c(15L, 37L) # reset species code
  # tpr
  h <- seq(0, Ht, 0.1)
  d15 <- tprDiameter(tpr[1], Hx=h)
  d37 <- tprDiameter(tpr[2], Hx=h)
  dbdt <- getDiameter(bdt[1,], Hx=h)
  plot(tpr[1], main=paste0("Vergleich spp=15 | spp=37 | BDAT, Bhd=", D), bark=TRUE)
  points(x=h, y=d37, lty=2, type="l")
  points(x=h, y=dbdt, lty=3, type="l")
  points(x=1.3, y=D, cex=2, pch=3)
  legend("topright", legend=c("TapeS on main axis data", "TapeS on BDAT-presmoothing", "good old BDAT"), lty=c(1,2, 3))
}
