#' developing an crown coarse wood expansion for non-monopodial tree species
#' like beech and oak (no data for red oak available)
#'
#' the idea is the following:
#' the tapes-models are fitted on data of the main axis of each tree, no matter
#' of conifer or deciduous tree species. Hence, it is necessary to expand the
#' coarse wood volume ('Derbholz') eventually for deciduous tree species.
#'
#' Expansion factors can be deduced from RBS-sampled data. From this data,
#' models should derived, which allow to expand crown main axis coarse wood
#' (as predicted by tapes) to total crown coarse wood.
#'
#' It is clear, that there are trees holding coarse wood on their branches,
#' hence it is necessary to apply a binomial classification model
#' ("hurdle model") before estimating the branch coarse wood. For this, we can
#' use the hurdle model of the developed biomass functions, which are based on
#' the same data.

## libs ####
require(RODBC)

## data ####
#' we use RBS data, which has been prepared for another question. All RBS-trees
#' werde processed to estimated total coarse wood volume, stem volume up to
#' height of crown base and main axis coarse wood volume. From these, one can
#' calculate crown main axis coarse wood and total crown coarse wood, of which
#' the difference is branch coarse wood volume. One can then estimate shares
#' of branch coarse wood volume against total crown coarse wood volume or
#' against total coarse wood volume.
#'
#' Additionally, we use data from a DBU-project on nutrient availability.
#' Data stems from TUM / LWF and was acquired during EnNa-Project (P01278).

#' RBS-data
p <- "H:/FVA-Projekte/P01697_BWI_TapeR/Daten/Urdaten/Lh_AstDerbholz"
con <- RODBC::odbcConnectExcel2007(file.path(p, "BuI_RBS_Derbholzkomp.xlsx"))
vol <- RODBC::sqlFetch(con, "BuI_RBS_Derbholzkomp")
baum <- RODBC::sqlFetch(con, "tblBaum")
RODBC::odbcCloseAll()

head(vol)
head(baum)
dat <- merge(vol[, c("Baumid", "MwAstDhAnteilKro")],
             baum[, c("Id", "Baumart", "Alter", "BHD", "Höhe", "KRO")],
             by.x = "Baumid", by.y = "Id")
colnames(dat)[which(colnames(dat) == "Höhe")] <- "Hoehe"
colnames(dat)[which(colnames(dat) == "MwAstDhAnteilKro")] <- "AstDhAnteilKro"
dat$origin <- "RBS"
dat$BHD <- dat$BHD / 10 # in cm

#' dbu data
con <- RODBC::odbcConnectExcel2007(file.path(p, "DBU_tapes_Vol.xlsx"))
dbu <- RODBC::sqlFetch(con, "tapes_Vol")
RODBC::odbcCloseAll()

head(dbu)
dbu$Baumid <- 1:nrow(dbu)
dbu$orgin <- "DBU"
dbu$BArt <- ifelse(dbu$BArt == 50, 7, 6)
dbu$Anteil_AstDh_an_KroDh <- ifelse(is.na(dbu$Anteil_AstDh_an_KroDh), 0,
                                    dbu$Anteil_AstDh_an_KroDh)
colnames(dbu) <- c("Projekt_Nr", "BuLand_Nr", "BE_Nr", "BAZiel_nr", "BNR",
                   "Baumart", "Hoehe", "BHD", "KRO", "Vol_StaHADh_m3",
                   "Vol_KroHADh_m3", "Vol_AstDh_m3", "AstDhAnteilKro",
                   "Baumid", "origin")

dat <- rbind(dat[, c("Baumid", "AstDhAnteilKro", "Baumart", "BHD",
                     "Hoehe", "KRO", "origin")],
             dbu[, c("Baumid", "AstDhAnteilKro", "Baumart", "BHD",
                     "Hoehe", "KRO", "origin")])

dat <- subset(dat, AstDhAnteilKro >= 0 & AstDhAnteilKro <= 100)
unique(dat$Baumart)
head(dat)

## data plots ####
for(sp in 6:8){
  for(var in c("BHD", "Hoehe", "KRO")){
    # sp <- 6
    # var <- "BHD"
    ddat <- subset(dat, Baumart == sp)
    ddat$x <- ddat[, var]
    plot(AstDhAnteilKro ~ x, data=ddat, col=ifelse(ddat$origin=="DBU", 3, 2),
         xlab=var,
         main=paste0("Baumart = ", sp),
         pch=ifelse(ddat$AstDhAnteilKro < 1, 3, 16))
    x <- ddat[ddat$AstDhAnteilKro>0, "x"]
    y <- ddat[ddat$AstDhAnteilKro>0, "AstDhAnteilKro"]
    k <- sample(1:10, size = length(x), replace = TRUE)
    for(kk in 1:10){
      i <- which(k != kk)
      lines(smooth.spline(x[i], y[i], spar=1), lty=2)
    }
    abline(m <- lm(y ~ x), col=sp)
    for(o in c("RBS", "DBU")){
      dddat <- ddat[ddat$origin==o & ddat$AstDhAnteilKro>0,]
      if(nrow(dddat)>3){
        abline(m <- lm(AstDhAnteilKro ~ x, data=dddat),
               col=ifelse(o=="DBU", 3, 2), lty=3)
      }
    }
    legend("topleft", legend=c("RBS", "DBU"), pch=16, col=2:3)
    print(summary(m))
  }
}


d6 <- dat[dat$Baumart==6 & dat$AstDhAnteilKro>0,]
m6 <- lm(AstDhAnteilKro ~ BHD + Hoehe + KRO, data=d6)
summary(m6)
par(mfrow=c(2,2))
plot(m6)
d6$yhat <- fitted(m6)
d6$resid <- resid(m6)
par(mfrow=c(1,1))
plot(AstDhAnteilKro ~ BHD, data=d6)
points(x=d6$BHD, y=d6$yhat, pch=3, col=2)
plot(resid ~ BHD, data=d6)
plot(yhat ~ AstDhAnteilKro, data=d6, main="Baumart 6", xlim=c(0,100), ylim=c(0,100),
     xlab="observed", ylab="fitted"); abline(0,1)
Metrics::rmse(d6$AstDhAnteilKro, d6$yhat) # 15.32
Metrics::bias(d6$AstDhAnteilKro, d6$yhat) # 0

d7 <- dat[dat$Baumart==7 & dat$AstDhAnteilKro>0,]
m7 <- lm(AstDhAnteilKro ~ BHD + Hoehe + KRO, data=d7)
summary(m7)
par(mfrow=c(2,2))
plot(m7)
d7$yhat <- fitted(m7)
d7$resid <- resid(m7)
par(mfrow=c(1,1))
plot(AstDhAnteilKro ~ BHD, data=d7)
points(x=d7$BHD, y=d7$yhat, pch=3, col=2)
plot(resid ~ BHD, data=d7)
plot(yhat ~ AstDhAnteilKro, data=d7, main="Baumart 7", xlim=c(0,100), ylim=c(0,100),
     xlab="observed", ylab="fitted"); abline(0,1)
Metrics::rmse(d7$AstDhAnteilKro, d7$yhat) # 16.99
Metrics::bias(d7$AstDhAnteilKro, d7$yhat) # 0

d8 <- dat[dat$Baumart==8 & dat$AstDhAnteilKro>0,]
m8 <- lm(AstDhAnteilKro ~ BHD + Hoehe, data=d8)
summary(m8)
par(mfrow=c(2,2))
plot(m8)
d8$yhat <- fitted(m8)
d8$resid <- resid(m8)
par(mfrow=c(1,1))
plot(AstDhAnteilKro ~ BHD, data=d8)
points(x=d8$BHD, y=d8$yhat, pch=3, col=2)
plot(resid ~ BHD, data=d8)
plot(yhat ~ AstDhAnteilKro, data=d8, main="Baumart 8", xlim=c(0,100), ylim=c(0,100),
     xlab="observed", ylab="fitted"); abline(0,1)
Metrics::rmse(d8$AstDhAnteilKro, d8$yhat) # 15.28
Metrics::bias(d8$AstDhAnteilKro, d8$yhat) # 0


## beta regression ####
require(betareg)
d6$y <- d6$AstDhAnteilKro / 100
b6 <- betareg(y ~ BHD + Hoehe + KRO | BHD, data=d6)
summary(b6)
par(mfrow=c(2,2))
plot(b6)
d6$yhat <- fitted(b6)
d6$resid <- resid(b6)
par(mfrow=c(1,1))
plot(y ~ BHD, data=d6, ylab="AstDhAnteilKro")
points(x=d6$BHD, y=d6$yhat, pch=3, col=2)
plot(resid ~ BHD, data=d6)
plot(yhat ~ y, data=d6, main="Baumart 6", xlim=c(0,1), ylim=c(0,1),
     xlab="observed", ylab="fitted"); abline(0,1)
Metrics::rmse(d6$y, d6$yhat) # 15.35
Metrics::bias(d6$y, d6$yhat) # 0


d7$y <- d7$AstDhAnteilKro / 100
b7 <- betareg(y ~ BHD + Hoehe | 1, data=d7)
summary(b7)
par(mfrow=c(2,2))
plot(b7)
d7$yhat <- fitted(b7)
d7$resid <- resid(b7)
par(mfrow=c(1,1))
plot(y ~ BHD, data=d7, ylab="AstDhAnteilKro")
points(x=d7$BHD, y=d7$yhat, pch=3, col=2)
plot(resid ~ BHD, data=d7)
plot(yhat ~ y, data=d7, main="Baumart 7", xlim=c(0,1), ylim=c(0,1),
     xlab="observed", ylab="fitted"); abline(0,1)
Metrics::rmse(d7$y, d7$yhat) # 17.1
Metrics::bias(d7$y, d7$yhat) # 0.1


d8$y <- d8$AstDhAnteilKro / 100
b8 <- betareg(y ~ Hoehe | 1, data=d8)
summary(b8)
par(mfrow=c(2,2))
plot(b8)
d8$yhat <- fitted(b8)
d8$resid <- resid(b8)
par(mfrow=c(1,1))
plot(y ~ BHD, data=d8, ylab="AstDhAnteilKro")
points(x=d8$BHD, y=d8$yhat, pch=3, col=2)
plot(resid ~ BHD, data=d8)
plot(yhat ~ y, data=d8, main="Baumart 8", xlim=c(0,1), ylim=c(0,1),
     xlab="observed", ylab="fitted"); abline(0,1)
Metrics::rmse(d8$y, d8$yhat) # 15.88
Metrics::bias(d8$y, d8$yhat) # 0.06


## betar-gam ####
require(mgcv)
g6 <- gam(y ~ s(BHD) + s(Hoehe) + s(KRO), family = betar(), data=d6)
summary(g6)
par(mfrow=c(2,2))
plot(g6)
d6$yhat <- fitted(g6)
d6$resid <- resid(g6)
par(mfrow=c(1,1))
plot(y ~ BHD, data=d6, ylab="AstDhAnteilKro")
points(x=d6$BHD, y=d6$yhat, pch=3, col=2)
plot(resid ~ BHD, data=d6)
plot(yhat ~ y, data=d6, main="Baumart 6", xlim=c(0,1), ylim=c(0,1),
     xlab="observed", ylab="fitted"); abline(0,1)
Metrics::rmse(d6$y, d6$yhat) # 13.99
Metrics::bias(d6$y, d6$yhat) # 0.04



g7 <- gam(y ~ s(BHD) + s(Hoehe), family = betar(), data = d7)
summary(g7)
par(mfrow=c(1,2))
plot(g7)
d7$yhat <- fitted(g7)
d7$resid <- resid(g7)
par(mfrow=c(1,1))
plot(y ~ BHD, data=d7, ylab="AstDhAnteilKro")
points(x=d7$BHD, y=d7$yhat, pch=3, col=2)
plot(resid ~ BHD, data=d7)
plot(yhat ~ y, data=d7, main="Baumart 7", xlim=c(0,1), ylim=c(0,1),
     xlab="observed", ylab="fitted"); abline(0,1)
Metrics::rmse(d7$y, d7$yhat) # 16.8
Metrics::bias(d7$y, d7$yhat) # 0.05


g8 <- gam(y ~ s(BHD) + s(Hoehe), family = betar(), data=d8)
summary(g8)
par(mfrow=c(1,2))
plot(g8)
d8$yhat <- fitted(g8)
d8$resid <- resid(g8)
par(mfrow=c(1,1))
plot(y ~ BHD, data=d8, ylab="AstDhAnteilKro")
points(x=d8$BHD, y=d8$yhat, pch=3, col=2)
plot(resid ~ BHD, data=d8)
plot(yhat ~ y, data=d8, main="Baumart 8", xlim=c(0,1), ylim=c(0,1),
     xlab="observed", ylab="fitted"); abline(0,1)
Metrics::rmse(d8$y, d8$yhat) # 15.4
Metrics::bias(d8$y, d8$yhat) # -0.01


## prediction ####
require(iS)
plot(AstDhAnteilKro ~ BHD, data=d6, xlim=c(0,150), ylim=c(0,100),
     xlab="BHD", ylab="Anteil", main="Eiche / LM")
d <- 1:150
qtl <- c(17, 50, 83)
anteil <- seq(0.1, 0.9, 0.2)
for(i in seq(along=qtl)){
  q <- qtl[i]
  for(j in seq(along=anteil)){
    ant <- anteil[j]
    h <- iS::estHeight(d, sp = 17, qtl = q)
    kro <- h * ant
    f <- predict(m6, newdata = data.frame(BHD=d, Hoehe=h, KRO=kro))
    points(x=d, y=f, type="l", col=i, lty=j)
  }
}
legend("bottomright", legend = c(paste0("qtl=", qtl), paste0("HBCr=", anteil)),
       lty=c(1,1,1,1,2,3,4,5), col=c(1,2,3,1,1,1,1,1))

plot(I(AstDhAnteilKro/100) ~ BHD, data=d6, xlim=c(0,150), ylim=c(0,1),
     xlab="BHD", ylab="Anteil", main="Eiche / Beta")
d <- 1:150
qtl <- c(17, 50, 83)
anteil <- seq(0.1, 0.9, 0.2)
for(i in seq(along=qtl)){
  q <- qtl[i]
  for(j in seq(along=anteil)){
    ant <- anteil[j]
    h <- iS::estHeight(d, sp = 17, qtl = q)
    kro <- h * ant
    f <- predict(b6, newdata = data.frame(BHD=d, Hoehe=h, KRO=kro))
    points(x=d, y=f, type="l", col=i, lty=j)
    # f <- predict(b6, newdata = data.frame(BHD=d, Hoehe=h, KRO=kro), type = "quantile", at = c(0.025, 0.975))
    # points(x=d, y=f[, "q_0.025"], type="l", col=i, lty=j)
    # points(x=d, y=f[, "q_0.975"], type="l", col=i, lty=j)
  }
}
legend("bottomright", legend = c(paste0("qtl=", qtl), paste0("HBCr=", anteil)),
       lty=c(1,1,1,1,2,3,4,5), col=c(1,2,3,1,1,1,1,1))

plot(I(AstDhAnteilKro/100) ~ BHD, data=d6, xlim=c(0,150), ylim=c(0,1),
     xlab="BHD", ylab="Anteil", main="Eiche / GAM")
d <- 1:150
qtl <- c(17, 50, 83)
anteil <- seq(0.1, 0.9, 0.2)
for(i in seq(along=qtl)){
  q <- qtl[i]
  for(j in seq(along=anteil)){
    ant <- anteil[j]
    h <- iS::estHeight(d, sp = 17, qtl = q)
    kro <- h * ant
    f <- predict(g6, newdata = data.frame(BHD=d, Hoehe=h, KRO=kro), type="response")
    points(x=d, y=f, type="l", col=i, lty=j)
  }
}
legend("bottomright", legend = c(paste0("qtl=", qtl), paste0("HBCr=", anteil)),
       lty=c(1,1,1,1,2,3,4,5), col=c(1,2,3,1,1,1,1,1))

## beech
require(iS)
plot(AstDhAnteilKro ~ BHD, data=d7, xlim=c(0,150), ylim=c(0,100),
     xlab="BHD", ylab="Anteil", main="Buche / LM")
d <- 1:150
qtl <- c(17, 50, 83)
anteil <- seq(0.1, 0.9, 0.2)
for(i in seq(along=qtl)){
  q <- qtl[i]
  for(j in seq(along=anteil)){
    ant <- anteil[j]
    h <- iS::estHeight(d, sp = 15, qtl = q)
    kro <- h * ant
    f <- predict(m7, newdata = data.frame(BHD=d, Hoehe=h, KRO=kro))
    points(x=d, y=f, type="l", col=i, lty=j)
  }
}
legend("bottomright", legend = c(paste0("qtl=", qtl), paste0("HBCr=", anteil)),
       lty=c(1,1,1,1,2,3,4,5), col=c(1,2,3,1,1,1,1,1))

plot(I(AstDhAnteilKro/100) ~ BHD, data=d7, xlim=c(0,150), ylim=c(0,1),
     xlab="BHD", ylab="Anteil", main="Buche / Beta")
d <- 1:150
qtl <- c(17, 50, 83)
anteil <- seq(0.1, 0.9, 0.2)
for(i in seq(along=qtl)){
  q <- qtl[i]
  for(j in seq(along=anteil)){
    ant <- anteil[j]
    h <- iS::estHeight(d, sp = 15, qtl = q)
    kro <- h * ant
    f <- predict(b7, newdata = data.frame(BHD=d, Hoehe=h, KRO=kro))
    points(x=d, y=f, type="l", col=i, lty=j)
  }
}
legend("bottomright", legend = c(paste0("qtl=", qtl), paste0("HBCr=", anteil)),
       lty=c(1,1,1,1,2,3,4,5), col=c(1,2,3,1,1,1,1,1))

plot(I(AstDhAnteilKro/100) ~ BHD, data=d7, xlim=c(0,150), ylim=c(0,1),
     xlab="BHD", ylab="Anteil", main="Buche / GAM")
d <- 1:150
qtl <- c(17, 50, 83)
anteil <- seq(0.1, 0.9, 0.2)
for(i in seq(along=qtl)){
  q <- qtl[i]
  for(j in seq(along=anteil)){
    ant <- anteil[j]
    h <- iS::estHeight(d, sp = 15, qtl = q)
    kro <- h * ant
    f <- predict(g7, newdata = data.frame(BHD=d, Hoehe=h, KRO=kro), type="response")
    points(x=d, y=f, type="l", col=i, lty=j)
  }
}
legend("bottomright", legend = c(paste0("qtl=", qtl), paste0("HBCr=", anteil)),
       lty=c(1,1,1,1,2,3,4,5), col=c(1,2,3,1,1,1,1,1))


## Anwendung mit TapeS und Vergleich gegen BDAT ####
require(TapeS)
require(rBDAT)
# Beispielbaum
tree <- tprTrees(spp=15, Dm=180, Hm=1.3, Ht=iS::estHeight(180, 15))
plot(tree)
abline(v=.7*iS::estHeight(180, 15))
bt <- buildTree(list(spp=15, D1=180, H=iS::estHeight(180, 15)))
plot(bt)
abline(v=.7*iS::estHeight(180, 15))

## Bhd-Reihe
df <- data.frame(spp=15, D1=10:180)
df$H1 <- 0
df$D2 <- 0
df$H2 <- 50
df$H <- iS::estHeight(df$D1, df$spp)
df$btVol <- getVolume(df)
tmp <- buildTree(df)
class(tmp)
df$tprVol <- TapeS::tprVolume(TapeS::bdat_as_tprtrees(tmp))
head(df)
plot(tprVol ~ btVol, data=df, pch=3, las=1)
abline(0, 1)
plot(btVol ~ D1, data=df, type="l", las=1)
points(x=df$D1, y=df$tprVol, type="l", lty=2)
df$diff <- (df$tprVol - df$btVol) / df$btVol
summary(df$diff)
points(x=df$D1, y=20+100*df$diff, type="l", lty=3)
abline(h=c(15, 20, 25), col="grey", lty=c(2,1,2))
axis(side = 4, at = seq(0, 50, 5), labels = seq(-20, 30, 5), las=1)

## what to do:
# 1 guess if a monopodial tree or not
# if monopodial: OK, accept as is
# if not monopodial
# 2 estimate height of base of crown (hbc)
# 3 estimate share of branche coarse wood

df$hbc <- 0.5*df$H # 0.5 * H = Kronenansatzhöhe
df$tprHADh <- tprVolume(TapeS::bdat_as_tprtrees(tmp),
                        AB = list(A=df$hbc, B=7), iAB = c("H", "dob"))
head(df)
df$AstDhAnteil <- predict(b7, newdata=data.frame(BHD=df$D1, Hoehe=df$H))
df$tprAstDh <- df$AstDhAnteil * df$tprHADh / (1 - df$AstDhAnteil)
df$tprVolkorr <- df$tprVol + df$tprAstDh
plot(btVol ~ D1, data=df, type="l")
points(x=df$D1, y=df$tprVol, type="l", lty=2)
points(x=df$D1, y=df$tprVolkorr, type="l", lty=2, col=2)

plot(df$AstDhAnteil ~ df$D1)
plot(df$tprHADh ~ df$D1)
plot(tprAstDh ~ D1, data=df, type="l")
plot(I(tprAstDh/tprHADh) ~ D1, data=df)


#' it seems that the models predict to high shares for Astderholz-Anteil an
#' KronenDerbholz: if AstDerbholz-Anteil is 99%, then the HADh is leveraged by
#' 1/(1-99), that is by Faktor 100. Way too much.
#' As we do not have further observations - especially with regard to large
#' trees - I first generate some artificial data from a comparison between
#' BDAT-Taper curve and TapeR-Taper curve. Ratio behind that is, that the
#' specific gravity deduced from NSUR biomass model and BDAT-Taper curves look
#' quite sensible, so that at least the proportions between both are meaningful.

# build comprehensive data.frame
ndf <- expand.grid(spp=c(15, 17), D1=seq(20, 180, 1), H1=1.3, D2=0,
                    H2=seq(10, 90, 10))
ndf$H50 <- iS::estHeight(ndf$D1, sp = ndf$spp, qtl = 50)
ndf$H17 <- iS::estHeight(ndf$D1, sp = ndf$spp, qtl = 17)
ndf$H83 <- iS::estHeight(ndf$D1, sp = ndf$spp, qtl = 83)
ndf$sdH <- (ndf$H83 - ndf$H17)/2 # vertrauensintervall +/- 1sd/2sd/3sd
ndf$H <- sapply(1:nrow(ndf), function(a)
  rnorm(1, ndf$H50[a], sd = ndf$sdH[a])
  )
plot(H ~ D1, data=ndf, subset=(spp==17), ylim=c(0, 45))
ndf$H50 <- ndf$H17 <- ndf$H83 <- ndf$sdH <- NULL
View(ndf)

ndf <- buildTree(ndf[, c("spp", "D1", "H1", "D2", "H2", "H")])
ndf$bdtvol <- getVolume(ndf)
ndf$tprvol <- tprVolume(bdat_as_tprtrees(ndf))
ndf$bdtKroVol <- ndf$tprHAVol <- ndf$AstDh <- ndf$AstDhpct <- ndf$hbc <- NA
head(ndf)
pdf("devel/crown_exp_bdat_vs_taper.pdf", paper = "a4r", width = 20, height = 10)
par(mfrow=c(2,2))
for(i in 1:nrow(ndf)){
  tmp <- data.frame(hm=seq(0, ndf[i, "H"], 0.1))
  tmp$bdtDm <- getDiameter(ndf[i, 1:6], Hx=tmp$hm)
  tmp$tprDm <- tprDiameter(tpr <- bdat_as_tprtrees(ndf[i,]), Hx=tmp$hm)
  (maxDmDiff <- max((tmp[tmp$hm > 0.1*ndf[i, "H"],]$bdtDm -
                         tmp[tmp$hm > 0.1*ndf[i, "H"],]$tprDm)))
  if(maxDmDiff > 2){
    ndf$hbc[i] <- tmp$hm[max(which(tmp$bdtDm < (tmp$tprDm) &
                              tmp$hm < floor(ndf[i, "H"])))]
    # ndf$hbc[i] <- ifelse(ndf$hbc[i] < 0.1*ndf[i, "H"] |is.na(ndf[i, "hbc"]),
    #                      0.3*ndf[i, "H"], ndf$hbc[i])
  } else {
    ndf$hbc[i] <- NA
  }

  if(TRUE){
    plot(bdat_as_tprtrees(ndf[i,]))
    points(x=tmp$hm, y=tmp$bdtDm, type="l", lty=2)
    points(x=seq(0, ndf[i, "H"], 0.1),
           y=getDiameter(ndf[i,], Hx=seq(0, ndf[i, "H"], 0.1)), type="l", lty=2)
    points(x=Hm(tpr), y=Dm(tpr), pch=3)
    points(x=Hm(tpr), y = getDiameter(ndf[i, 1:6], Hx = Hm(tpr)))
    abline(v=ndf$hbc[i])
  }
  if(!is.na(ndf$hbc[i])){
    ndf[i, "bdtKroVol"] <- getVolume(ndf[i,], AB = list(A=ndf$hbc[i], B=7), iAB=c("H", "dob"))
    ndf[i, "tprHAVol"] <- tprVolume(bdat_as_tprtrees(ndf[i,]), AB = list(A=ndf$hbc[i], B=7), iAB=c("H", "dob"))
    ndf[i, "AstDh"] <- ndf[i, "bdtKroVol"] - ndf[i, "tprHAVol"]
    ndf[i, "AstDhpct"] <- ndf[i, "AstDh"] / ndf[i, "bdtKroVol"]
  }
  rm(tmp)
}
dev.off()
head(ndf)
ndf$hbcr <- ndf$hbc / ndf$H
summary(ndf$hbcr) #> mean=0.3074, median=0.3097

cols <- rainbow(n=length(unique(ndf$H2)))
par(mfrow=c(2,1))
plot(AstDhpct ~ D1, data=ndf, subset=(spp==15), main="Ba=15", pch=16, col=cols[ndf$H2/10], ylim=c(0,.55))
plot(AstDhpct ~ H, data=ndf, subset=(spp==15), main="Ba=15", pch=16, col=cols[ndf$H2/10], ylim=c(0,.55))
plot(AstDhpct ~ D1, data=ndf, subset=(spp==17), main="Ba=17", pch=16, col=cols[ndf$H2/10], ylim=c(0,.55))
plot(AstDhpct ~ H, data=ndf, subset=(spp==17), main="Ba=17", pch=16, col=cols[ndf$H2/10], ylim=c(0,.55))

ndf$sppf <- as.factor(ndf$spp)
ndf$pred <- NA
require(mgcv)
b15 <- gam(AstDhpct ~ s(D1) + H2 + s(H), data=ndf, subset=(spp==15), family=betar())
summary(b15)
par(mfrow=c(2,2))
gam.check(b15)
ndf[ndf$spp==15,]$pred <- predict(b15, newdata=ndf[ndf$spp==15,], type="response")

b17 <- gam(AstDhpct ~ s(D1) + H2 + s(H), data=ndf, subset=(spp==17), family=betar())
summary(b17)
par(mfrow=c(2,2))
gam.check(b17)
ndf[ndf$spp==17,]$pred <- predict(b17, newdata=ndf[ndf$spp==17,], type="response")

par(mfrow=c(1,1))
plot(pred ~ AstDhpct, data=ndf, subset=(spp==15), col=cols[ndf$H2/10])
abline(0, 1, col=2, lwd=2)
plot(pred ~ AstDhpct, data=ndf, subset=(spp==17), col=cols[ndf$H2/10])
abline(0, 1, col=2, lwd=2)
mean(e <- ndf$pred - ndf$AstDhpct, na.rm=TRUE) # bias
sqrt(mean(e^2, na.rm = TRUE)) # rmse

for(i in sort(unique(ndf$H2))){
  # i <- 10
  plot(AstDhpct ~ D1, data=ndf, subset=(spp==15 & H2==i), pch=16,
       col=cols[ndf$H2/10], ylim=c(0,0.5), xlim=c(0, 180),
       main=paste0("BA=15, H2=", i))
  points(x = ndf[ndf$spp==15 & ndf$H2==i,]$D1,
         y=ndf[ndf$spp==15 & ndf$H2==i,]$pred,
         pch=3, col=cols[ndf[ndf$spp==15 & ndf$H2==i,]$H2/10])
}

for(i in sort(unique(ndf$H2))){
  plot(AstDhpct ~ D1, data=ndf, subset=(spp==17 & H2==i), pch=16,
       col=cols[ndf$H2/10], ylim=c(0, 0.5), xlim=c(0, 180),
       main=paste0("BA=17, H2=", i))
  points(x = ndf[ndf$spp==17 & ndf$H2==i,]$D1,
         y=ndf[ndf$spp==17 & ndf$H2==i,]$pred,
         pch=3, col=cols[ndf[ndf$spp==15 & ndf$H2==i,]$H2/10])
  o <- ndf[ndf$spp==17 & ndf$H2==i,]$AstDhpct
  p <- ndf[ndf$spp==17 & ndf$H2==i,]$pred
  print(Metrics::bias(o[!is.na(o)], p[!is.na(o)]))
}

## check out the developed models
ndf$AstDhAnteil <- NA
ndf$hbc <- ifelse(is.na(ndf$hbc), 0.5*ndf$H, ndf$hbc) # 0.5 * H = Kronenansatzhöhe
ndf$tprHADh <- sapply(1:nrow(ndf), function(a){
  tprVolume(TapeS::bdat_as_tprtrees(ndf[a,]), AB = list(A=ndf[a,]$hbc, B=7),
            iAB = c("H", "dob"))
  })
ndf$AstDhAnteil <- ndf$pred
summary(ndf$AstDhAnteil)
ndf$tprAstDh <- ndf$AstDhAnteil * ndf$tprHADh / (1 - ndf$AstDhAnteil)
ndf$tprKroVol <- ndf$tprHADh + ndf$tprAstDh
ndf$tprVolkorr <- ndf$tprvol + ndf$tprAstDh
par(mfrow=c(2,1))
for(h2 in seq(10, 90, 10)){
  for(sp in c(15, 17)){
    plot(bdtvol ~ D1, data=ndf, subset=(spp==sp & H2==h2), main=paste0("spp=", sp, ", H2=", h2),
         xlim=c(0, 200), ylim=c(0, 60))
    lines(smooth.spline(x=ndf[ndf$spp==sp & ndf$H2==h2,]$D1,
                        y=ndf[ndf$spp==sp & ndf$H2==h2,]$bdtvol), lwd=2, col=2)
    points(x=ndf[ndf$spp==sp & ndf$H2==h2,]$D1, y=ndf[ndf$spp==sp & ndf$H2==h2,]$tprvol, pch=3)
    lines(smooth.spline(x=ndf[ndf$spp==sp & ndf$H2==h2,]$D1,
                        y=ndf[ndf$spp==sp & ndf$H2==h2,]$tprvol), lwd=2, col=3)
    points(x=ndf[ndf$spp==sp & ndf$H2==h2,]$D1, y=ndf[ndf$spp==sp & ndf$H2==h2,]$tprVolkorr, pch=4, col=4)
    lines(smooth.spline(x=ndf[ndf$spp==sp & ndf$H2==h2 & !is.na(ndf$tprVolkorr),]$D1,
                        y=ndf[ndf$spp==sp & ndf$H2==h2 & !is.na(ndf$tprVolkorr),]$tprVolkorr), lwd=2, col=4)
    legend("topleft", legend=c("bdat", "taper", "taper-corr"), lty=1, lwd=2, col=c(2, 3, 4))
    # only crown
    plot(bdtKroVol ~ D1, data=ndf, subset=(spp==sp & H2==h2), main=paste0("spp=", sp, ", H2=", h2),
         xlim=c(0, 200), ylim=c(0, 60))
    lines(smooth.spline(x=ndf[ndf$spp==sp & ndf$H2==h2 & !is.na(ndf$bdtKroVol),]$D1,
                        y=ndf[ndf$spp==sp & ndf$H2==h2 & !is.na(ndf$bdtKroVol),]$bdtKroVol), lwd=2, col=2)
    points(x=ndf[ndf$spp==sp & ndf$H2==h2,]$D1, y=ndf[ndf$spp==sp & ndf$H2==h2,]$tprHADh, pch=3)
    lines(smooth.spline(x=ndf[ndf$spp==sp & ndf$H2==h2,]$D1,
                        y=ndf[ndf$spp==sp & ndf$H2==h2,]$tprHADh), lwd=2, col=3)
    points(x=ndf[ndf$spp==sp & ndf$H2==h2,]$D1, y=ndf[ndf$spp==sp & ndf$H2==h2,]$tprKroVol, pch=4, col=4)
    lines(smooth.spline(x=ndf[ndf$spp==sp & ndf$H2==h2 & !is.na(ndf$tprVolkorr),]$D1,
                        y=ndf[ndf$spp==sp & ndf$H2==h2 & !is.na(ndf$tprVolkorr),]$tprKroVol), lwd=2, col=4)
    # legend("topleft", legend=c("bdat", "taper", "taper-corr"), lty=1, lwd=2, col=c(2, 3, 4))
  }
}

#' check of a parametric models is equally suited
#' would be easier to be applied in different situations.
plot(AstDhpct ~ D1, data=ndf, subset=(spp==15),
     col=cols[ndf[ndf$spp==15,]$H2/10], pch=16, ylim=c(0,0.55))
points(x=ndf[ndf$spp==15,]$D1, y=predict(b15, newdata=ndf[ndf$spp==15,],
                                         type="response"))
require(nlme)
tmp <- ndf[!is.na(ndf$AstDhpct) & ndf$spp==15 & ndf$H2==50,]
n15 <- nls(AstDhpct ~ SSlogis(D1, Asym, xmid, scal),
           data=tmp,
           start=c(Asym=0.5, xmid=110, scal=1))
coef(n15)
plot(AstDhpct ~ D1, data=tmp,
     col=cols[tmp$H2/10], pch=16, ylim=c(0,0.55))
points(x=tmp$D1, y=predict(n15), pch=3)
m15 <- gnls(AstDhpct ~ SSlogis(D1, Asym, xmid, scal),
            data = tmp,
            params = list(Asym + xmid + scal ~ 1),
            start = c(Asym=0.5, xmid=100, scal=35))
points(x=tmp$D1, y=predict(m15), pch=4)

tmp <- ndf[!is.na(ndf$AstDhpct) & ndf$spp==15,]
plot(AstDhpct ~ D1, data=tmp,
     col=cols[tmp$H2/10], pch=16, ylim=c(0,0.55))
coef <- list()
for(i in sort(unique(ndf$H2))){
  tmp <- ndf[!is.na(ndf$AstDhpct) & ndf$spp==15 & ndf$H2==i,]
  m15b <- gnls(AstDhpct ~ SSlogis(D1, Asym, xmid, scal),
               data = tmp,
               params = list(Asym ~ 1, xmid+scal~1),
               start = c(Asym=c(0.5), xmid=c(100), scal=c(35)))
  coef[[i]] <- coef(m15b)
  points(x=tmp$D1, y=predict(m15b), type="l")
}
coef <- matrix(unlist(coef), ncol = 3, byrow = TRUE)
plot(coef[, 1]~sort(unique(ndf$H2)))
plot(coef[, 2]~sort(unique(ndf$H2)))
plot(coef[, 3]~sort(unique(ndf$H2)))

tmp <- ndf[!is.na(ndf$AstDhpct) & ndf$spp==15 & ndf$H2==50,]
n15b <- nls(AstDhpct ~ ylag + Asym / (1 + exp(-(D1 - xmid) / scal)),
            data=tmp,
            start=c(Asym = 0.5, xmid= 100, scal=1, ylag=-0.1))
coef(n15)
coef(n15b)

plot(AstDhpct ~ D1, data=tmp,
     col=cols[tmp$H2/10], pch=16, ylim=c(0, 0.55), xlim=c(0, 200))
points(x=tmp$D1, y=predict(n15b), pch=3)
x <- seq(0, 200, 1)
y <- predict(n15b, newdata = data.frame(D1=x))
points(x=x, y=ifelse(y<0, 0, y), pch=3, type="l")

#> a 4-parameter nls model does properly capture the (mean) AstDhpct
#> but still, the question is about *correct* crown volume still applies.
#> BDAT is not necessarily correct.


#' depending on the taper form ("formigkeit") the results look quite OK
#' nevertheless, it seems like there is a small overestimation, which comes from
#' the fact, that the BDAT-form shows smaller diameter below crown base as a
#' consequence of the "wave" over crown base.
#' Maybe, correct the complete volume instead of only correcting above crown
#' base.

# build a second comprehensive data.frame
ndf <- expand.grid(spp=c(15, 17), D1=seq(20, 180, 2), H1=1.3, D2=0,
                   H2=seq(10, 90, 10))
ndf$H50 <- iS::estHeight(ndf$D1, sp = ndf$spp, qtl = 50)
ndf$H17 <- iS::estHeight(ndf$D1, sp = ndf$spp, qtl = 17)
ndf$H83 <- iS::estHeight(ndf$D1, sp = ndf$spp, qtl = 83)
ndf$sdH <- (ndf$H83 - ndf$H17)/2 # vertrauensintervall +/- 1sd/2sd/3sd
ndf$H <- sapply(1:nrow(ndf), function(a) rnorm(1, ndf$H50[a], sd = ndf$sdH[a]))
ndf$H50 <- ndf$H17 <- ndf$H83 <- ndf$sdH <- NULL

ndf <- buildTree(ndf)
ndf$bdtvol <- getVolume(ndf)
ndf$tprvol <- tprVolume(bdat_as_tprtrees(ndf))
ndf$bdtKroVol <- ndf$tprHAVol <- ndf$AstDh <- ndf$AstDhpct <- ndf$hbc <- NA
head(ndf)
pdf("devel/crown_exp_bdat_vs_taper_compare_totalVol.pdf", paper = "a4r", width = 20, height = 10)
par(mfrow=c(2,2))
for(i in 1:nrow(ndf)){
  tmp <- data.frame(hm=seq(0, ndf[i, "H"], 0.1))
  tmp$bdtDm <- getDiameter(ndf[i,], Hx=tmp$hm)
  tmp$tprDm <- tprDiameter(bdat_as_tprtrees(ndf[i,]), Hx=tmp$hm)
  maxDmDiff <- max((tmp[tmp$hm > 0.1*ndf[i, "H"],]$bdtDm -
                      tmp[tmp$hm > 0.1*ndf[i, "H"],]$tprDm))
  if(maxDmDiff > 2){
    ndf$hbc[i] <- tmp$hm[max(which(tmp$bdtDm < tmp$tprDm &
                                     tmp$hm < floor(ndf[i, "H"])))]
  } else {
    ndf$hbc[i] <- NA
  }

  if(TRUE){
    plot(bdat_as_tprtrees(ndf[i,]))
    points(x=seq(0, 40, 0.1), y=getDiameter(ndf[i,], Hx=seq(0, 40, 0.1)), type="l", lty=2)
    abline(v=ndf$hbc[i])
  }
  if(!is.na(ndf$hbc[i])){
    ndf[i, "bdtKroVol"] <- getVolume(ndf[i,], AB = list(A=0.01*ndf$H[i], B=7), iAB=c("H", "dob"))
    # ndf[i, "bdtKroVol"] <- getVolume(ndf[i,], AB = list(A=ndf$hbc[i], B=7), iAB=c("H", "dob"))
    ndf[i, "tprHAVol"] <- tprVolume(bdat_as_tprtrees(ndf[i,]), AB = list(A=0.01*ndf$H[i], B=7), iAB=c("H", "dob"))
    # ndf[i, "tprHAVol"] <- tprVolume(bdat_as_tprtrees(ndf[i,]), AB = list(A=ndf$hbc[i], B=7), iAB=c("H", "dob"))
    ndf[i, "AstDh"] <- ndf[i, "bdtKroVol"] - ndf[i, "tprHAVol"]
    ndf[i, "AstDhpct"] <- ndf[i, "AstDh"] / ndf[i, "bdtKroVol"]
  }
}
dev.off()
head(ndf)
plot(AstDhpct ~ D1, data=ndf, subset=(spp==15), pch=16, col=cols[ndf$H2/10])
plot(AstDhpct ~ H, data=ndf, subset=(spp==15), pch=16, col=cols[ndf$H2/10])
plot(AstDhpct ~ D1, data=ndf, subset=(spp==17), pch=3, col=cols[ndf$H2/10])
plot(AstDhpct ~ H, data=ndf, subset=(spp==17), pch=3, col=cols[ndf$H2/10])

ndf$sppf <- as.factor(ndf$spp)
b <- gam(AstDhpct ~ s(D1) + H2 + s(H) + sppf, data=ndf, family=betar())
summary(b)
ndf$pred <- sapply(1:nrow(ndf), function(a) predict(b, newdata=ndf[a,], type="response"))
plot(pred ~ AstDhpct, data=ndf, col=cols[ndf$H2/10])
abline(0,1, col="red", lwd=2)
m <- lm(AstDhpct ~ D1 + H + H2 + sppf, data=ndf)
summary(m)
par(mfrow=c(2,2))
plot(m)
par(mfrow=c(1,1))
# ndf$pred <- sapply(1:nrow(ndf), function(a) predict(m, newdata=ndf[a,]))
# ndf$pred <- ifelse(ndf$pred < 0, 0, ndf$pred)

plot(AstDhpct ~ D1, data=ndf, subset=(spp==15), pch=16, col=cols[ndf$H2/10],
     ylim=c(0,0.3), xlim=c(0, 200))
points(x = ndf[ndf$spp==15,]$D1, y=ndf[ndf$spp==15,]$pred, pch=3,
       col=cols[ndf[ndf$spp==15,]$H2/10])
abline(lm(AstDhpct ~ D1, data=ndf, subset=(spp==15)), lty=1, col=2, lwd=2)
abline(h=0)

plot(AstDhpct ~ D1, data=ndf, subset=(spp==17), pch=16, col=cols[ndf$H2/10],
     ylim=c(0,0.3), xlim=c(0, 200))
points(x = ndf[ndf$spp==17,]$D1, y=ndf[ndf$spp==17,]$pred, pch=3,
       col=cols[ndf[ndf$spp==17,]$H2/10])
abline(lm(AstDhpct ~ D1, data=ndf, subset=(spp==17)), lty=1, col=2, lwd=2)
abline(h=0)

## check out the developed models
ndf$AstDhAnteil <- NA
ndf$hbc <- ifelse(is.na(ndf$hbc), 0.5*ndf$H, ndf$hbc) # 0.5 * H = Kronenansatzhöhe
ndf$tprHADh <- tprVolume(TapeS::bdat_as_tprtrees(ndf),
                         AB = list(A=0.01*ndf$H, B=7), iAB = c("H", "dob"))
# ndf[!is.na(ndf$AstDh),]$AstDhAnteil <- predict(m)
ndf[!is.na(ndf$AstDh),]$AstDhAnteil <- predict(b, type="response")
ndf$tprAstDh <- ndf$AstDhAnteil * ndf$tprHADh / (1 - ndf$AstDhAnteil)
ndf$tprKroVol <- ndf$tprHADh + ndf$tprAstDh
ndf$tprVolkorr <- ndf$tprvol + ndf$tprAstDh
par(mfrow=c(2,1))
for(h2 in seq(10, 90, 10)){
  for(sp in c(15, 17)){
    # total volume
    plot(bdtvol ~ D1, data=ndf, subset=(spp==sp & H2==h2), main=paste0("spp=", sp, ", H2=", h2))
    lines(smooth.spline(x=ndf[ndf$spp==sp & ndf$H2==h2,]$D1,
                        y=ndf[ndf$spp==sp & ndf$H2==h2,]$bdtvol), lwd=2, col=2)
    points(x=ndf[ndf$spp==sp & ndf$H2==h2,]$D1, y=ndf[ndf$spp==sp & ndf$H2==h2,]$tprvol, pch=3)
    lines(smooth.spline(x=ndf[ndf$spp==sp & ndf$H2==h2,]$D1,
                        y=ndf[ndf$spp==sp & ndf$H2==h2,]$tprvol), lwd=2, col=3)
    points(x=ndf[ndf$spp==sp & ndf$H2==h2,]$D1, y=ndf[ndf$spp==sp & ndf$H2==h2,]$tprVolkorr, pch=4, col=4)
    lines(smooth.spline(x=ndf[ndf$spp==sp & ndf$H2==h2 & !is.na(ndf$tprVolkorr),]$D1,
                        y=ndf[ndf$spp==sp & ndf$H2==h2 & !is.na(ndf$tprVolkorr),]$tprVolkorr), lwd=2, col=4)
    legend("topleft", legend=c("bdat", "taper", "taper-corr"), lty=1, lwd=2, col=c(2, 3, 4))
    # only crown
    plot(bdtKroVol ~ D1, data=ndf, subset=(spp==sp & H2==h2), main=paste0("spp=", sp, ", H2=", h2))
    lines(smooth.spline(x=ndf[ndf$spp==sp & ndf$H2==h2 & !is.na(ndf$bdtKroVol),]$D1,
                        y=ndf[ndf$spp==sp & ndf$H2==h2 & !is.na(ndf$bdtKroVol),]$bdtKroVol), lwd=2, col=2)
    points(x=ndf[ndf$spp==sp & ndf$H2==h2,]$D1, y=ndf[ndf$spp==sp & ndf$H2==h2,]$tprHADh, pch=3)
    lines(smooth.spline(x=ndf[ndf$spp==sp & ndf$H2==h2,]$D1,
                        y=ndf[ndf$spp==sp & ndf$H2==h2,]$tprHADh), lwd=2, col=3)
    points(x=ndf[ndf$spp==sp & ndf$H2==h2,]$D1, y=ndf[ndf$spp==sp & ndf$H2==h2,]$tprKroVol, pch=4, col=4)
    lines(smooth.spline(x=ndf[ndf$spp==sp & ndf$H2==h2 & !is.na(ndf$tprVolkorr),]$D1,
                        y=ndf[ndf$spp==sp & ndf$H2==h2 & !is.na(ndf$tprVolkorr),]$tprKroVol), lwd=2, col=4)
    legend("topleft", legend=c("bdat", "taper", "taper-corr"), lty=1, lwd=2, col=c(2, 3, 4))
  }
}

#' looks quite OK, although the deviations are small they might depend on the
#' actual "drawn" observations...
#' but actually the results are worse than when only modelling the crown part
#' so: save the gam-models b15 and b17

save(b15, b17, file = "H:/BuI/Persoenlich/Vonderach/TapeS/data/crownExpansion.rdata")
