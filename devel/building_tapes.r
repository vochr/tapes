# script to build and install current state of package
pkgname <- "TapeS"
Rcpp::compileAttributes()
roxygen2::roxygenise()
detach("package:TapeS", unload=TRUE)
remove.packages(pkgname)
p <- file.path("~/Documents/FVA", pkgname)
system(paste0("cd ", p))

## BUILD
system("R CMD build .")

## INSTALL
version <- read.dcf(file.path(p, "DESCRIPTION"), "Version")
tarfile <- paste0(pkgname, "_", version, ".tar.gz")
if(file.exists(file.path(p, tarfile))){
  system(paste0("R CMD INSTALL ", tarfile))
}

## Test if can be loaded and functions be called
eval(parse(text=paste0("require(", pkgname, ")")))
require(pkgname, character.only = TRUE)
works <- try(simTrees(), silent = TRUE)
if(class(works) == "try-error"){
  stop("Build & install process *NOT* successful!")
}
