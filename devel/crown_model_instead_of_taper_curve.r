#' developing an crown volume models for replacement of taper curve in
#' deciduous tree species crowns.
#'
# p <- "H:/BuI/Persoenlich/Vonderach/TapeS/devel"
# rmarkdown::render(file.path(p, "crown_model_instead_of_taper_curve.r"),
#                   output_format = "pdf_document")
require(RODBC)

## data ####
#' we use RBS data, which has been prepared for another question. All RBS-trees
#' werde processed to estimated total coarse wood volume, stem volume up to
#' height of crown base and crown coarse wood volume.
#'
#' Additionally, we use data from a DBU-project on nutrient availability.
#' Data stems from TUM / LWF and was acquired during EnNa-Project (P01278).

#' RBS-data
p <- "H:/FVA-Projekte/P01697_BWI_TapeR/Daten/Urdaten/Lh_AstDerbholz"
con <- RODBC::odbcConnectExcel2007(file.path(p, "BuI_RBS_Derbholzkomp.xlsx"))
vol <- RODBC::sqlFetch(con, "BuI_RBS_Derbholzkomp")
baum <- RODBC::sqlFetch(con, "tblBaum")
RODBC::odbcCloseAll()

head(vol)
head(baum)
dat <- merge(vol[, c("Baumid", "MwVolGrenz", "MwVolStamm")],
             baum[, c("Id", "Baumart", "Alter", "BHD", "Höhe", "KRO" , "b_D7")],
             by.x = "Baumid", by.y = "Id")
colnames(dat)[which(colnames(dat) == "Höhe")] <- "Hoehe"
colnames(dat)[which(colnames(dat) == "MwVolGrenz")] <- "DhVol"
colnames(dat)[which(colnames(dat) == "MwVolStamm")] <- "DhSta"
colnames(dat)[which(colnames(dat) == "b_D7")] <- "D7"
dat$origin <- "RBS"
dat$BHD <- dat$BHD / 10 # in cm
dat$D7 <- ifelse(dat$D7 <= 0, NA, dat$D7 / 10) # in cm

#' dbu data
con <- RODBC::odbcConnectExcel2007(file.path(p, "DBU_tapes_Vol.xlsx"))
dbu <- RODBC::sqlFetch(con, "tapes_Vol")
RODBC::odbcCloseAll()
head(dbu)

con <- RODBC::odbcConnectExcel2007(file.path(p, "DBU_040-Hauptachse.xlsx"))
ha <- RODBC::sqlFetch(con, "040-Hauptachse") # to infer D03
RODBC::odbcCloseAll()
head(ha)

key <- c("Projekt_Nr", "BuLand_Nr", "BE_Nr", "BAZiel_nr", "BNR", "BArt")
dbu$d7 <- NA
for(i in 1:nrow(dbu)){
  # i <- 1
  h <- dbu[i, "Hoehe_m"]
  tmp <-merge(dbu[i, key], ha[, c(key, "h_m", "d_mR_cm")], by=key)
  tmp <- tmp[order(tmp$h_m),]
  h <- ifelse(is.na(h), max(tmp$h_m), h)
  if(nrow(tmp) >= 3 & max(c(tmp$h_m), 0) > 7 & min(c(tmp$h_m), h) < 7){
    dbu[i, "d7"] <- approx(x = tmp$h_m, y = tmp$d_mR_cm, xout = 7)$y
  }
}

dbu$Baumid <- 1:nrow(dbu)
dbu$orgin <- "DBU"
dbu$BArt <- ifelse(dbu$BArt == 50, 7, 6)
dbu$DhVol <- ifelse(is.na(dbu$Vol_StaHADh_m3), 0, dbu$Vol_StaHADh_m3) +
  ifelse(is.na(dbu$Vol_KroHADh_m3), 0, dbu$Vol_KroHADh_m3) +
  ifelse(is.na(dbu$Vol_AstDh_m3), 0, dbu$Vol_AstDh_m3)
dbu$DhSta <- ifelse(is.na(dbu$Vol_StaHADh_m3), 0, dbu$Vol_StaHADh_m3)
colnames(dbu) <- c("Projekt_Nr", "BuLand_Nr", "BE_Nr", "BAZiel_nr", "BNR",
                   "Baumart", "Hoehe", "BHD", "KRO", "Vol_StaHADh_m3",
                   "Vol_KroHADh_m3", "Vol_AstDh_m3", "AstDhAnteilKro",
                   "D7", "Baumid", "origin", "DhVol", "DhSta")

dat <- rbind(dat[, c("Baumid", "DhVol", "DhSta", "Baumart", "BHD", "D7",
                     "Hoehe", "KRO", "origin")],
             dbu[, c("Baumid", "DhVol", "DhSta", "Baumart", "BHD", "D7",
                     "Hoehe", "KRO", "origin")])

dat$KroVol <- dat$DhVol - dat$DhSta
dat$KroVolpct <- dat$KroVol / dat$DhSta
dat$rKRO <- dat$KRO / dat$Hoehe
par(mfrow=c(1,1))
hist(dat[dat$Baumart==7,]$KroVolpct, main="Buche, KroDh / StaDh")
hist(dat[dat$Baumart==6,]$KroVolpct, main="Eiche, KroDh / StaDh")
dat <- subset(dat, KroVolpct >= 0 & !is.na(KroVolpct) &
                !is.na(Hoehe) & Baumart != 8)
dat2 <- subset(dat, !is.na(D7))
unique(dat$Baumart)
head(dat)
summary(dat$q7 <- dat$D7 / dat$BHD)

plot(KroVolpct ~ BHD, data=dat, pch=ifelse(KroVolpct <= 0, 3, 1), col=dat$Baumart-5)
lines(smooth.spline(dat$BHD, dat$KroVolpct, spar=1), lty=2, lwd=2, col=2)
abline(lm(KroVolpct ~ BHD, data=dat))
plot(KroVolpct ~ D7, data=dat, pch=ifelse(KroVolpct <= 0, 3, 1))
lines(smooth.spline(dat2$D7, dat2$KroVolpct, spar=1), lty=2, lwd=2, col=2)
abline(lm(KroVolpct ~ D7, data=dat))
plot(KroVolpct ~ Hoehe, data=dat, pch=ifelse(KroVolpct <= 0, 3, 1))
lines(smooth.spline(dat$Hoehe, dat$KroVolpct, spar=1), lty=2, lwd=2, col=2)
abline(lm(KroVolpct ~ Hoehe, data=dat))
plot(KroVolpct ~ KRO, data=dat, pch=ifelse(KroVolpct <= 0, 3, 1))
lines(smooth.spline(dat$KRO, dat$KroVolpct, spar=1), lty=2, lwd=2, col=2)
abline(lm(KroVolpct ~ KRO, data=dat))
plot(KroVolpct ~ rKRO, data=dat, pch=ifelse(KroVolpct <= 0, 3, 1))
lines(smooth.spline(dat$rKRO, dat$KroVolpct, spar=1), lty=2, lwd=2, col=2)
abline(lm(KroVolpct ~ rKRO, data=dat))

## build a model to predict crown volume ####
require(mgcv)
head(dat)
b15 <- gam(KroVolpct ~ BHD + Hoehe + s(rKRO, k = 20) - 1, data=dat2,
           subset=(Baumart==7 & KroVolpct>0), weights = 1/BHD^2)
par(mfrow=c(2,2))
gam.check(b15)
abline(0, 1)
summary(b15)

b17 <- gam(KroVolpct ~ BHD + Hoehe + s(rKRO) - 1, data=dat,
           subset=(Baumart==6 & KroVolpct>0), weights = 1/BHD^2)
par(mfrow=c(2,2))
gam.check(b17)
abline(0, 1)
summary(b17)
dat$estKroVolPct <- ifelse(dat$Baumart == 6,
                           predict(b17, newdata=dat, type = "response"),
                           predict(b15, newdata=dat, type = "response"))
dat$pred <- dat$DhSta * (1 + dat$estKroVolPct)
par(mfrow=c(1, 1))
plot(pred ~ DhVol, data=dat, subset=(Baumart==6 & !is.na(D7)),
     pch=ifelse(dat$Baumart==6, 3, 1), main="Eiche")
abline(0,1)
plot(pred ~ DhVol, data=dat, subset=(Baumart==7 & !is.na(D7)),
     pch=ifelse(dat$Baumart==6, 3, 1), main="Buche")
abline(0,1)

plot(estKroVolPct ~ KroVolpct, data=dat, subset = (Baumart==7),
     pch=ifelse(!is.na(dat$D7), 1, 3))
abline(0, 1)


dat$e <- dat$pred - dat$DhVol
dat$er <- (dat$pred - dat$DhVol) / dat$DhVol
aggregate(dat$e, by=list(dat$Baumart), FUN=function(x) sqrt(mean(x^2))) # rmse
aggregate(dat$e, by=list(dat$Baumart), FUN="mean", na.rm=TRUE) # bias
aggregate(dat$er, by=list(dat$Baumart), FUN=function(x) sqrt(mean(x^2, na.rm=TRUE))) # rmse
aggregate(dat$er, by=list(dat$Baumart), FUN="mean", na.rm=TRUE) # bias


## compare measured total volume against  taper curves of BDAT and TapeR ####
require(TapeS)
require(rBDAT)
dat$BaBDAT <- ifelse(dat$Baumart == 6, 17, ifelse(dat$Baumart==7, 15, dat$Baumart))
dat$bdtVol <- getVolume(list(spp=dat$BaBDAT, D1=dat$BHD, D2=ifelse(is.na(dat$D7), 0, dat$D7),
                             H2=ifelse(is.na(dat$D7), 50, 7), H=dat$Hoehe))
dat$tprVol <- sapply(1:nrow(dat), function(a){
  # a <- 1
  tpr <- bdat_as_tprtrees(buildTree(list(spp=dat[a,]$BaBDAT, D1=dat[a,]$BHD,
                                         D2=ifelse(is.na(dat[a,]$D7), 0, dat[a,]$D7),
                                         H2=ifelse(is.na(dat[a,]$D7), 50, 7),
                                         H=dat[a,]$Hoehe)))
  vol <- try(tprVolume(tpr), silent = TRUE)
  if(class(vol) == "try-error") vol <- NA
  return(vol)
})
dat$tprVol37 <- sapply(1:nrow(dat), function(a){
  # a <- 1
  tpr <- bdat_as_tprtrees(buildTree(list(spp=dat[a,]$BaBDAT, D1=dat[a,]$BHD,
                                         D2=ifelse(is.na(dat[a,]$D7), 0, dat[a,]$D7),
                                         H2=ifelse(is.na(dat[a,]$D7), 50, 7),
                                         H=dat[a,]$Hoehe)))
  spp(tpr) <- 37 # estimte volume according to relBDAT-TapeR-curves
  vol <- try(tprVolume(tpr), silent = TRUE)
  if(class(vol) == "try-error") vol <- NA
  return(vol)
})

par(mfrow=c(1,1))
for(sp in unique(dat$Baumart)){
  tmp <- subset(dat, Baumart == sp)
  plot(bdtVol ~ BHD, data=tmp, main=paste0("BA=", sp),
       ylim=c(0, max(tmp[, c("bdtVol", "tprVol", "tprVol37", "pred")], na.rm = TRUE)))
  points(x=tmp$BHD, y=tmp$tprVol, pch=3)
  points(x=tmp$BHD, y=tmp$tprVol37, pch=4)
  points(x=tmp$BHD, y=tmp$DhVol, pch=5)
  points(x=tmp$BHD, y=tmp$pred, pch=6)
  legend("topleft", legend = c("BDAT", "TapeS", "relBDAT-TapeR", "StaDh-Exp", "obs"),
         pch=c(1, 3, 4, 6, 5))

  plot(I(bdtVol - DhVol) ~ BHD, data=tmp, main=paste0("BA=", sp), ylab="error",
       ylim=range(tmp[, c("bdtVol", "tprVol", "tprVol37", "pred")] - tmp$DhVol, na.rm=TRUE))
  abline(h=0, col="grey")
  points(x=tmp$BHD, y=tmp$tprVol - tmp$DhVol, pch=3)
  points(x=tmp$BHD, y=tmp$tprVol37 - tmp$DhVol, pch=4)
  points(x=tmp$BHD, y=tmp$pred - tmp$DhVol, pch=6)
  legend("topleft", legend = c("BDAT", "TapeS", "relBDAT-TapeR", "StaDh-Exp"),
         pch=c(1, 3, 4, 6), title = "Obs - pred")

  plot(bdtVol ~ DhVol, data=tmp, main=paste0("BA=", sp), xlab="observed",
       ylab = "fitted",
       ylim=c(0, max(tmp[, c("bdtVol", "tprVol", "tprVol37", "pred")], na.rm = TRUE)))
  abline(0, 1)
  points(x=tmp$DhVol, y=tmp$tprVol, pch=3)
  points(x=tmp$DhVol, y=tmp$tprVol37, pch=4)
  points(x=tmp$DhVol, y=tmp$pred, pch=6)
  legend("topleft", legend = c("BDAT", "TapeS", "relBDAT-TapeR", "StaDh-Exp"),
         pch=c(1, 3, 4, 6))

  df <- as.data.frame(matrix(NA, nrow=7, ncol=14))
  colnames(df) <- c("minBhd", "n", "bdtKS", "tprKS", "tprKS37", "GAMExpKS",
                    "bdtRMSE", "tprRMSE", "tprRMSE37", "GAMExpRMSE",
                    "bdtBIAS", "tprBIAS", "tprBIAS37", "GAMExpBIAS")
  i <- 0
  for(minBhd in seq(10, 70, 10)){
    i <- i+1
    df[i, "minBhd"] <- minBhd
    ttmp <- subset(tmp, BHD > minBhd & BHD < minBhd + ifelse(minBhd >= 70, 20, 10))
    df[i, "n"] <- nrow(ttmp)
    df[i, "bdtKS"] <- ks.test(ttmp$DhVol, ttmp$bdtVol)$statistic
    df[i, "tprKS"] <- ks.test(ttmp$DhVol, ttmp$tprVol)$statistic
    df[i, "tprKS37"] <- ks.test(ttmp$DhVol, ttmp$tprVol37)$statistic
    df[i, "GAMExpKS"] <- ks.test(ttmp$DhVol, ttmp$pred)$statistic

    df[i, "bdtRMSE"] <- Metrics::rmse(ttmp$DhVol, ttmp$bdtVol)
    df[i, "tprRMSE"] <- Metrics::rmse(ttmp$DhVol, ttmp$tprVol)
    df[i, "tprRMSE37"] <- Metrics::rmse(ttmp$DhVol, ttmp$tprVol37)
    df[i, "GAMExpRMSE"] <- Metrics::rmse(ttmp$DhVol, ttmp$pred)

    df[i, "bdtBIAS"] <- -1 * Metrics::bias(ttmp$DhVol, ttmp$bdtVol)
    df[i, "tprBIAS"] <- -1 * Metrics::bias(ttmp$DhVol, ttmp$tprVol)
    df[i, "tprBIAS37"] <- -1 * Metrics::bias(ttmp$DhVol, ttmp$tprVol37)
    df[i, "GAMExpBIAS"] <- -1 * Metrics::bias(ttmp$DhVol, ttmp$pred)
  }
  plot(bdtKS ~ n, data=df, type="b", ylim=range(c(df[, grep("KS", colnames(df))], -0.1)),
       main=paste0("BA=", sp), xlab=c("Bhd (oben)\nn (unten)"), ylab="KS-Test-Statistik", las=1)
  points(df$n, df$tprKS, type="b", lty=2)
  points(df$n, df$tprKS37, type="b", lty=3)
  points(df$n, df$GAMExpKS, type="b", lty=4)
  axis(1, at = unique(df$n), labels = NA, tcl=0.5)
  text(x=rev(unique(df$n)), y = -0.075, labels = paste0(">", seq(70, 10, -10)))
  legend("topright", legend=c("BDAT", "TapeS", "relBDAT-TapeR", "StaDh-Exp"), lty=1:4)

  plot(bdtRMSE ~ n, data=df, type="b", ylim=range(c(df[, grep("RMSE", colnames(df))], 0)),
       main=paste0("BA=", sp), xlab=c("Bhd (oben)\nn (unten)"), ylab="RMSE", las=1)
  points(df$n, df$tprRMSE, type="b", lty=2)
  points(df$n, df$tprRMSE37, type="b", lty=3)
  points(df$n, df$GAMExpRMSE, type="b", lty=4)
  axis(1, at = unique(df$n), labels = NA, tcl=0.5)
  text(x=rev(unique(df$n)), y = 0.05, labels = paste0(">", seq(70, 10, -10)))
  legend("topright", legend=c("BDAT", "TapeS", "relBDAT-TapeR", "StaDh-Exp"), lty=1:4)

  plot(bdtBIAS ~ n, data=df, type="n", ylim=range(c(df[, grep("BIAS", colnames(df))], -1.7)),
       main=paste0("BA=", sp), xlab=c("Bhd (oben)\nn (unten)"), ylab="BIAS", las=1)
  abline(h=0, col="grey")
  points(df$n, df$bdtBIAS, type="b", lty=1)
  points(df$n, df$tprBIAS, type="b", lty=2)
  points(df$n, df$tprBIAS37, type="b", lty=3)
  points(df$n, df$GAMExpBIAS, type="b", lty=4)
  axis(1, at = unique(df$n), labels = NA, tcl=0.5)
  text(x=rev(unique(df$n)), y = -1.6, labels = paste0(">", seq(70, 10, -10)))
  legend("topright", legend=c("BDAT", "TapeS", "relBDAT-TapeR", "StaDh-Exp"), lty=1:4)

  ## plot according to diameter classes
  dat$Bhdc <- dat$BHD %/% 5 * 5
  df <- dat[, c("Bhdc", "DhVol", "bdtVol", "tprVol", "tprVol37", "pred")]
  df[, c("bdtVol", "tprVol", "tprVol37", "pred")] <-
    df[, c("bdtVol", "tprVol", "tprVol37", "pred")] / df$DhVol
  df <- data.frame(stack(df[,-c(1:2)]), Bhd=df$Bhdc)
  par(mar=c(5,6,4,.1))
  boxplot(values ~ ind:Bhd, data=df, col=c(2:6), las=1, horizontal=TRUE,
          ylab="", varwidth=TRUE)
  abline(v=1)

  library(lattice)
  bwplot(values ~ ind | Bhd,    ## see the powerful conditional formula
         data=df,
         # between=list(y=1),
         main="compare Taper models", panel = function(...){
           panel.abline(h=1)
           panel.bwplot(..., varwidth=TRUE, pch="|", col=2:5)
         }, notch=TRUE)
  aggm <- aggregate(values ~ ind + Bhd, data = df, FUN="mean")
  aggm <- tidyr::spread(aggm, ind, values)
  aggsd <- aggregate(values ~ ind + Bhd, data = df, FUN="sd")
  aggsd <- tidyr::spread(aggsd, ind, values)
  aggn <- aggregate(values ~ ind + Bhd, data = df, FUN="length")
  aggn <- tidyr::spread(aggn, ind, values)
  ## plots
  # aggm$Bhd <- as.numeric(levels(aggm$Bhd))
  # aggsd$Bhd <- as.numeric(levels(aggsd$Bhd))
  # aggn$Bhd <- as.numeric(levels(aggn$Bhd))
  ylims <- range(aggm[,-1]+2*aggsd[,-1], aggm[,-1]-2*aggsd[,-1])
  plot(bdtVol ~ Bhd, data=aggm, type="n", las=1, ylab="deviation from obs (pred/obs) [%]",
       ylim=ylims)
  abline(h=1)
  vars <- c("bdtVol", "tprVol", "tprVol37", "pred")
  for(i in seq(along=vars)){
    var <- vars[i]
    polygon(c(aggm$Bhd, rev(aggm$Bhd)),
            y = c(aggm[, var] + 2*aggsd[, var], rev(aggm[, var] - 2*aggsd[, var])),
            col=rgb(0,0,0,0.1), lty=i, border = FALSE)
    points(aggm$Bhd, aggm[, var], type="p", cex=sqrt(aggn$bdtVol/10), lwd=2, col=i)
    points(aggm$Bhd, aggm[, var], type="l", lty=i, lwd=2, col=i)
    points(aggm$Bhd, aggm[, var]+2*aggsd[, var], type="l", lty=i, col=i)
    points(aggm$Bhd, aggm[, var]-2*aggsd[, var], type="l", lty=i, col=i)
  }
  legend("top", legend=c("BDAT", "TapeS", "TapeS37", "StaDh-Exp"), lty=1:4,
         lwd=2, col=1:4)
  invisible(sapply(seq(along=aggn$pred), function(a) text(x = aggn$Bhd[a], y = min(ylims), labels = aggn$pred[a])))
  print("bestes Modell pro Bhd-Klasse:")
  print(vars[apply(aggm[,-c(1)], MARGIN = 1, FUN = function(x) which.min(abs(x-1)))])
  print("bestes Schaftkurvenmodell pro Bhd-Klasse:")
  print(vars[apply(aggm[,-c(1, 5)], MARGIN = 1, FUN = function(x) which.min(abs(x-1)))])
}


