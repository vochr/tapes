#' building the taper curve list for TapeS-package
#' using the sekDB-Taper Models from Project P01697_BWI_TapeR

spp <- c(1, 3, 8, 5, 9, 15, 17, 18, 37, 38, 39)
name <- c("Fi", "Ta", "Dgl", "Kie", "Lae", "Bu", "Ei", "REi", "Bu_", "Ei_", "REi_")
SKPar <- list()
p <- "H:/FVA-Projekte/P01697_BWI_TapeR/Daten/Ergebnisse/TapeR_Fits"
p2 <- "H:/BuI/Persoenlich/Vonderach/TapeS/devel"
for(a in seq(along=spp)){
  # a <- 6
  ba <- spp[a]
  print(ba)
  e <- new.env()
  if(ba < 15){
    load(file.path(p, paste0("TaperModel_data=all_Ba=", ba, ".rdata")), envir = e)
  } else if(ba >=15 & ba <= 18){
    load(file.path(p2, paste0("BDAT_reldata_", ba, "_tapeRmodel.rdata")), envir = e)
  } else {
    if(identical(name[a], "Bu_")){
      ba <- ba - 36 + 14
    } else {
      ba <- ba - 36 + 15
    }
    load(file.path(p, paste0("TaperModel_data=all_Ba=", ba, ".rdata")), envir = e)
  }

  n <- length(unique(attr(fitted(e$tpm$fit.lme), "names")))
  nD <- length(resid(e$tpm$fit.lme))
  en <- rBDAT::getSpeciesCode(ba, "long")
  print(paste0("\\item{", en, ": }{n=",n, ", n_D=", nD, "}"))
  SK.par.lme <- e$tpm$par.lme
  f <- prod((TapeR:::XZ_BSPLINE.f(c(0.01, 0.05), SK.par.lme$knt_x, SK.par.lme$ord_x) %*% SK.par.lme$b_fix)^c(1,-1))
  SK.par.lme$q001 <- f
  attr(SK.par.lme, "spp") <- as.integer(spp[a])
  attr(SK.par.lme, "name") <- name[a]
  SKPar[[a]] <- SK.par.lme
}
if(FALSE){
  # data("SKPar", package = "TapeS")
  # a <- 8
  ## adding in a BDAT_data based TapeR model
  load("devel/BDAT_reldata_Bu_tapeRmodel.rdata")
  n <- length(unique(attr(fitted(tpm$fit.lme), "names")))
  nD <- length(resid(tpm$fit.lme))
  en <- "beech_BDAT"
  print(paste0("\\item{", en, ": }{n=",n, ", n_D=", nD, "}"))
  SK.par.lme <- tpm$par.lme
  f <- prod((TapeR:::XZ_BSPLINE.f(c(0.01, 0.05), SK.par.lme$knt_x, SK.par.lme$ord_x) %*% SK.par.lme$b_fix)^c(1,-1))
  SK.par.lme$q001 <- f
  attr(SK.par.lme, "spp") <- as.integer(37)
  attr(SK.par.lme, "name") <- "bdtBu"
  SKPar[[max(a)+1]] <- SK.par.lme
}
save(SKPar, file="data/SKPar.rdata")
