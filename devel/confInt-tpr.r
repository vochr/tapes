require(TapeS)
setTapeSoptions(Rfn=list(fn="zero"))
setTapeSoptions(Rfn=list(fn="sig2"))
tpr <- tprTrees(spp=1, Dm=c(30, 0.8*30), Hm=c(1.3, 9), Ht=30)
tpr <- tprTrees(spp=1, Dm=61.2, Hm=1.3, Ht=41)
tpr <- tprTrees(spp=1, Dm=c(61.2, 49.8), Hm=c(1.3, 7), Ht=41)
tpr
tprVolume(tpr)
tprVolume(tpr, interval = "confidence")
plot(tpr, bark=TRUE)
hx=seq(0, Ht(tpr), 0.1)
dx <- tprDiameter(tpr, Hx=hx, interval = "confidence")
points(x=hx, y=dx[, "lwr"], type="l", lty=2)
points(x=hx, y=dx[, "upr"], type="l", lty=2)
dx <- tprDiameter(tpr, Hx=hx, interval = "prediction")
points(x=hx, y=dx[, "upr"], type="l", lty=3)
points(x=hx, y=dx[, "lwr"], type="l", lty=3)

sd1 <- 18
sd2 <- 20
var1 <- sd1^2
var2 <- sd2^2
vars <- var1 + var2
sds <- sqrt(vars)

require(TapeR)
tprm <- TapeS::SKPar[[1]]
EDx <- TapeR::E_DHx_HmDm_HT.f(Hx = hx, Hm = c(1.3, 7), Dm = c(61.2, 49.8),
                              mHt = 41, sHt = 0, par.lme = tprm)
str(EDx, max.level = 1)
head(EDx$MSE_Mean)
head(EDx$CI_Mean)
qtv <- qt(p = 0.025, df = tprm$dfRes, ncp = 0, lower.tail = F, log.p = FALSE)
75.92707 + c(-qtv, 0, qtv)*sqrt(6.0097)

EVol <- TapeR::E_VOL_AB_HmDm_HT.f(Hm = c(1.3, 7), Dm = c(61.2, 49.8),
                               mHt = 41, sHt = 0, A=0, B=41, iDH="H", par.lme = tprm)
str(EVol, max.level = 1)
EVol$E_VOL
sqrt(EVol$VAR_VOL)
EVol$E_VOL + c(-qtv, 0, qtv) * sqrt(EVol$VAR_VOL)
tprVolume(tpr, interval = "confidence", AB = list(A=0, B=41, sl=.01), iAB = "H")

# unknown but estimated height
EVol <- TapeR::E_VOL_AB_HmDm_HT.f(Hm = c(1.3, 7), Dm = c(61.2, 49.8),
                                  mHt = 41, sHt = 2, A=0, B=41, iDH="H", par.lme = tprm)
EVol$E_VOL
sqrt(EVol$VAR_VOL)

## get MSE from intervals
tpr <- tprTrees(spp=1, Dm=c(61.2, 49.8), Hm=c(1.3, 7), H=41)
tvol <- tprVolume(tpr, interval = "confidence", AB = list(A=0, B=41, sl=.01), iAB = "H")
qtv <- qt(p = 0.025, df = tprm$dfRes, ncp = 0, lower.tail = F, log.p = FALSE)
(abs(tvol["EVol"] - tvol["lwr"]) / qtv)^2

# compare to TapeR-function
EVol <- TapeR::E_VOL_AB_HmDm_HT.f(Hm = c(1.3, 7), Dm = c(61.2, 49.8),
                                  mHt = 41, sHt = 0, A=0, B=41, iDH="H", par.lme = tprm)
EVol$E_VOL
EVol$VAR_VOL #> very different (half!)
