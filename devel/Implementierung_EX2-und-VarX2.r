ex1 <- 30 # 30cm Bhd
sx1 <- 0.5 # 0.5 cm standardabweichung
x1 <- rnorm(1000, ex1, sx1)
x2 <- rnorm(1000, ex1, sx1)
plot(x=x1, y=x1)
plot(x1 ~ x2)
cov(x1, x2)

## summe
mean(x1 + x1)
mean(x1) + mean(x1)
mean(x1 + x2)
mean(x1) + mean(x2)

## produkt
mean(xx <- x1*x1)
mean(x1) * mean(x1) # too low
mean(x1) * mean(x1) + var(x1) # correct
mean(x1) * mean(x1) + cov(x1, x1) # correct

var(xx)
## abhängige Variable
# https://de.wikipedia.org/wiki/Varianz_(Stochastik)#Summen_und_Produkte
mean(x1)^2*var(x1)+mean(x1)^2*var(x1)+var(x1)*var(x1) # wrong, much too low
# Formel 12' in Kublin et al. (2013) S. 989 für Varianz der Volumenberechnung
var(x1)*var(x1) + 2*cov(x1, x1)^2 + 4*mean(x1)*mean(x1)*cov(x1, x1) # good!
3*var(x1)^2 + 4*mean(x1)*mean(x1)*var(x1) # good!


mean(x1*x2)
mean(x1) * mean(x2) # almost correct, not 100% independent
mean(x1) * mean(x2) + cov(x1, x2) # correct


par(mfrow=c(2,2))
hist(x1)
hist(x2)
hist(x1*x2, freq = FALSE)
abline(v=mean(x1*x2))
lines(density(x1*x2), lty=2)
curve(dnorm(x, mean(x1*x2), sd(x1*x2)), add=TRUE)
sd(x1*x2)
var(x1*x2)

## unabhängige Variable
# https://de.wikipedia.org/wiki/Varianz_(Stochastik)#Summen_und_Produkte
mean(x1)^2*var(x2) + mean(x2)^2*var(x1) + var(x1)*var(x2)
mean(x1)^2*var(x2) + mean(x2)^2*var(x1) + cov(x1, x2)
# Formel 12' in Kublin et al. (2013) S. 989 für Varianz der Volumenberechnung
var(x1)*var(x2) + 2*cov(x1, x2)^2 + 4*mean(x1)*mean(x2)*cov(x1, x2) # not good
# ist ja auch nicht unabhängig!

## check out COV(X1^2, X2^2) mit X1=D1 und X2=D2
require(mvtnorm)
sigma <- matrix(c(1,0.8,0.8,1), ncol=2)
m <- c(34, 32)
# m <- c(0, 0)
X <- rmvnorm(n=10000, mean = m, sigma = sigma)
x <- X[, 1]
y <- X[, 2]
# plot(y ~ x)
var(x); var(y)
cov(x, y)
cov(x-34, y-32)
cor(x, y)

cov(x^2, y^2)
cor(x^2, y^2)
2*cov(x, y)^2
2*cor(x, y)^2
cov2cor(cov(X))
cov(x^2, y^2)
4*mean(x)*mean(y)*cor(x,y)*sd(x)*sd(y) + 2*cor(x,y)^2*var(x)*var(y)
cm <- pi/4e4*2
cov(cm*x^2, cm*y^2)
cm^2 * (4*mean(x)*mean(y)*cor(x,y)*sd(x)*sd(y) + 2*cor(x,y)^2*var(x)*var(y))

cov(x, x) == var(x)

## kovarianz zweier Zufallsvariablen, falls E(X) und VAR(X) bekannt sind
# Beispiel
tpr <- tprTrees()
d <- tprDiameter(tpr, Hx=seq(1, 10, 1), interval="MSE")
ex <- d[1, "EDx"]; ey <- d[1, "MSE_Mean"]
x <- rnorm(1000, ex, sqrt(vx))
for(i in 2:10){
  vx <- 0.75; vy <- 0.45
  y <- rnorm(1000, ey, sqrt(vy))
  mean(x); var(x)
  mean(y); var(y)
  print(cov(x, y))
}


## Implementierung ####
# in TapeS:: tprVolume
interval <- "confidence"
par <- list(list(spp=1, n=1, ddist="norm", dpar=list(mu=30, sd=4)))
obj <- simTrees(par)
plot(obj)
SKspp <- spp(obj)
Rfn <- NULL
if(is.null(Rfn)) Rfn <- getOption("TapeS_Rfn")
ab <- TapeS:::slfun(0, 5, sl=2)
suppH <- suppD <- numeric(0)

if(interval %in% c("confidence", "prediction")){
  ncol <- 3
  nrow <- length(obj)
  colnms <- c("lwr", "EVol", "upr")
  if(identical(interval, "confidence")){
    useLE <- "CI_Mean" # confidence intervals
    # useMSE <- "MSE_Mean" #
    useMSE <- "KOV_Mean" # full covariance matrix, diag==MSE_Mean
  } else {
    useLE <- "CI_Pred" # prediction intervals
    # useMSE <- "MSE_Pred" #
    useMSE <- "KOV_Pred" # varR included as diagonal of vcov matrix
  }
} else if(interval == "none"){
  ncol <- 1
  nrow <- length(obj)
  colnms <- NULL
  useLE <- "DHx"
} else {
  stop("'interval' must be one of 'none', 'confidence', 'prediction'")
}
res <- matrix(NA, ncol = ncol, nrow = nrow, dimnames = list(NULL, colnms))

for(i in 1:length(obj)){
  i <- 1 # first tree
  dlist <- TapeR::E_DHx_HmDm_HT.f(Hx = ab$Hx,
                                  Dm = c(suppD, obj@Dm[[i]]),
                                  Hm = c(suppH, obj@Hm[[i]]),
                                  mHt = obj@Ht[i],
                                  sHt = obj@sHt[i],
                                  par.lme = SKPar[[ SKspp[i] ]],
                                  Rfn = Rfn)

  if(FALSE){ #alt
    D <- dlist[[ useLE ]]
    varD <- dlist[[ useMSE ]]
    (res[i,] <- colSums(pi/4 * 1e-4 * (D^2 + varD) * ab$L))
  }
  if(TRUE){ #neu
    kovD <- dlist[[ useMSE ]]
    corD <- cov2cor(kovD)
    varD <- diag(kovD)
    D <- dlist$DHx # in [cm]
    Cm <- pi/4 * 1e-4 * ab$L
    # sektionsvolumen
    # Erwartungswert Sektionsvolumina in [m]
    Sek <- Cm * (D^2 + varD)
    # Varianz Sektionsvolumina in m^3
    # es gilt: Var(a*X) = a^2*Var(X)
    # daher: Var(Cm*D^2)=Cm^2*Var(X^2)=Cm^2 * (3*varD^2 + 4*varD * D^2)
    # vereinfacht aus var(x1*x1)= var(x1)*var(x1) + 2*cov(x1, x1)^2 + 4*mean(x1)*mean(x1)*cov(x1, x1)
    varSek <- Cm^2 * (3*varD^2 + 4*varD*D^2)
    ## es wird die kovarianz für d1^2*l1, d2^2*l2, ..... benötigt
    kovSek12 <- 4*D[1,1]*D[2,1]*corD[2,1]*sqrt(diag(kovD)[1])*sqrt(diag(kovD)[2]) + 2*corD[2,1]^2*diag(kovD)[1]*diag(kovD)[2]


    ## Addition der Sektionsvolumina für Abschnittsvolumen
    # Erwartungswert Abschnittsvolumen
    Vol <- sum(Sek)
    # Varianz Abschnittsvolumen, d.h. Summe der Segmente
    # https://de.wikipedia.org/wiki/Varianz_(Stochastik)#Summen_und_Produkte
    # Var(X + Y) = VAR(X) + VAR(Y) + 2*COV(X, Y)
    varVol <- sum(varSek) + 2*sum(kovD[upper.tri(kovD)])

    (res[i,] <- cbind(Vol - 2*sqrt(varVol), Vol, Vol + 2*sqrt(varVol)))
  }

}


D <- structure(c(30.762032189736264, 27.018177541714923), .Dim = c(2L, 1L))
kovD <- structure(c(0.73888597051768756, -0.16351119760269936,
                    -0.1635111976024381, 0.3255386343163344), .Dim = c(2L, 2L))
varD <- diag(kovD)
L <- c(2, 2)
D <- rmvnorm(n=1000, mean=D, sigma=kovD)
d1 <- D[, 1]
l1 <- 2
d2 <- D[, 2]
l2 <- 2
plot(d1, d2)

cov(d1, d2) # diameter
cov(3*d1, 2*d2)
3*2*cov(d1, d2)
cov(d1^2, d2^2) # squared diameter
cov(pi/4e4*l1*d1^2, pi/4e4*l2*d2^2) # volume
pi/4e4*l1 * pi/4e4*l2 * cov(d1^2, d2^2) # same, scaled

## see https://mathoverflow.net/questions/330162/correlation-between-square-of-normal-random-variables
cov(d1^2, d2^2)
(covD2 <- 4*mean(d1)*mean(d2)*cor(d1,d2)*sd(d1)*sd(d2) + 2*cor(d1,d2)^2*var(d1)*var(d2))

cov(pi/4e4*l1*d1^2, pi/4e4*l2*d2^2) # volume
pi/4e4*l1 * pi/4e4*l2 * cov(d1^2, d2^2) # same, scaled
pi/4e4*l1 * pi/4e4*l2 * covD2 # same from formula
