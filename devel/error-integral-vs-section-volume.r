for(i in c(1,3,5,8)){
  d <- 20:80
  h <- TapeS::estHeight(d, i)

  vol1 <- rBDAT::getVolume(list(spp=i, D1=d, H=h), AB = list(A=0, B=7, sl=1), iAB = c("H", "Dob"))
  vol001 <- rBDAT::getVolume(list(spp=i, D1=d, H=h), AB = list(A=0, B=7, sl=0.01), iAB = c("H", "Dob"))
  bias <- vol1 / vol001
  plot(bias ~ d, main=paste0("sp=",i), ylab="vol1 / vol001", type="l", ylim=c(0.98, 1))
  abline(h=c(1), col="grey")

  bias <- vol001 - vol1 # m3
  print(mean(bias)) # 1.5%
  plot(bias ~ d, main=paste0("sp=",i), ylab=" vol001 - vol1", type="l", ylim=c(-0.02, 0.02))
  abline(h=0, col="grey")
  abline(h=mean(bias), col="red")

  bias <- (vol001 - vol1) / vol1 # m3
  (mean(bias)) # 1.5%
  plot(bias ~ d, main=paste0("sp=",i), ylab="(vol001 - vol1) / vol1", type="l", ylim=c(-0.02, 0.02))
  abline(h=0, col="grey")
  abline(h=mean(bias), col="red")
}
