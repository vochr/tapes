#' Funktionalitäten und Funktionsweise von TapeS

## Definition von Daten ####
require(TapeS)
obj <- new("trees", spp=c(1L, 3L),
           Hm=list(c(1.3, 5, 7), c(1.3)),
           Dm=list(c(27, 23.5, 22.4), c(27)),
           H=c(27, 27))
Hx <- c(1.3, 5, 7)

## species list
tprSpeciesCode() # aus rBDATPRO übernommen

## species mapping to models
BaMap()

## diameter prediction
tprDiameter(obj, Hx = Hx) # R0=TRUE, Schaftkurve durch Messungen
tprDiameter(obj, Hx = Hx, bark = FALSE)
tprDiameter(obj, Hx = Hx, interval = "prediction")

## height prediction
tprHeight(obj, Dx = 7)
tprHeight(obj, Dx = 7, bark = FALSE)

## bark prediction
tprBark(obj, Hx = 1.3)

## biomass prediction
tprBiomass(obj) # bwi-biomass
tprBiomass(obj, component = c("dh", "dhri", "nad")) # still NSUR (d13-h) models implemented

## volume prediction (no intervals yet)
tprVolume(obj) # default is Vfm
tprVolume(obj, AB = list(A=0, B=7), iAB=c("h", "dob"), bark=TRUE) # same
Vfm(obj) # wrapper
VolR(obj) # wrapper
Efm(obj, stH = 0.01) # default
VolE(obj)
VolFAO(obj)
Vfm_phys(obj) # takes a while
Efm_phys(obj)
tprVolume(obj, AB = list(A=0.01*obj@H, B=7, sl=0.01), iAB = c("H", "Dob"), bark=FALSE)

## assortment
tprAssortment(obj) ## default assortment parameters
pars <- parSort(stH=0.2, Lxh=c(1, 1.5), fixN=2, fixL=4)
tprAssortment(obj, pars = pars)
help("parSort-class")


## using BDAT-data (only tprDiameter so far)
tree <- rBDATPRO::buildTree(list(spp=1, D1=20:30, H1=1.3, D2=0, H2=50, H=20:30))
d03 <- rBDATPRO::getDiameter(tree, Hx=7)
tree <- rBDATPRO::buildTree(list(spp=1, D1=20:30, H1=1.3, D2=d03, H2=7, H=20:30))
head(tree, n=10)

tprDiameter(tree, Hx = 1.3) # R0=TRUE
