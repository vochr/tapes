#' to use NSUR parameter more easily (instead of text evaluation) reformat
#' the produced parameter file

nsur <- read.csv2("H:/FVA-Projekte/P01697_BWI_TapeR/Daten/Ergebnisse/biomass/nsur/Parameter_NSUR.csv")
## add species code
spcode <- c("bu"=5, "dgl"=4, "ei"=6, "fi"=1, "kie"=3, "ba"=7, "es"=8, "ta"=2)
nsur$spp <- spcode[nsur$Ba]
## define necessary components
comps <- c("Stock", "Stockrinde", "DerbGesamt", "DerbGesamtRinde",
           "NichtDerbmitRinde", "Nadeln")
nsur <- subset(nsur, stats=="par" & compartment %in% comps,
               c("Ba", "spp", "comp", "compartment", "equation"))

## make balanced
nsur <- rbind(nsur, data.frame(spp=c(5, 6, 7, 8),
                               Ba=c("bu", "ei", "ba", "es"),
                               comp = 8,
                               compartment = "Nadeln",
                               equation = "0 * BHD^0"))
nsur <- nsur[order(nsur$spp, nsur$comp),]



nr <- nrow(nsur)
pars <- lapply(1:nr, function(a){

  intcpt <- scale <- BHD <- Hoehe <- D03 <- Stockhoehe <- KL <- DH <- 0

  tmp <- strsplit(nsur$equation[a], split = "+", fixed = TRUE)[[1]]
  if(length(tmp)>1){
    intcpt <- as.numeric(tmp[1])
    tmp <- strsplit(tmp[2], split = "*", fixed = TRUE)[[1]]
  } else {
    tmp <- strsplit(tmp, split = "*", fixed = TRUE)[[1]]
  }

  pnms <- c("BHD", "Hoehe", "D03", "Stockhoehe", "KL", "DH")
  idxs <- integer(0)

  for(pn in pnms){
    # pn <- "BHD"
    idx <- grep(pn, tmp)
    if(length(idx) > 0){
      val <- strsplit(tmp[idx], "^", fixed = TRUE)[[1]][2]
      assign(pn, as.numeric(val))
      idxs <- c(idxs, idx)
    }
  }
  if (length(idxs) != length(tmp)){
    assign("scale", as.numeric(tmp[1]))
  }
  return(list(intcpt=intcpt, scale=scale, BHD=BHD, Hoehe=Hoehe, D03=D03,
              Stockhoehe=Stockhoehe, KL=KL, DH=DH))
  })

pars <- as.data.frame(do.call(rbind, pars))
head(pars)

## textual output to be taken into rcpp-script 'BiomassFn.cpp'
parnms <- letters[1:ncol(pars)]
nc <- length(unique(nsur$comp))
nr <- length(unique(nsur$spp))
for(i in 1:ncol(pars)){
  # i <- 1
  tmp <- as.vector(unlist(pars[, i, drop=TRUE]))
  cat("double ", parnms[i], "[", nr, "][", nc, "] = {\n", sep="")
  for(j in 1:nr){
    # j <- 1
    cat("{", paste0(tmp[((j-1)*nc+1):(j*nc)], collapse = ", "), "}",
        ifelse(j!=nr, ",", ""), "\n", sep="")
  }
  cat("};\n")
}
