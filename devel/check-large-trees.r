#' check performance of NSUR function given the large RBS-trees
require(dplyr)
require(RODBC)
p <- "J:/BuI/Persoenlich/Vonderach/Ideen/TLS-QSM/RBS-data/Biomasse_Ergebnis_Baum1-11_Stand20230809.mdb"
con <- RODBC::odbcConnectAccess(p)
B <- RODBC::sqlFetch(con, sqtable = "Baum")
S <- RODBC::sqlFetch(con, sqtable = "Segment")
RODBC::odbcClose(con)

# estimate D03 from segment table.
# first version (=D03) with extrapolating the last value (rule=2)
# second version (=D032) with no extrapolation (not used)
D <- S %>%
  group_by(BaumId) %>%
  filter(fQKum >= 1) %>%
  arrange(Segmentnr) %>%
  left_join(B[, c("Id", "Höhe")], by = c("BaumId"="Id")) %>%
  summarise(D03 = approx(x = Hoehe, y = Do, xout = 0.3*first(Höhe), rule=2)$y,
            D032 = approx(x = Hoehe, y = Do, xout = 0.3*first(Höhe))$y)

B <- B %>%
  select(Id, Baumart, BHD, Höhe, KRO, b_Trockenmasse) %>%
  left_join(D, by=c("Id" = "BaumId"))
B

require(TapeS)
tpr <- tprTrees(spp=ifelse(B$Baumart==7, 15, 17), Dm = B$BHD/10, Hm = rep(1.3, nrow(B)), Ht = B$Höhe)
B$tprD03 <- D03(tpr)
# estimate biomass given dbh, height, stemfoot, d03 and measured crown base height
N <- TapeS:::nsur(spp = ifelse(B$Baumart==7, 5, 6), dbh = B$BHD/10, ht = B$Höhe,
                  sth = 0.01*B$Höhe, d03 = B$D03/10, kl = B$KRO)
B$N1 <- rowSums(N[, -1])
# estimate biomass given dbh, height, stemfoot, d03 and fixed crown base height at 70% of tree height
N <- TapeS:::nsur(spp = ifelse(B$Baumart==7, 5, 6), dbh = B$BHD/10, ht = B$Höhe,
                  sth = 0.01*B$Höhe, d03 = B$D03/10, kl = 0.5*B$Höhe)
B$N2 <- rowSums(N[, -1])
# estimate biomass given dbh, height, stemfoot, d03 from TapeS-function and measured crown base height
N <- TapeS:::nsur(spp = ifelse(B$Baumart==7, 5, 6), dbh = B$BHD/10, ht = B$Höhe,
                  sth = 0.01*B$Höhe, d03 = B$tprD03, kl = B$KRO)
B$N3 <- rowSums(N[, -1])
# estimate biomass given dbh, height, stemfoot, d03 from TapeS-function and fixed crown base height at 50% of tree height
N <- TapeS:::nsur(spp = ifelse(B$Baumart==7, 5, 6), dbh = B$BHD/10, ht = B$Höhe,
                  sth = 0.01*B$Höhe, d03 = B$tprD03, kl = 0.5*B$Höhe)
B$N4 <- rowSums(N[, -1])
# estimate biomass given dbh and tree height only
N <- TapeS:::nsur2(spp = ifelse(B$Baumart==7, 5, 6), dbh = B$BHD/10, ht = B$Höhe)
B$N5 <- rowSums(N[, -1])


B$bdt <- rBDAT::getBiomass(list(spp=ifelse(B$Baumart==6, 17, 15), D1=B$BHD/10, H1=1.3, H=B$Höhe))

plot(N1 ~ BHD, data=B, col=ifelse(Baumart==6, "red", "green"), pch=1, ylab="AGB",
     ylim=c(7000, 18000))
points(x=B$BHD, y=B$N2, pch=2, col=ifelse(B$Baumart==6, "red", "green"))
points(x=B$BHD, y=B$N3, pch=3, col=ifelse(B$Baumart==6, "red", "green"))
points(x=B$BHD, y=B$N4, pch=4, col=ifelse(B$Baumart==6, "red", "green"))
points(x=B$BHD, y=B$N5, pch=5, col=ifelse(B$Baumart==6, "red", "green"))
points(x=B$BHD, y=B$bdt, pch="b", col=ifelse(B$Baumart==6, "red", "green"))

points(x=B$BHD, y=B$b_Trockenmasse, pch=16, col=ifelse(B$Baumart==6, "red", "green"))

legend("topleft", legend=c("Buche", "Eiche"), pch=16, col=c("green", "red"))


## check the default values of height of crown base and stump height
require(mice)
load("J:/FVA-Projekte/P01278_EnNa/P01278_EnNa_BiomassFunc/Daten/aufbereiteteDaten/RData/Imp/Imp_Bu.RData")
class(imp)
bu <- mice::complete(imp)
# average height of crown base
summary(bu$KRO / bu$Hoehe) #> 42%
plot(KRO ~ BHD, data=bu)
plot(KRO ~ Hoehe, data=bu)
plot(I(KRO/Hoehe) ~ Hoehe, data=bu)
abline(lm(I(KRO/Hoehe) ~ Hoehe, data=bu))
plot(I(KRO/Hoehe) ~ BHD, data=bu)
abline(lm(I(KRO/Hoehe) ~ BHD, data=bu))

# average height of stump
summary(bu$Stockhoehe / bu$Hoehe)
#> 0.76%
plot(Stockhoehe ~ BHD, data=bu)
plot(Stockhoehe ~ Hoehe, data=bu)
plot(I(Stockhoehe/Hoehe) ~ BHD, data=bu)
abline(lm(I(Stockhoehe/Hoehe) ~ BHD, data=bu))


## check additional QSM-trees
require(readxl)
require(rBDAT)
require(TapeS) # cran version
bu <- as.data.frame(readxl::read_xlsx("J:/BuI/Persoenlich/Vonderach/Ideen/TLS-QSM/RBS-data/additionalQSM-data/Buche_data.xlsx"))
head(bu)
bu$rBDAT2 <- getBiomass(buildTree(list(spp=15, D1=bu$dbh, H1=1.3, D2=bu$D03, H2=0.3*bu$ht, H=bu$ht)))
plot(x=bu$rBDAT, y=bu$rBDAT2); abline(0,1)
bu$nsur <- rowSums(nsur(spp = rep(5, nrow(bu)), dbh = bu$dbh, ht = bu$ht, sth = 0.01*bu$ht, d03 = bu$D03, kl = bu$CL)[, -1])
bu$nsur2 <- rowSums(nsur2(spp = rep(5, nrow(bu)), dbh = bu$dbh, ht = bu$ht)[, -1])
plot(AGB ~ dbh, data=bu, pch=16, col="green",
     ylim=range(bu[, 9:20]))
# points(x=bu$dbh, y=bu$rBDAT, pch=1)
points(x=bu$dbh, y=bu$rBDAT, pch="b")
points(x=bu$dbh, y=bu$rBDAT2, pch=1)
points(x=bu$dbh, y=bu$TapeS, pch=2)
points(x=bu$dbh, y=bu$Bartelink1997, pch=3)
points(x=bu$dbh, y=bu$Ciencialaetal2005, pch=4)
points(x=bu$dbh, y=bu$Wutzleretal2008, pch=5)
points(x=bu$dbh, y=bu$Dutcăetal2020, pch=6)
points(x=bu$dbh, y=bu$`Hochbichler, 2002`, pch=7)
points(x=bu$dbh, y=bu$`Duvigneaud et al., 1977`, pch=8)
points(x=bu$dbh, y=bu$nsur, pch=9)
points(x=bu$dbh, y=bu$nsur2, pch=10)

## Eiche
ei <- as.data.frame(readxl::read_xlsx("J:/BuI/Persoenlich/Vonderach/Ideen/TLS-QSM/RBS-data/additionalQSM-data/Eiche_data.xlsx"))
head(ei)
ei$rBDAT2 <- getBiomass(buildTree(list(spp=15, D1=ei$dbh, H1=1.3, D2=ei$D03, H2=0.3*ei$ht, H=ei$ht)))
plot(x=ei$rBDAT, y=ei$rBDAT2); abline(0,1) # ei$rBDAT without D03
ei$nsur <- rowSums(nsur(spp = rep(6, nrow(ei)), dbh = ei$dbh, ht = ei$ht, sth = 0.01*ei$ht, d03 = ei$D03, kl = ei$CL)[, -1])
ei$nsur2 <- rowSums(nsur2(spp = rep(6, nrow(ei)), dbh = ei$dbh, ht = ei$ht)[, -1])
plot(QSM ~ dbh, data=ei, pch=16, col="red",
     ylim=range(ei[, c(8:10, 12:18)]), main="oak")
points(x=ei$dbh, y=ei$rBDAT, pch=1)
# points(x=ei$dbh, y=ei$rBDAT, pch="b")
points(x=ei$dbh, y=ei$rBDAT2, pch=2)
points(x=ei$dbh, y=ei$TapeS, pch=3)
points(x=ei$dbh, y=ei$Ciencialaetal2008, pch=4)
# points(x=ei$dbh, y=ei$Suchomeletal2012, pch=5)
# points(x=ei$dbh, y=ei$Krejzaetal2017_dh, pch=6)
# points(x=ei$dbh, y=ei$Krejzaetal2017, pch=7)
points(x=ei$dbh, y=ei$nsur, pch=8)
points(x=ei$dbh, y=ei$nsur2, pch=9)
legend("topleft", legend=c("QSM", "rBDAT", "rBDAT+D03", "TapeS: Muna", "TapeS: nsur", "nsur: DBH+Ht"),
       pch=c(16, 1,2,3,8,9), col=c("red", rep("black",5)))
