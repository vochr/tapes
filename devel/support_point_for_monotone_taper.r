#' to assist taper curve build up if not monotone, add a "virtual" support point
#' into measurements. In 1% of tree height (=stump height), assume diameter to
#' be approximately 20% larger than DBH

require(TapeS)
require(rBDAT)

cols <- rainbow(n = 9)
shq <- c(17, 50, 83)
plot(1, type="n", ylim=c(1, 2), xlim=c(0, 80), ylab="d001/d", xlab="Bhd",
     main=paste0("D001 vs Bhd"))
for(i in seq(along=shq)){
  hq <- shq[i]
  d <- seq(7, 80, 0.1)
  h <- iS::estHeight(d, 3, qtl = hq)
  # plot(h~d, type="l")
  abline(v=7)
  for(h2 in seq(10, 90, 10)){
    d001 <- rBDAT::getDiameter(list(spp=3, D1=d, H2=h2, H=h, Hx=0.01*h))
    points(x=d, y=d001/d, pch=16, col=cols[h2/10], type="l", lty=i, lwd=3)
    abline(0,1)
  }
  # plot(rBDAT::buildTree(list(spp=3, D1=d[1], H2=h2, H=h[1])))
}


tpr <- bdat_as_tprtrees(bdt <- buildTree(list(spp=3, D1=d, H=iS::estHeight(d, 3))))
TF <- sapply(1:length(tpr), function(a) tpr[a]@monotone)
idx <- which(TF==FALSE)
length(idx)
par(mfrow=c(2,2))
for(i in idx){
# for(i in 1:50){
  plot(tpr[i], ylim=c(0, 1.5*d[i]))
  dd <- Dm(tpr[i])
  hh <- Hm(tpr[i])
  points(x=hh, y=dd, pch=1)

  tmp <- tprTrees(spp=3,
                  Dm=c(1.2*dd[which.min(hh)], dd),
                  Hm=c(0.01*Ht(tpr[i]), hh), Ht=Ht(tpr[i]))
  points(x=c(0.01*Ht(tpr[i]), hh), y=c(1.2*dd[which.min(hh)], dd), pch=3)
  x <- seq(0, Ht(tpr[i]), length.out = 100)
  y <- tprDiameter(tmp, Hx=x)
  points(x=x, y=y, type="l", lty=2)
}

