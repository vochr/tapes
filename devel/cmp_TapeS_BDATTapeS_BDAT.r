require(TapeS)
TapeS::setTapeSoptions(Rfn = list(fn="sig2"))
TapeS::setTapeSoptions(Rfn = list(fn="zero"))
options()$TapeS_Rfn
require(rBDAT)

n0 <- numeric(0)
df <- data.frame(sp=n0, d=n0, voltapes=n0, volreltaper=n0, volbdat=n0)

pdf("devel/cmp_TapeS_BDATTapeS_BDAT.pdf", width = 20, height = 10, paper = "a4r")
for(sp in c(15L, 17L, 18L)){
  for(d in seq(10, 150, 10)){
    print(d)
    h <- TapeS::petterson(BaMap(sp, type=6), d)
    q <- rBDAT::getForm(list(spp=sp, D1=d, H=h), inv=4) # BWI3
    tpr <- bdat_as_tprtrees(bdt <- buildTree(list(spp=c(15, 15), D1=d, H1=1.3,
                                                  D2=d*q, H2=0.3*h, H=h)))
    if(identical(sp, 15L)){
      tpr@spp <- c(15L, 37L)
    } else if(identical(sp, 17L)){
      tpr@spp <- c(17L, 38L)
    } else if(identical(sp, 18L)){
      tpr@spp <- c(18L, 39L)
    }
    tpr@monotone <- check_monotonicity(tpr)


    plot(tpr[1], bark=TRUE)
    hx <- seq(0, ceiling(tpr[2]@Ht*10)/10, 0.1)
    points(x=hx, y=tprDiameter(tpr[2], Hx = hx, bark = TRUE), lty=2, col="black", type="l")

    points(x=hx, y=rBDAT::getDiameter(bdt[1,], Hx=hx, bark=TRUE), lty=3, type="l")

    points(x=c(1.3, 0.3*h), y=c(d, q*d), pch=3, cex=2)

    vol1 <- round(tprVolume(tpr[1]), 2)
    vol2 <- round(tprVolume(tpr[2]), 2)
    vol3 <- round(getVolume(bdt[1,]), 2)
    df <- rbind(df, data.frame(sp=sp, d13=d, voltapes=vol2, volreltaper=vol1, volbdat=vol3))
    legend("topright", legend=paste0(c("TapeR_relBDAT, vol = ",
                                       "TapeR_data,    vol = ",
                                       "rBDAT,         vol = "), c(vol1, vol2, vol3)),
           lty=c(1, 2, 3))
  }
}
dev.off()

## get differences between models
head(df)
df$evolS_B <- (df$voltapes - df$volbdat) / df$volbdat
df$evolR_B <- (df$volreltaper - df$volbdat) / df$volbdat
df$evolR_S <- (df$volreltaper - df$voltapes) / df$voltapes
pdf(file="H:/FVA-Projekte/P01697_BWI_TapeR/Programme/Eigenentwicklung/96_publication/cmp_bdt_tpr_tps.pdf")
par(cex=1.2)
plot(evolbdat ~ d13, data=df, type="n", las=1, ylab="deviation of solid wood volume[%]",
     ylim=range(df[, grep("^e", colnames(df))]),
     main="exemplary comparison between the \n different taper curve models")
for(spp in c(15L, 17L, 18L)){
  vars <- c("evolS_B", "evolR_B", "evolR_S")
  for(i in seq(along=vars)){
    m <- vars[i]
    sdf <- subset(df, sp==spp)
    y <- sdf[, m]
    x <- sdf$d13
    points(y~x, type="l", col=spp-14, lty=i)
  }
}
legend("right", legend=c("EB", "OA", "RE"), col=c(15, 17, 18)-14, lty=1, title="species")
legend("bottomleft", legend=c("TapeS vs. BDAT", "relTapeR vs. BDAT", "relTapeR vs. TapeS"), lty=1:3)
dev.off()

## calculate differences against BWI3
