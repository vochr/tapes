#' @title Estimate biomass component share
#' @description Calculate biomass component share for given species and dimension
#' using biomass function from Vonderach et al. (2018)
#' @param spp species
#' @param dbh diameter in breast heigth
#' @param h tree height
#' @details function to calculate component biomass share; uses biomass
#' functions from Vonderach et al. (2018) and species mapping as in
#' \code{TapeS::BaMap(, type=7)}.
#' @return depending on parameter comp, returns above-ground biomass (if NULL),
#' or required components.
#' @references Vonderach, C., G. Kändler and C. F. Dormann (2018).
#' "Consistent set of additive biomass functions for eight tree species in
#' Germany fit by nonlinear seemingly unrelated regression."
#' Annals of Forest Science 75(2): 49. \url{https://doi.org/10.1007/s13595-018-0728-4}
#' @import dplyr
#' @import tidyr
#' @export
#' @examples
#' spp <- 1
#' dbh <- 30
#' h <- 25
#' comp <- c("dh")
#' BmComp(spp=spp, dbh=dbh, h=h, comp=comp)
#' comp <- c("dh", "dhri", "ndh")
#' BmComp(spp=spp, dbh=dbh, h=h)
#' BmComp(spp=spp, dbh=dbh, h=h, comp=comp)
#' spp <- c(1,1,3,3,15)
#' BmComp(spp=spp, dbh=dbh, h=h, comp=comp)

BmComp <- function(spp, dbh, h, comp=NULL){

  ## get data
  bmf <- loadBMF(sp = spp)
  if(is.null(comp)){
    returncomp <- "agb"
    comp <- c("sto", "stori", "dh", "dhri", "ndh", "nad")
  } else {
    if(!all(comp %in% c("sto", "stori", "dh", "dhri", "ndh", "nad", "agb"))){
      stop("'comp' wrong!")
    } else {
      returncomp <- comp
    }
  }

  ## calculate biomass per compartment
  res <- data.frame(spp=spp, BHD=dbh, Hoehe=h) %>%
    dplyr::mutate(id = 1:n(),
                  Stockhoehe = 0.01 * Hoehe,
                  DH = BHD * Hoehe) %>%
    dplyr::left_join(bmf, by=c("spp"="BaBDAT")) %>%
    group_by(id, compartment) %>%
    mutate(bm = eval(parse(text=equation)),
           equation = NULL,
           Ba = NULL,
           spp = NULL,
           BHD = NULL,
           Hoehe = NULL,
           Stockhoehe = NULL,
           DH = NULL) %>%
    group_by(id) %>%
    mutate(bms = bm / sum(bm),
           bm = NULL) %>%
    ungroup() %>%
    tidyr::spread(compartment, bms) %>%
    mutate(agb=1) %>%
    select(id, returncomp) %>%
    as.data.frame()

  return(res)
}


loadBMF <- function(sp=NULL){
  # bmf <- read.csv2("H:/FVA-Projekte/P01479_HE_VSB/Daten/Urdaten/vonEnNa/Parameter_NSUR_tobeused.csv",
  #                  stringsAsFactors = FALSE)

  bmf <- structure(list(Ba = c("ta", "ta", "ta", "ta", "ta", "ta",
                               "bu", "bu", "bu", "bu", "bu", "dgl",
                               "dgl", "dgl", "dgl", "dgl", "dgl",
                               "ei", "ei", "ei", "ei", "ei",
                               "fi", "fi", "fi", "fi", "fi", "fi",
                               "kie", "kie", "kie", "kie", "kie", "kie",
                               "ba", "ba", "ba", "ba", "ba",
                               "es", "es", "es", "es", "es"),
                        BaBDAT = c(rep(3, 6), rep(15, 5), rep(8, 6), rep(17, 5),
                                   rep(1, 6), rep(5, 6), rep(23, 5), rep(21, 5)),
                        compartment = c("sto", "stori", "dh", "dhri", "ndh", "nad",
                                        "sto", "stori", "dh", "dhri", "ndh",
                                        "sto", "stori", "dh", "dhri", "ndh", "nad",
                                        "sto", "stori", "dh", "dhri", "ndh",
                                        "sto", "stori", "dh", "dhri", "ndh", "nad",
                                        "sto", "stori", "dh", "dhri", "ndh", "nad",
                                        "sto", "stori", "dh", "dhri", "ndh",
                                        "sto", "stori", "dh", "dhri", "ndh"),
                        equation = c("0.0121 * BHD^2.2645 * Stockhoehe^0.7596",
                                     "0.0036 * BHD^2.1225 * Stockhoehe^0.7856",
                                     "0.0046 * BHD^1.1917 * Hoehe^2.2103",
                                     "0.0019 * BHD^1.4458 * Hoehe^1.678",
                                     "0.0273 * BHD^2.2573",
                                     "0.1071 * BHD^1.6952",
                                     "0.0315 * BHD^2.1447 * Stockhoehe^0.798",
                                     "0.004 * BHD^1.9184 * Stockhoehe^0.8076",
                                     "-4.6332 + 0.019 * BHD^2.0861 * Hoehe^0.9502",
                                     "0.0013 * BHD^1.9596 * Hoehe^1.099",
                                     "0.4006 * BHD^2.3211 * Hoehe^-0.7636",
                                     "0.0186 * BHD^2.185 * Stockhoehe^0.7723",
                                     "0.0032 * BHD^2.0357 * Stockhoehe^0.7621",
                                     "0.0131 * BHD^1.9299 * Hoehe^1.0715",
                                     "0.0018 * BHD^1.9099 * Hoehe^1.0306",
                                     "0.2784 * BHD^3.1276 * Hoehe^-1.7984",
                                     "-1.8821 + 0.2749 * BHD^2.4833 * Hoehe^-1.3051",
                                     "0.0363 * BHD^2.0657 * Stockhoehe^0.7721",
                                     "0.0158 * BHD^1.791 * Stockhoehe^0.9032",
                                     "-3.9731 + 0.0223 * BHD^2.1012 * Hoehe^0.8647",
                                     "0.0043 * BHD^1.9437 * Hoehe^0.9576",
                                     "0.3028 * BHD^2.1945 * Hoehe^-0.6882",
                                     "0.022 * BHD^2.1212 * Stockhoehe^0.6056",
                                     "-0.0128 + 0.0067 * BHD^1.7268 * Stockhoehe^0.5947",
                                     "0.0142 * BHD^1.7414 * Hoehe^1.2401",
                                     "0.0038 * BHD^1.6076 * Hoehe^1.0528",
                                     "1.8472 + 0.0243 * BHD^2.9671 * Hoehe^-0.8183",
                                     "-1.6847 + 0.285 * BHD^2.1173 * Hoehe^-0.8334",
                                     "0.0624 * BHD^1.9322 * Stockhoehe^1.0414",
                                     "0.0077 * BHD^1.8127 * Stockhoehe^0.8732",
                                     "0.0173 * BHD^2.0072 * Hoehe^0.914",
                                     "0.0055 * BHD^2.0108 * Hoehe^0.5374",
                                     "0.1316 * BHD^2.444 * Hoehe^-0.949",
                                     "0.1484 * BHD^2.332 * Hoehe^-1.2026",
                                     "-0.9368 + 0.0529 * BHD^1.9653 * Stockhoehe^0.664",
                                     "-0.1469 + 0.012 * BHD^1.7668 * Stockhoehe^0.6883",
                                     "0.0171 * BHD^2.0527 * Hoehe^0.9476",
                                     "0.0039 * BHD^2.0204 * Hoehe^0.7584",
                                     "0.0416 * BHD^2.1138",
                                     "1.1307 + 0.0054 * BHD^2.668 * Stockhoehe^0.9223",
                                     "0.0434 * BHD^1.4611 * Stockhoehe^0.9111",
                                     "0.0171 * BHD^2.0198 * Hoehe^1.0356",
                                     "0.0005 * DH^1.7454", "0.1024 * BHD^1.9711")),
                   class = "data.frame", row.names = c(NA, -44L))

  if(!is.null(sp[1])){
    bmf <- bmf[bmf$BaBDAT %in% sp, ]
  }

  return(bmf)
}
