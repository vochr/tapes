\documentclass{article}

% \usepackage{graphicx}
% \usepackage{mathptmx}
\usepackage[ngerman]{babel} %Rechtschreibung
\usepackage[T1]{fontenc} %Zeichensatz für Output
\usepackage[utf8]{inputenc} %Zeichensatz für Input
\usepackage{natbib} %Literaturverzeichnis und Zitate
\usepackage[autostyle=true, english=british]{csquotes} %Zitate in \enquote{}
\usepackage{amsmath, amsfonts, amssymb, bm}
\usepackage{bigfoot}
% \usepackage{multirow}
% \usepackage{fixltx2e}
% \usepackage{blindtext}
\usepackage{Sweave}

\begin{document}
\input{EX-VARX-concordance}
\title{Erwartungswert und Varianzschätzungen in TapeR/TapeS}
\author{Christian Vonderach}
\maketitle

\section{Einführung}
Das R-Paket TapeS stellt Schaftkurvenmodelle und ergänzende Funktionen zur
Verfügung um -- äquivalent zu BDAT -- eine Voluminierung und Sortierung von
Bäumen vornehmen zu können. Vorteil der neuen Schaftkurvenmodelle gegenüber BDAT
ist deren statistische Grundlage (linear gemischtes Modell auf B-Spline Basis),
die damit einhergehende Möglichkeit Varianzen darzustellen und die größere
Modell-Stabilität im Extrapolationsbereich. Zusätzlich sind die neuen Modelle
durch eine umfangreichere Datenbasis abgesichert, die einen geringeren
Extrapolationsbereich nötig macht.

Die Schätzung von Durchmessern und daraus abgeleitet von Volumen in TapeS wird
im Hintergrund durch das TapeR-Paket realisiert. Die durch die TapeR-Modelle
zur Verfügung gestellten Fehlerterme werden genutzt um die Varianzen von
geschätzten Durchmessern und Volumen darzustellen.
\begin{Schunk}
\begin{Sinput}
> require(TapeR)
> require(TapeS)
> tprm <- TapeS::SKPar[[1]] # TapeR-Model für Fichte
\end{Sinput}
\end{Schunk}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Durchmesserschätzung}
\subsection{Erwartungswert}
Der Durchmesser $D(H)$ in gewählter Höhe $H$ werden mit der Methodik nach
\citet{Kublinetal2013} geschätzt. Entsprechend Gleichung~3 auf Seite 987 wird
der Erwartungswert eines Durchmessers $E[D(H)]$ einer mit gemessenen Durchmesser
$D_m$ in Messhöhe $H_m$ und Baumhöhe $H_t$ neu kalibrierten Schaftkurve
berechnet mit
\begin{equation}
  \hat{E}[D(H)|D_m, H_t] := \hat{E}[D(H)|D_m] = \hat{f}(h) + \hat{g}_m(h) = x(h)^T\hat{\beta} + z(h)^T\hat{\gamma}_m
\end{equation}
wobei $x(h)^T$ und $z(h)^T$ die Auswertung der B-Spline-Matrizen in der
relativen Höhe $h=\frac{H}{H_t}$, $\hat{\beta}$ die Parameter des
Populationsmodells und $\hat{\gamma}_m$ die Zufallseffekte der kalibrierten
Schaftkurve sind.

\subsection{Varianz}
Die Berechnung der Varianz ist ebenfalls in \citet{Kublinetal2013} beschrieben
und wird bei Nutzung von TapeS aus der TapeR-Rückgabe übernommen. Laut
Gleichung~4 auf Seite~987 wird die Varianz berechnet durch
\begin{equation}
  \hat{VAR}[\hat{E}[D(H)|D_m, H_t]] = x(h)^T\hat{\Sigma}_{\hat{\beta}}x(h)^T +
    z(h)^T\hat{V}_{m}z(h) + x(h)^T\hat{C}_{m}z(h) + z(h)^T\hat{C}^T_{m}x(h)
\end{equation}

Sowohl der Erwartungswert als auch die Varianz wird durch TapeR für jede
angeforderte Höhe zurückgeliefert. Für beide Formeln ist implizit angenommen,
die Baumhöhe $H_t$ zu kennen bzw. fehlerfrei erfasst zu sein. Für den Fall einer
fehlerbehafteten Baumhöhe sind in \citet{Kublinetal2013} entsprechende Formeln
hinterlegt und in TapeR implementiert, sodass hier auf eine Darstellung
verzichtet wird. Da das Interface zwischen TapeR und TapeS beide Fälle abdeckt
und für die Ableitung von Erwartungswert und Varianz der
Volumina aus der Durchmesserschätzung diese Differenzierung unerheblich ist,
werden die Formeln hier nicht reproduziert. Es wird auf \citet{Kublinetal2013}
verwiesen.

\subsection{Implementierung}
Beispielhaft wird die Implementierung dargestellt:
\begin{Schunk}
\begin{Sinput}
> ## Beispiel aus Kublin et al. 2013
> ## Definition der Höhen für die Durchmesserschätzung
> hx <- seq(0.5, 40.5, 1) # Mitte von 1m-Sektion entlang des Schaftes
> ## Durchmesserschätzung direkt mit TapeR
> EDx <- TapeR::E_DHx_HmDm_HT.f(Hx = hx, Hm = c(1.3, 7), Dm = c(61.2, 49.8),
+                               mHt = 41, sHt = 0, par.lme = tprm)
> str(EDx, max.level = 1)
\end{Sinput}
\begin{Soutput}
List of 9
 $ DHx     : num [1:41, 1] 69 59.7 54.9 52.9 51.9 ...
 $ Hx      : num [1:41] 0.5 1.5 2.5 3.5 4.5 5.5 6.5 7.5 8.5 9.5 ...
 $ MSE_Mean: num [1:41] 1.978 0.182 0.454 0.483 0.341 ...
 $ CI_Mean : num [1:41, 1:3] 66.3 58.8 53.5 51.5 50.8 ...
 $ KOV_Mean: num [1:41, 1:41] 1.978 0.169 -0.573 -0.667 -0.526 ...
 $ MSE_Pred: num [1:41] 2.207 0.411 0.683 0.711 0.569 ...
 $ CI_Pred : num [1:41, 1:3] 66.1 58.4 53.2 51.2 50.4 ...
 $ KOV_Pred: num [1:41, 1:41] 2.207 0.169 -0.573 -0.667 -0.526 ...
 $ Rfn     :List of 1
\end{Soutput}
\end{Schunk}
Die Rückgabe der TapeR-Funktion liefert eine Liste mit den Elemente \verb|DHx|
mit den geschätzten Durchmesser, \verb|Hx| der dazugehörigen Höhe,
\verb|MSE_Mean| den geschätzten Varianzen des Erwartungswerts, \verb|CI_Mean|
den daraus abgeleiteten Konfidenzintervallen für den Erwartungswert und
\verb|KOV_Mean| den Kovarianzen des Erwartungswerts. Die letzten drei Elemente
werden auch für eine neue Beobachtung bereitgestellt (\verb|MSE_Pred|,
\verb|CI_Pred|, \verb|KOV_Pred|). Zuletzt wird noch eine Liste bzgl. der
verwendeten Funktion zur Erstellung Residual-Matrix \verb|Rfn| angegeben.
Standardmäßig wird \emph{sig2}, d.\,h. der Standardfehler der Residuen
verwendet\footnote{Es ist möglich die Schaftkurve durch die gemessenen
Durchmesser zu zwingen in dem der Standardfehler der Residuen auf Null gesetzt
wird (\verb|Rfn=list(fn="zero")|). Diese
Option sollte aber nicht überstrapaziert werden, also nur bei wenigen
vorliegenden Durchmesser pro Baum eingesetzt werden.}.

\begin{Schunk}
\begin{Sinput}
> ## Fortsetzung Beispiel aus Kublin et al. 2013 mit TapeS
> ## Definition eines Objekts mit Klasse 'tprTrees'
> tpr <- tprTrees(spp=1, Dm=c(61.2, 49.8), Hm=c(1.3, 7), Ht=41, sHt=0)
> ## Durchmesserschätzung:
> estD <- tprDiameter(tpr, Hx = hx, bark = TRUE, interval = "MSE")
> all.equal(EDx$DHx[, 1], estD[, "EDx"])
\end{Sinput}
\begin{Soutput}
[1] TRUE
\end{Soutput}
\begin{Sinput}
> all.equal(EDx$MSE_Mean, estD[, "MSE_Mean"])
\end{Sinput}
\begin{Soutput}
[1] TRUE
\end{Soutput}
\begin{Sinput}
> all.equal(EDx$MSE_Pred, estD[, "MSE_Pred"])
\end{Sinput}
\begin{Soutput}
[1] TRUE
\end{Soutput}
\begin{Sinput}
> #> TapeR und TapeS liefern identische Ergebnisse
\end{Sinput}
\end{Schunk}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Volumenschätzung aus einer Schaftkurve}
Im TapeR-Paket \citep{KublinBreidenbach2015} wird das Volumen aus dem
Rotationsintegral der Schaftkurve abgeleitet. Im TapeS-Paket,  ebenso in BDAT,
wird dagegen das Volumen aus 2\,m-Sektionen\footnote{die Sektionslänge ist
standardmäßig auf 2\,m vorgegeben, kann aber beliebig angepasst werden.}
berechnet. Die Ergänzung einer Volumenfunktion für das physikalische Volumen
(aka Rotationsintegral) ist vorgesehen.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Sektionsvolumen}
\subsubsection{Erwartungswert}
Der Erwartungswert des Volumens (in m$^3$) einer Sektion $s$ mit Länge $L$
(in m) wird bei Kenntniss oder (exakter) Messung des Mitteldurchmessers $D$
(in cm) entsprechend berechnet aus
\begin{equation}
  V_{s} = \frac{\pi}{4} \cdot 10^{-4} \cdot L_s \cdot D_s^2.
\end{equation}
Ist der benötigte Mitteldurchmesser allerdings nicht bekannt und wird geschätzt,
wie dies üblicherweise im Fall der Nutzung einer Schaftkurve stattfindet, muss
der Fehler der Durchmesserschätzung berücksichtigt werden. Dies resultiert aus
den Rechenregeln für Erwartungswerte und Varianzen von Zufallsvariablen. Der
Erwartungswert eines Produktes zweier abhängigen Zufallsvariablen wird berechnet
als
\begin{equation}
  % vgl. https://de.wikipedia.org/wiki/Erwartungswert
  E[X \cdot Y] = E[X] \cdot E[Y] + COV[X, Y]
\end{equation}
mit der Kovarianz $COV(X,Y)$ zwischen $X$ und $Y$. Im Fall der Volumenberechnung
einer Sektion bei der $X$ und $Y$ jeweils der geschätzte Mitteldurchmesser
$\hat{D}$ darstellt, reduziert sich die $COV(X,Y)$ zu $VAR(\hat{D})$. Damit wird
das Sektionsvolumen $\hat{V_s}$ berechnet als
\begin{equation}
  \hat{V_{s}} = \frac{\pi}{4} \cdot 10^{-4} \cdot L_s \cdot (\hat{D_s}^2 + VAR(\hat{D})).
\end{equation}
Beide Größen ($\hat{D}$ und $VAR(\hat{D})$) werden durch die Funktionen in
TapeR bereitgestellt und können direkt weiter verarbeitet werden.

\subsubsection{Varianz}
Die Volumenschätzung eines Segments basiert auf der Volumenformel eines
Zylinders und damit auf den quadrierten Durchmessern, letztendlich auf dem
\enquote{Produkt zweier abhängiger Zufallsvariablen}.

Die Varianz für das Volumen, berechnet aus dem Integral über die Höhen A und B
der Schaftkurve, wird laut \citet[][Gl.~12, S.~989]{Kublinetal2013} berechnet
durch
\begin{equation}
  % vgl. https://de.wikipedia.org/wiki/Erwartungswert
  VAR[Vol] = (\frac{\pi}{4} \cdot 10^{-4} \cdot L)^2 (VAR[D]*VAR[D] + 2*COV[D, D]^2 + 4*E[D]*E[D]*COV[D, D])\\
\end{equation}
bzw. vereinfacht
\begin{equation}
  VAR[Vol] = (\frac{\pi}{4} \cdot 10^{-4} \cdot L)^2 (3*VAR[D]^2 + 4*E[D]*E[D]*VAR[D])
\end{equation}


\subsubsection{Implementierung}
Die Volumenschätzung ist sowohl in TapeR als auch -- mit mehr Optionen -- in
TapeS implementiert. Im ersten Schritt wird die Volumenschätzung für eine
Sektion gezeigt:
\begin{Schunk}
\begin{Sinput}
> ## beispielhaft für Sektion 5
> (Hx <- hx[5])
\end{Sinput}
\begin{Soutput}
[1] 4.5
\end{Soutput}
\begin{Sinput}
> L <- 1 # in [m]
> (D <- estD[5, "EDx"])
\end{Sinput}
\begin{Soutput}
     EDx 
51.92733 
\end{Soutput}
\begin{Sinput}
> (varD <- estD[5, "MSE_Mean"]) # its a rounded value
\end{Sinput}
\begin{Soutput}
MSE_Mean 
  0.3408 
\end{Soutput}
\begin{Sinput}
> (vol <- pi/4 * 10^-4 * L * (D^2 + varD)) # Sektionsvolumen
\end{Sinput}
\begin{Soutput}
      EDx 
0.2118053 
\end{Soutput}
\begin{Sinput}
> (pi/4 * 1e-4 * L)^2 * (3 * varD^2 + 4 * D^2 * varD) # Varianz des Sektionsvolumens
\end{Sinput}
\begin{Soutput}
    MSE_Mean 
2.267632e-05 
\end{Soutput}
\begin{Sinput}
> # tprVolume takes unrounded value (diag(KOV_Mean) instead of MSE_Mean)
> tprVolume(tpr, AB=list(A=4, B=5, sl=1), iAB="H", interval = "confidence")
\end{Sinput}
\begin{Soutput}
         lwr         EVol          upr          MSE 
2.024726e-01 2.118053e-01 2.211380e-01 2.267315e-05 
\end{Soutput}
\begin{Sinput}
> TapeR::E_VOL_AB_HmDm_HT.f(Hm = c(1.3, 7), Dm = c(61.2, 49.8), mHt = 41, sHt = 0,
+                           A=4, B=5, iDH = "H", par.lme = tprm)[1:2]
\end{Sinput}
\begin{Soutput}
$E_VOL
[1] 0.211834

$VAR_VOL
[1] 2.272614e-05
\end{Soutput}
\end{Schunk}

Die manuelle Volumenschätzung stimmt mit der Umsetzung in der Funktion
\verb|TapeS::tprVolume()| praktisch überein. Eine kleine Differenz existiert
zwischen der Zylinderapproximation aus TapeS und dem Rotationsintegral in TapeR.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Abschnittsvolumen}

Das Volumen über bestimmte Abschnitte AB (A=untere Höhenlage im Schaft, B=obere
Höhenlage im Schaft) der Schaftkurve bis hin zur Schätzung für
den Gesamtbaum geschieht in TapeS standardmäßig über 2\,m-Sektionen.

\subsubsection{Erwartungswert}
Sofern mehr als ein Segment ausgewertet wird, muss über diese Segmente summiert
werden. Das letzte Segment hat in der Regel\footnote{Dieser
Fall wird hier nicht anders behandelt. Eventuell spielt das eine Rolle, da die
Abschnittslänge in bestimmten Fällen nicht vorgegeben ist, sondern von
Durchmessern abhängt, z.\,B. Grenzdurchmesser 7\,cm.} eine Länge < 2\,m.

\begin{equation}
  \hat{V_{AB}} = \frac{\pi}{4} \cdot 10^{-4} \cdot \sum_{n=1}^{n} L_{s,i} \cdot (\hat{D_{s,i}}^2 + VAR(\hat{D_{s,i}}))
\end{equation}

\subsubsection{Varianz}
Die Varianz des Abschnittsvolumens über mehrere Sektion wird berechnet als Summe
der Varianzen der Sektionen plus deren Kovarianz. Diese ist nicht Null, da die
geschätzten Durchmesser autokorreliert sind.
Da die (geschätzten) Varianzen für die Volumina einzelner Sektionen bekannt
sind, kann die Gesamtvarianz abgeleitet werden mit:
\begin{equation}
  VAR(Vol_{AB}) = \sum_{i=1}^n VAR(X_i) + 2\sum_{i<j} COV(Vol_i, Vol_j)
\end{equation}

Die Kovarianzen zwischen den Volumina der  einzelnen Sektionen berechnet durch
(vgl. letzte Formel in \citet{Kublinetal2013}, Anhang S. 996):
\begin{equation}
\begin{split}
  Cov(Vol_i, Vol_j)&= Cov(cm*D_i^2, cm*D_j^2)\\
  &= cm^2 * (4*E[D_i]*E[D_j]*Cor(D_i,D_j)*sd(D_i)*sd(D_j) \\
  &+ 2*Cor(D_i,D_j)^2 * var(D_i)*var(D_j))
\end{split}
\end{equation}
mit $cm=\frac{pi}{4}\cdot 10^{-4}\cdot L$

\subsubsection{Implementierung}
TODO: manuelle implementierung der Varianz

\begin{Schunk}
\begin{Sinput}
> L <- rep(1, length(hx)) # in [m]
> sum(pi/4 * 10^-4 * L * (estD[, "EDx"]^2 + estD[, "MSE_Mean"]))
\end{Sinput}
\begin{Soutput}
[1] 4.808455
\end{Soutput}
\begin{Sinput}
> tprVolume(tpr, AB=list(A=0, B=41, sl=1), iAB="H", interval = "confidence")
\end{Sinput}
\begin{Soutput}
       lwr       EVol        upr        MSE 
4.51281202 4.80845509 5.10409815 0.02275264 
\end{Soutput}
\begin{Sinput}
> TapeR::E_VOL_AB_HmDm_HT.f(Hm = c(1.3, 7), Dm = c(61.2, 49.8), mHt = 41, sHt = 0,
+                           A=0, B=41, iDH = "H", par.lme = tprm)[1:2]
\end{Sinput}
\begin{Soutput}
$E_VOL
[1] 4.817973

$VAR_VOL
[1] 0.02273838
\end{Soutput}
\end{Schunk}

Die TapeS-Funktion \verb|tprVolume| liefert das gleiche Ergebnis wie die
manuelle Implementierung. Das Ergebnis von TapeR, d.\,h. das Volumen aus dem
Rotationsintegral ist in diesem Fall leicht höher. Auch die Varianzschätzung für
das Volumen ist numerisch nicht identisch.

Wird die Segmentlänge in TapeS geringer gewählt, im Folgenden Beispiel 0.1\,m,
so nähert sich die Schätzung dem Rotationsintegral an.
\begin{Schunk}
\begin{Sinput}
> hx <- seq(0.05, 40.95, 0.1) # Segmente mit 0.1m Länge
> estD <- tprDiameter(tpr, Hx = hx, bark = TRUE, interval = "MSE")
> L <- rep(0.1, length(hx)) # in [m]
> sum(pi/4 * 10^-4 * L * (estD[, "EDx"]^2 + estD[, "MSE_Mean"]))
\end{Sinput}
\begin{Soutput}
[1] 4.816059
\end{Soutput}
\begin{Sinput}
> tprVolume(tpr, AB=list(A=0, B=41, sl=0.1), iAB="H", interval = "confidence")
\end{Sinput}
\begin{Soutput}
      lwr      EVol       upr       MSE 
4.5205549 4.8160592 5.1115636 0.0227313 
\end{Soutput}
\begin{Sinput}
> TapeR::E_VOL_AB_HmDm_HT.f(Hm = c(1.3, 7), Dm = c(61.2, 49.8), mHt = 41, sHt = 0,
+                           A=0, B=41, iDH = "H", par.lme = tprm)[1:2]
\end{Sinput}
\begin{Soutput}
$E_VOL
[1] 4.817973

$VAR_VOL
[1] 0.02273838
\end{Soutput}
\end{Schunk}


\subsection{Schätzung bei sHt > 0}
In diesem Fall bleibt Nutzungstechnisch alles gleich. Im Hintergrund werden die
größeren Varianzen insbesondere im oberen Schaftbereich berücksichtigt und die
Varianzen der Volumenschätzung etwas größer.




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Hochrechnung: Volumen mehrerer Bäume}
\subsection{Erwartungswert}
Neben dem Einzelbaumvolumen ist oftmals das Volumen (Derbholz, o.\,ä.) mehrerer
Bäume / eines Bestands / einer Inventur von Interesse. Der Schätzwert für das
Gesamtvolumen wird über die Summe der Einzelvolumina gebildet:
\begin{equation}
  Vol_{total} = \Sigma_{i=1}^{n} Vol_i
\end{equation}
bzw. falls noch ein (oder mehrere) Repäsentationfaktor genutzt wird:
\begin{equation}
  Vol_{total} = \Sigma_{i=1}^{n} Vol_i \cdot N_{ha, i}
\end{equation}

\subsection{Varianz}
Die Varianz für diese Bestandesschätzung unter Berücksichtigung eines (oder
mehrerer) Repräsentationsfaktores $a$ wird über die Formel:
\begin{equation}
  \operatorname{VAR}(a_1X_1+\ldots+a_nX_n) = a_1^2\operatorname{VAR}(X_i) + \ldots + a_n^2\operatorname{VAR}(X_n)
\end{equation}
geschätzt. Damit ergibt sich z.\,B. für das Hektarvolumen eine Varianz von:
\begin{equation}
  \operatorname{VAR}(Vol_{ha}) = \sum_{i=1}^{n} N_{ha,i}^2\operatorname{VAR}(Vol_i)
\end{equation}
Dies hat allerdings keine Bedeutung in TapeS, da dort nur Einzelbaumvolumina
bzw. nur Segmentvolumina geschätzt werden.

\bibliographystyle{spbasic}      % basic style, author-year citations
\bibliography{Lit}   % name your BibTeX data base
\end{document}
