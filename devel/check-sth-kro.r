## check the default values of height of crown base and stump height
require(mice)
p <- "J:/FVA-Projekte/P01278_EnNa/P01278_EnNa_BiomassFunc/Daten/aufbereiteteDaten/RData/Imp/"
ll <- list()
for(f in c("Fi", "Ta", "Kie", "Dgl", "Bu", "Ei", "Es", "Ba")){
  # f <- "Fi"
  load(file.path(p, paste0("Imp_", f, ".RData")))
  class(imp)
  df <- mice::complete(imp)
  ll[[f]] <- c(sth=median(df$Stockhoehe / df$Hoehe), hcb=median(df$KRO / df$Hoehe))
  #> 54%
  par(mfrow=c(2,2))
  plot(Stockhoehe ~ BHD, data=df, main=f)
  plot(Stockhoehe ~ Hoehe, data=df, main=f)
  plot(I(Stockhoehe/Hoehe) ~ Hoehe, data=df, main=f)
  abline(lm(I(Stockhoehe/Hoehe) ~ Hoehe, data=df))
  plot(I(Stockhoehe/Hoehe) ~ BHD, data=df, main=f)
  abline(lm(I(Stockhoehe/Hoehe) ~ BHD, data=df))

  plot(KRO ~ BHD, data=df, main=f)
  plot(KRO ~ Hoehe, data=df, main=f)
  plot(I(KRO/Hoehe) ~ Hoehe, data=df, main=f)
  abline(lm(I(KRO/Hoehe) ~ Hoehe, data=df))
  plot(I(KRO/Hoehe) ~ BHD, data=df, main=f)
  abline(lm(I(KRO/Hoehe) ~ BHD, data=df))
}
do.call(rbind, ll)
