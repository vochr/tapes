#' try build a tpr-model using
#' (i) the RBS-preprocessed data (script DeciduousTaperCurvefromRBS_MDM.r)
#' (ii) a stump approximation using BDAT
#' (iii) a shift crown base to 0.5 of tree height
#' (iv) use a DhGr-model for estimation of top diameter of 7cm
#' and build tpr-model with height of crown base as predictor
#'
#' data and models comes from DeciduousTaperCurvefromRBS_MDM.r
#' stored in H:/FVA-Projekte/P01697_BWI_TapeR/Programme/Eigenentwicklung/96_publication/data.rdata

rm(list=ls())
p <- "H:/FVA-Projekte/P01697_BWI_TapeR/Programme/Eigenentwicklung/96_publication/"
load(file.path(p, "data.rdata"))
#> baum RBS-tree data
#> mm[[6]] GAM model for HDhGr / oak
#> mm[[7]] GAM model for HDhGr / beech
#> res volume estimation results for different approaches
#> segment reconstructed taper curve data, i.e. volume curve
#> smoothed smoothed reconstructed taper curve data (average of all 2 resp. 3 paths)
#> t taper curve based on 'segment' data, based on all data (i.e. oak + beech)
#> ts taper curve based on 'smoothed' data, based on all data (i.e. oak + beech)

bid <- 11901
ss <- subset(segment, id==bid)
plot(d~h, data=ss, type="b")
b <- subset(baum, Id==bid)
abline(v=c(b$KRO, ss$HDhGr[1]))
points(x=c(1.3, 7), y=c(b$BHD, b$b_D7)/10, pch=3, cex=2)
sm <- subset(smoothed, id==bid)
points(x=sm$h, y=sm$d, type="b", pch=2, col="blue")
