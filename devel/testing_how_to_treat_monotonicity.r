par.lme <- TapeS::SKPar[[2]]
R0 <- FALSE
Ht <- 12
xm <- c(1.3, 3.6)/Ht
ym <- c(8, 7.5)
xp <- seq(0, 1, 0.01)
tpr <- tprTrees(spp=3, Dm=ym, Hm=xm*Ht, Ht=Ht)
plot(tpr)
points(x=Hm(tpr), y=Dm(tpr))
str(tpr)

## example with correct taper curve
# Ht <- 12
# xm <- c(0.1, 1.3, 3.6)/Ht
# ym <- c(9, 8, 7.5)
# xp <- seq(0, 1, 0.01)
# tpr <- tprTrees(spp=3, Dm=ym, Hm=xm*Ht, Ht=Ht)
# str(tpr)
# plot(tpr)
# points(x=Hm(tpr), y=Dm(tpr))


EBLUP_b_k = MSE_Mean = MSE_Pred = CI_Mean = CI_Pred = NULL

#   Design Matrizen X und Z zu den Kalibrierungsdaten :.........................................

x_k 	= xm
y_k   = ym

X_k = TapeR:::XZ_BSPLINE.f(x_k, par.lme$knt_x, par.lme$ord_x)
Z_k = TapeR:::XZ_BSPLINE.f(x_k, par.lme$knt_z, par.lme$ord_z)

#   Feste Effekte - Mittlere Schaftkurve in der GesamtPopulation (PA) : E[y|x] = X*b_fix:.......

b_fix 		= par.lme$b_fix
KOVb_fix    = par.lme$KOVb_fix

#   Kovarianz fuer die Random Effekte (fit.lme):................................................

KOV_b 		= par.lme$KOVb_rnd
Z_KOVb_Zt_k = Z_k%*%KOV_b%*%t(Z_k)                                			# fix(Z_KOVb_Zt)

#   Residuen Varianz:...........................................................................

sig2_eps = par.lme$sig2_eps
dfRes    = par.lme$dfRes

if(isTRUE(R0)){

  R_k = diag(0, ncol(Z_KOVb_Zt_k))                 			#	Interpolation der Messwerte

} else {

  R_k = diag(sig2_eps, ncol(Z_KOVb_Zt_k))

}

#   Kovarianzmatrix der Beobachtungen (Sigma.i) :...............................................

#	rm(KOV_y_k, KOVinv_y_k)

KOV_y_k		= Z_KOVb_Zt_k + R_k                       	#   SIGMA_k in V&C(1997)(6.2.3)
KOVinv_y_k  = solve(KOV_y_k);                    		#   SIGMA_k^(-1)

#   EBLUP - Posterior mean (b_k) aus Einhaengung (xm,ym) berechnen :.............................

#   ***************************************************************
EBLUP_b_k 	= KOV_b%*%t(Z_k)%*%KOVinv_y_k%*%(y_k - X_k%*%b_fix); #   V&C(1997) (6.2.49)
#   ***************************************************************

#	x_pre 	= unique(xp[order(xp)])
#	x_pre 	= xp[order(xp)]
x_pre 	= xp

X_kh = TapeR:::XZ_BSPLINE.f(x_pre, par.lme$knt_x, par.lme$ord_x)
Z_kh = TapeR:::XZ_BSPLINE.f(x_pre, par.lme$knt_z, par.lme$ord_z)

#   ********************************************************************************************
yp_fix = EBLUP_y_kh  = X_kh%*%b_fix
yp = EBLUP_y_kh  = X_kh%*%b_fix + Z_kh%*%EBLUP_b_k    		#	V&C(1997) (6.2.52)

# random deviation
plot(x=xp*Ht, y=y_rnd, type="l")
points(x=xp*Ht, y=Z_kh[,1]*EBLUP_b_k[1], type="l", lty=2)
points(x=xp*Ht, y=Z_kh[,2]*EBLUP_b_k[2], type="l", lty=3)
points(x=xp*Ht, y=Z_kh[,3]*EBLUP_b_k[3], type="l", lty=4)
points(x=xp*Ht, y=Z_kh[,4]*EBLUP_b_k[4], type="l", lty=5)

## unrestricted estimate of taper curve
yp = X_kh%*%b_fix + Z_kh%*%EBLUP_b_k
plot(x=xp*Ht, y=yp, type="l")
points(x=Hm(tpr), y=Dm(tpr))

## resetting parameter estimate of first Rnd-BSpline to match size of Fix-BSpline
EB <- EBLUP_b_k
EB[1] <- -b_fix[1] + attr(tpr@monotone, "maxD")
yp = X_kh%*%b_fix + Z_kh%*%EB
plot(x=xp*Ht, y=yp, type="l")
points(x=Hm(tpr), y=Dm(tpr))
#> not monotone either

EB <- EBLUP_b_k
EB[1] <- .5 * EB[1]
yp = X_kh%*%b_fix + Z_kh%*%EB
plot(x=xp*Ht, y=yp, type="l")
points(x=Hm(tpr), y=Dm(tpr))
#> monotone but arbitrary


yp = X_kh%*%b_fix + Z_kh%*%EBLUP_b_k
maxD <- attr(tpr@monotone, "maxD")
if((b_fix[1] + EBLUP_b_k[1]) < maxD){
  HmaxD <- attr(tpr@monotone, "HmaxD")
  yp <- ifelse(yp < maxD & xp < HmaxD/Ht, maxD, yp)
}
plot(x=xp*Ht, y=yp, type="l")
points(x=HmaxD, y=maxD, pch=3)
points(x=Hm(tpr), y=Dm(tpr))
# this makes the taper curve monotone, but not continuous

## compare KOV etc. from TapeS models between different species ####
require(TapeS)
t(sapply(1:8, function(a) SKPar[[a]]$b_fix))
sapply(1:8, function(a) SKPar[[a]]$KOVb_fix)
sapply(1:8, function(a) SKPar[[a]]$sig2_eps)
sapply(1:8, function(a) SKPar[[a]]$dfRes)
lapply(1:8, function(a) SKPar[[a]]$KOVb_rnd)
lapply(1:8, function(a) cov2cor(SKPar[[a]]$KOVb_rnd))

## how comes, EBLUP_b_k[1] can get that big (e.g. -33.59)? ####

