#' pre-smoothing mit

rm(list=ls())
## run DeciduousTaperCurvefromRBS_MDM.r before this script!
p <- "J:/FVA-Projekte/P01697_BWI_TapeR/Programme/Eigenentwicklung/96_publication/"
load(file.path(p, "data.rdata"))

head(baum)
head(smoothed)
nrow(merge(baum[, c("Id", "KRO")], res[, c("Id", "KRO")])) # same KRO data in res & Baum

smoothed <- merge(smoothed, baum[, c("Id", "KRO", "b_DerbholzVol")],
                  by.x = "id", by.y = "Id", all.x = TRUE)
colnames(smoothed)[colnames(smoothed) == "b_DerbholzVol"] <- "DhVol"

## optimise diameter in 70% of tree height ####
i <- 11701
t <- subset(smoothed, id==i)
t <- t[order(t$relH),]
head(t)

ht <- t$ht[1]
kro <- t$KRO[1]
vol <- t$DhVol[1]
x <- 25

h <- t[t$h < t$KRO,]$h
d <- approx(x = t[t$h < t$KRO,]$h, y = t[t$h < t$KRO,]$d,
            xout = seq(min(h), max(h), 0.1))$y
h <- c(seq(min(h), max(h), 0.1), x, ht)
d <- c(d, 7, 0)

require(TapeR)
knt <- c(0, 0.1, 0.7, 1)
BS <- TapeR:::XZ_BSPLINE.f(x = h/ht, knt = knt, ord = 4)
head(BS)

matplot(BS, type = "l")

m <- lm(d ~ BS - 1)
summary(m)

plot(d ~ h, data=t, type="o")
points(x=h, y=d, pch=3, cex=2)

xpred <- seq(0, 1, 0.01)
BSp <- TapeR:::XZ_BSPLINE.f(x = xpred, knt = knt, ord = 4)
yhat <- BSp %*% coef(m)
points(x=xpred*ht, y=yhat, type="l", lty=2, col=2)

i <- 22102
u <- 22102
u <- 16901
t <- subset(smoothed, id==u)
t <- t[order(t$relH),]
head(t)
d <- t$d
h <- t$h
ht <- t$ht[1]
kro <- t$KRO[1]
vol <- t$DhVol[1]
x <- mean(c(kro, ht))
sll=1
bnr <- u

f <- function(x, d, h, ht, vol, bnr, step=0.1, sll=2, vis=FALSE){
  # x <- 25
  # print(x)
  # step <- 1
  d <- approx(x = h, y = d,
              xout = seq(min(h), kro, step))$y
  h <- seq(min(h), kro, step)
  hmind <- h[d==min(d)]
  d <- c(d[h<=hmind], rep(7, 20), 3.5, 1.75, 0)
  h <- c(h[h<=hmind], rep(x, 20), x + (ht-x)/2, x + (ht-x)/4*3, ht)

  require(TapeR)
  knt <- c(0, 0.1, 0.7, 1)
  BS <- TapeR:::XZ_BSPLINE.f(x = h/ht, knt = knt, ord = 4)

  m <- lm(d ~ BS - 1)
  if(any(is.na(coef(m)))){
    if(isTRUE(vis)){
      plot(d ~ h, data=t, type="o", las=1, main=bnr, ylim=c(0, max(t$d, dhat)),
           xlim=range(c(0, ht)))
      abline(v=kro)
      abline(h=7, col="grey")
      points(x=h, y=d, pch=3)
    }
    loss <- NA
    attr(loss, "info") <- list(negDm=NA, diffVol=NA, crownDm=NA, stump=NA)
    warning(paste0("ID: ",i , " (coef with NA)"))
  } else {
    # summary(m)

    xpred <- seq(0, 1, 0.001)
    BSp <- TapeR:::XZ_BSPLINE.f(x = xpred, knt = knt, ord = 4)
    yhat <- BSp %*% coef(m)

    h7 <- approx(x = yhat, y = xpred*ht, xout = 7)$y
    # h7 <- xpred[which.min(abs(yhat-7))] * ht
    sl <- TapeS:::slfun(A = 0, B = h7, sl = sll)
    dhat <- approx(x = xpred*ht, y = yhat, xout = sl$Hx)$y
    volhat <- sum(pi * (dhat/200)^2 * sl$L)
    volhat <- ifelse(any(yhat < 0), 0, volhat)
    if(isTRUE(vis)){
      plot(d ~ h, data=t, type="o", las=1, main=bnr, ylim=c(0, max(t$d, dhat)),
           xlim=range(c(0, ht)))
      BB <- BSp
      BB[,1] <- BB[,1] * coef(m)[1]
      BB[,2] <- BB[,2] * coef(m)[2]
      BB[,3] <- BB[,3] * coef(m)[3]
      BB[,4] <- BB[,4] * coef(m)[4]
      BB[,5] <- BB[,5] * coef(m)[5]
      matplot(x = matrix(xpred*ht, ncol = 1), y = BB, type="l", add=TRUE)
      abline(v=kro)
      abline(h=7, col="grey")
      points(x=h, y=d, pch=3)
      points(x=xpred*ht, y=yhat, type="l", lty=2, col="red")
      # add cylinders for vol estimate
      sapply(seq(along=dhat), function(a) {
        # a <- 1
        rect(xleft = sl$Hx[a] - (sl$L[a] / 2), xright = sl$Hx[a] + (sl$L[a] / 2),
             ybottom = 0, ytop = dhat[a])
        # lines(x = hh, y=dd, col="blue")
      })
      # add BDAT
      dbh <- approx(x = h, y = d, xout = 1.3)$y
      d03 <- approx(x = h, y = d, xout = 0.3*ht)$y
      dhat <- rBDAT::getDiameter(list(spp=15, D1=dbh, D2=d03, H2=0.3*ht, H=ht,
                                      Hx = xpred*ht))
      points(x=xpred*ht, y=dhat, type="l", col="blue")
      volbdat <- rBDAT::getVolume(list(spp=15, D1=dbh, D2=d03, H2=0.3*ht, H=ht))
      # add TapeS
      tps <- tprTrees(spp=37, Dm = c(dbh, d03), Hm = c(1.3, 0.3*ht), Ht = ht)
      dhat <- TapeS::tprDiameter(tps, Hx = xpred*ht)
      points(x=xpred*ht, y=dhat, type="l", col="darkgreen")
      voltapes <- TapeS::Vfm(tps)
      # legend
      legend("bottomleft", legend=c(paste("Vol_NEW =", round(c(volhat), 4)),
                                    paste("Vol_RBS =", round(c(vol), 4)),
                                    "Dm für Anpassung",
                                    paste("BDAT =", round(c(volbdat), 4)),
                                    paste("TapeS =", round(c(voltapes), 4))),
             lty=c(2, 1, NA, 1, 1), col=c("red", "black", "black", "blue", "darkgreen"),
             pch=c(NA, 1, 3, NA, NA), bg = "white")
    }
    loss <- abs(volhat - vol)
    kroDm <- approx(x = xpred*ht, y = yhat, xout = kro)$y
    Dm01 <- approx(x = xpred*ht, y = yhat, xout = 0.1)$y
    Dm05 <- approx(x = xpred*ht, y = yhat, xout = 0.5)$y
    attr(loss, "info") <- list(negDm=any(yhat<0),
                               diffVol=abs((volhat - vol) / vol) > 0.05,
                               crownDm=max(yhat[xpred*ht>kro]) > kroDm*1.1, # 10% increase ok
                               stump=Dm01 < Dm05)
  }

  return(loss)
}

f(x=0.9*ht, d = t$d, h = t$h, ht = ht, vol = vol, sll = 1, bnr = u, vis = TRUE)
f(x=0.7*ht, d = t$d, h = t$h, ht = ht, vol = vol, step = 1, sll = 1, bnr = u, vis = TRUE)

h <- t$h
d <- t$d
res <- optimize(f = f(x, d, h, ht, vol), interval = c(kro, ht),
                d=d, h=h, ht=ht, vol=vol, step=1, sll=1, bnr=u)
res
f(res$minimum, d, h, ht, vol, step=1, sll = 1, bnr=u, vis = TRUE)


pdf("J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_BS_v2.pdf")
ll <- list()
uid <- sort(unique(smoothed$id))
# for(i in seq(along=uid)[1:20]){
for(i in seq(along=uid)){
  # i <- 1
  u <- uid[i]
  t <- subset(smoothed, id==u)
  t <- t[order(t$relH),]
  # head(t)

  ## optimise
  d <- t$d
  h <- t$h
  ht <- t$ht[1]
  kro <- t$KRO[1]
  vol <- t$DhVol[1]
  x <- 0.9*ht
  # f(x-2, d, h, ht, vol, sll = 1, bnr = u, vis = TRUE)
  res <- optimize(f = f(x, d, h, ht, vol, bnr, sll), interval = c(kro-1, ht),
                  d=d, h=h, ht=ht, vol=vol, bnr=u, step=0.5, sll=1)

  f(res$minimum, d, h, ht, vol, step=.5, sll=1, bnr=u, vis = TRUE)

  ll[[i]] <- as.data.frame(c(id=u, attr(res$objective, "info")))
  rm(res)
}
dev.off()

res <- do.call(rbind, ll)
res$sum <- rowSums(res[, -1])
colSums(res[res$sum>0, 2:5])
View(res[res$sum>0,])
View(res)

library('staplr')
staplr::select_pages(
  selpages = which(res$negDm>0),
  input_filepath = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_BS_v2.pdf",
  output_filepath = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_BS_v2-negDm.pdf")

staplr::select_pages(
  selpages = which(res$diffVol>0),
  input_filepath = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_BS_v2.pdf",
  output_filepath = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_BS_v2-diffVol.pdf")

staplr::select_pages(
  selpages = which(res$crownDm>0),
  input_filepath = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_BS_v2.pdf",
  output_filepath = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_BS_v2-crownDm.pdf")

staplr::select_pages(
  selpages = which(res$stump>0),
  input_filepath = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_BS_v2.pdf",
  output_filepath = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_with_BS_v2-stump.pdf")

#' problematic taper curve imputation
#' * dhat values below zero at top (eg. 66901, 78504)
#' * DhVol estimate unequal to RBS-DhVol (eg 66201)
#' * too high Dhat values at top (eg. 78404)
#' * stump not with monotonically decreasing dhat values (eg. 79704)
#' * most of DhVol is below crown base and the decrease in Dm must be strong inside crown
