#' fitting a nls model under side conditions
#'
#' CV
#' 
#' assume we have a taper curve, which is only valid until crown base.
#' inside crown, there is no continuous stem axis and volume is made up by 
#' several branches. Total merchantable volume should be estimated by the same
#' algorithm as for continuous stem species, hence we need to impute / estimate
#' a virtual taper curve inside crown, under the side condition, that the 
#' rotational integral returns total merchantable volume.
#' 

rm(list=ls()) 
## run DeciduousTaperCurvefromRBS_MDM.r before this script!
p <- "J:/FVA-Projekte/P01697_BWI_TapeR/Programme/Eigenentwicklung/96_publication/"
load(file.path(p, "data.rdata"))

head(baum)
head(smoothed)
nrow(merge(baum[, c("Id", "KRO")], res[, c("Id", "KRO")])) # same KRO data in res & Baum 

smoothed <- merge(smoothed, baum[, c("Id", "KRO", "b_DerbholzVol")], 
                  by.x = "id", by.y = "Id", all.x = TRUE)
colnames(smoothed)[colnames(smoothed) == "b_DerbholzVol"] <- "DhVol"

## optimise diameter in 70% of tree height ####
d <- t[t$h < t$KRO,]$d
h <- t[t$h < t$KRO,]$h
ht <- t$ht[1]
vol <- t$DhVol[1]
x <- 25
f <- function(x, d, h, ht, vol, vis=FALSE){
  # print(x)
  tpr <- TapeS::tprTrees(spp=15, Dm = c(d, x), Hm = c(h, 0.7*ht), Ht = ht, sHt = 0)
  if(isTRUE(vis)){
    plot(tpr, obs = T)
  }
  volhat <- TapeS::tprVolume(tpr)
  return(abs(volhat - vol))
}

res <- optimize(f = f(x, d, h, ht, vol), interval = c(0, 2*max(d)), 
                d=d, h=h, ht=ht, vol=vol)
res
f(res$minimum, d, h, ht, vol, vis = TRUE)


pdf("J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_v1.pdf")
for(i in unique(smoothed$id)){
  # i <- 11701
  t <- subset(smoothed, id==i)
  t <- t[order(t$relH),]
  head(t)
  
  plot(d ~ h, data=t, type="o", main=i)
  abline(v=t$KRO[1])
  
  ## optimise crown part
  d <- t[t$h < t$KRO,]$d
  h <- t[t$h < t$KRO,]$h
  ht <- t$ht[1]
  vol <- t$DhVol[1]
  
  res <- optimize(f = f(x, d, h, ht, vol), interval = c(0, 2*max(d)), 
                  d=d, h=h, ht=ht, vol=vol)
  
  tpr <- TapeS::tprTrees(spp=15, Dm = c(d, res$minimum), Hm = c(h, 0.7*ht), 
                         Ht = ht, sHt = 0)
  
  hx <- seq(0, ht, 0.1)
  dhat <- TapeS::tprDiameterCpp(tpr, Hx=hx)
  points(x=hx, y=dhat, type="l", col="red")
  points(x = c(h, 0.7*ht), y = c(d, res$minimum), pch=3, col="red")
  
}
dev.off()

## optimise height of D=7cm ####
d <- t[t$h < t$KRO,]$d
h <- t[t$h < t$KRO,]$h
ht <- t$ht[1]
kro <- t$KRO[1]
vol <- t$DhVol[1]
x <- mean(c(kro, ht))
f <- function(x, d, h, ht, vol, vis=FALSE){
  # print(x)
  tpr <- TapeS::tprTrees(spp=15, Dm = c(d, 7), Hm = c(h, x), Ht = ht, sHt = 0)
  if(isTRUE(vis)){
    plot(tpr, obs = T)
  }
  volhat <- TapeS::tprVolume(tpr)
  return(abs(volhat - vol))
}

res <- optimize(f = f(x, d, h, ht, vol), interval = c(kro, ht), 
                d=d, h=h, ht=ht, vol=vol)
res
f(res$minimum, d, h, ht, vol, vis = TRUE)


pdf("J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_v2.pdf")
for(i in unique(smoothed$id)){
  # i <- 11701
  t <- subset(smoothed, id==i)
  t <- t[order(t$relH),]
  head(t)
  
  plot(d ~ h, data=t, type="o", main=i)
  abline(v=t$KRO[1])
  
  ## optimise crown part
  d <- t[t$h < t$KRO,]$d
  h <- t[t$h < t$KRO,]$h
  ht <- t$ht[1]
  kro <- t$KRO[1]
  vol <- t$DhVol[1]
  
  res <- optimize(f = f(x, d, h, ht, vol), interval = c(kro, ht), 
                  d=d, h=h, ht=ht, vol=vol)
  
  tpr <- TapeS::tprTrees(spp=15, Dm = c(d, 7), Hm = c(h, res$minimum), 
                         Ht = ht, sHt = 0)
  
  hx <- seq(0, ht, 0.1)
  dhat <- TapeS::tprDiameterCpp(tpr, Hx=hx)
  points(x=hx, y=dhat, type="l", col="red")
  points(x = c(h, res$minimum), y = c(d, 7), pch=3, col="red")
  
}
dev.off()


## optimise with smoothing spline and only crown wood volume ####
d <- t[t$h < t$KRO,]$d
h <- t[t$h < t$KRO,]$h
ht <- t$ht[1]
kro <- t$KRO[1]
vol <- t$DhVol[1]
x <- kro + (1-0.618)*(ht - kro)
f <- function(x, d, h, ht, vol, vis=FALSE){
  # print(x)
  sl <- TapeS:::slfun(0, max(h), sl = 2)
  dhat <- approx(x = h, y = d, xout = sl$Hx)$y
  (stem <- sum(pi * (dhat/200)^2 * sl$L))
  
  (crvol <- vol - stem)
  if(x >= ht) x <- 16.5
  tpr <- TapeS::tprTrees(spp=15, 
                         Dm = c(d, 7), 
                         Hm = c(h, x),
                         Ht = ht, sHt = 0)
  if(isTRUE(vis)){
    plot(tpr, obs = TRUE)
    points(x=h, y=d)
    abline(v=kro)
  }
  volhat <- TapeS::tprVolume(tpr, AB = list(A=max(h), B=7), iAB=c("h", "dob"))
  
  return(abs(volhat - crvol))
}

res <- optimize(f = f(x, d, h, ht, vol), interval = c(kro, ht), 
                d=d, h=h, ht=ht, vol=vol)
res
f(res$minimum, d, h, ht, vol, vis = TRUE)


pdf("J:/BuI/Persoenlich/Vonderach/TapeS/devel/RBS_pre-smoothing_as_in_BDAT_v3.pdf")
for(i in unique(smoothed$id)[1:20]){
  # i <- 11701
  t <- subset(smoothed, id==i)
  t <- t[order(t$relH),]
  head(t)
  
  plot(d ~ h, data=t, type="o", main=i)
  abline(v=t$KRO[1])
  
  ## optimise crown part
  d <- t[t$h < t$KRO,]$d
  h <- t[t$h < t$KRO,]$h
  ht <- t$ht[1]
  vol <- t$DhVol[1]
  
  res <- optimize(f = f(x, d, h, ht, vol), interval = c(kro, ht-0.1), 
                  d=d, h=h, ht=ht, vol=vol)
  
  tpr <- TapeS::tprTrees(spp=15, 
                         Dm = c(d[which.max(h)], 7), 
                         Hm = c(h[which.max(h)], res$minimum), 
                         Ht = ht, sHt = 0)
  
  hx <- seq(0, ht, 0.1)
  dhat <- TapeS::tprDiameterCpp(tpr, Hx=hx)
  points(x=hx, y=dhat, type="l", col="red")
  points(x = c(max(h), res$minimum), y = c(d[which.max(h)], 7), pch=3, col="red")
  
  ## construct new taper curve based on measurements below crown and 
  ## the estimated taper curve, which returns proper crown volume
  dnew <- c(d, approx(x = hx, y = dhat, xout = seq(max(h), ht, 2))$y, 0)
  hnew <- c(h, seq(max(h), ht, 2), ht)
  points(x = hnew, y = dnew, pch=4, cex=2, col="blue")
  ## volume estimation
  sl <- TapeS:::slfun(A = 0, B = res$minimum, sl = 2)
  dd <- approx(x=c(h, hx[hx > max(h)]), y=c(d, dhat[hx > max(h)]), xout = sl$Hx)$y
  vnew <- sum(pi * (dd/200)^2 * sl$L)
  
  legend("bottomleft", legend=c(paste("Vol_NEW =", round(c(vnew), 4)),
                                paste("Vol_RBS =", round(c(vol), 4))), 
         lty=1, col=c("blue", "black"), pch=c(4, 1))
}
dev.off()


