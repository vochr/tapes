#' try estimate a crown expansion factor as a function of f(D1, H, H2 and Kro)
#' using the biomass data from EnNa

require(mice)

rm(list=ls())
load("H:/FVA-Projekte/P01278_EnNa/P01278_EnNa_BiomassFunc/Daten/aufbereiteteDaten/RData/Imp/Imp_Bu.RData")
head(dat <- complete(imp))
dat <- dat[, c("Studie", "b_Art", "Teilstudie", "Standort","Baumart", "BHD",
               "Hnn", "Hoehe", "Alter", "KRO", "D03", "Stockhoehe",
               "DerbGesamt", "DerbGesamtmitRinde",
               "KronenDerb", "KronenDerbmitRinde",
               "HAKronenDerb", "HAKronenDerbmitRinde",
               "AstDerb", "AstDerbmitRinde")]
head(dat)
dat$AstDhAnteil <- dat$AstDerb / dat$KronenDerb
dat$AstDhAnteil <- ifelse(is.na(dat$AstDhAnteil), 0, dat$AstDhAnteil)
dat$q03 <- dat$D03 / dat$BHD

for(var in c("BHD", "Hoehe", "KRO", "D03", "q03", "Alter")){
  # var <- "BHD"
  par(mfrow=c(1,1))
  plot(dat$AstDhAnteil ~ dat[, var], xlab=var, ylab = "AstDhAnteil [%]",
       xlim=c(0, max(dat[, var])), pch=ifelse(dat$AstDhAnteil<=0, 3, 1),
       main=paste0("AstDhAnteil vs. ", var))
  if(var=="BHD") abline(v=7)
  lines(smooth.spline(x = dat[dat$AstDhAnteil > 0, var],
                      y = dat[dat$AstDhAnteil > 0, ]$AstDhAnteil),
        lty=2, lwd=2, col="red")
  dat$tmp <- dat[, var]
  abline(m <- lm(AstDhAnteil ~ tmp, data=dat, subset=(AstDhAnteil>0)), col=3, lwd=2, lty=2)
  abline(m <- lm(AstDhAnteil ~ tmp - 1, data=dat, subset=(AstDhAnteil>0)), col=3, lwd=2)
  n <- try(nls(AstDhAnteil ~ SSlogis(tmp, Asym, xmid, scal),
               data=dat, subset=(AstDhAnteil>0), control = nls.control(5000),
               start = c(Asym=0.5, xmid=45, scal=35)), silent = TRUE)
  if(!class(n)=="try-error"){
    xx <- 1:max(dat[, var])
    points(x=xx, y=predict(n, newdata=data.frame(tmp=xx)), type="l", lwd=2)
  }
  # par(mfrow=c(2,2))
  # plot(m)
  # par(mfrow=c(1,1))
  # plot(n)
}


require(nlme)
g <- gnls(AstDhAnteil ~ SSlogis(BHD, Asym, xmid, scal),
          data=dat[dat$AstDhAnteil>0, ],
          params=list(Asym+xmid+scal~ 1),
          start = c(Asym=0.5, xmid=45, scal = 35))
summary(g)
plot(g)
AIC(g)

g2 <- gnls(AstDhAnteil ~ SSlogis(BHD, Asym, xmid, scal),
           data=dat[dat$AstDhAnteil>0, ],
           params=list(Asym ~ 1,
                       xmid ~ Hoehe + KRO,
                       scal ~ 1),
           start = c(Asym=c(0.5),
                     xmid=c(45, 0.1, 0.1),
                     scal = c(35)))
summary(g2)
plot(g2)
AIC(g2)
#> these are the only signicant effects considering Hoehe, KRO, D03, q03

plot(AstDhAnteil ~ BHD, data=dat, ylab = "AstDhAnteil [%]",
     pch=ifelse(dat$AstDhAnteil<=0, 3, 1),
     main=paste0("AstDhAnteil vs. ", var), xlim=c(0, 100))
dm <- 1:180
cols <- rainbow(n=9)
for(q in c(17, 50, 83)){
  h <- iS::estHeight(dm, sp = 15, qtl = q)
  for(k in seq(0.1, 0.9, 0.1)){
    pred <- predict(g2, newdata=data.frame(BHD=dm, Hoehe=h, KRO=k*h))
    points(x=dm, y=pred, type="l", col=cols[k*10],
           lty=ifelse(q==17, 1, ifelse(q==50, 2, 3)))
  }
}


## testing th bu model by
for(hbc in seq(0.1, 0.9, 0.1)){
  # hbc <- 0.3
  spp <- 15
  dm <- 15:180
  h <- iS::estHeight(d13 = dm, sp = spp)
  n <- length(dm)
  #plot(h ~ dm, main=tprSpeciesCode(spp, "lang"))
  bdt <- rBDAT::buildTree(list(spp=spp, D1=dm, H2=50, H=h))
  t <- bdat_as_tprtrees(bdt)
  dhvol <- TapeS::tprVolume(t, bark = FALSE)
  HAkrovol <- TapeS::tprVolume(t, AB = list(A=hbc*Ht(t), B=7), iAB = c("H", "dob"),
                               bark = FALSE) # main axis crown volume
  stavol <- dhvol - HAkrovol # stem volume
  #plot(dhvol ~ dm, main=tprSpeciesCode(spp, "lang"))
  bm <- tprBiomass(t, component = c("sw", "agb"))
  #plot(bm[, "sw"] ~ dm, main=tprSpeciesCode(spp, "lang"))
  dh <- TapeS::nsur(spp = BaMap(type = 6)[rep(spp, n)], dbh = dm, ht = h,
                    sth = 0.01*h, d03 = D03(t), kl = hbc*h)
  plot(dh[, "sw"] ~ dm, main=tprSpeciesCode(spp, "lang"), type="l",
       ylab="solid wood [kg]", xlab="dbh [cm]")
  points(x=dm, y=bm[,"sw"], type="l", lty=2)
  legend("topleft", legend = c("Allometry", "Marklund"), lty=c(1,2))

  ## calculate specific gravity between volume and biomass
  grav <- dh[, "sw"] / dhvol # nsur / taper
  grav2 <- bm[,"sw"] / dhvol # marklund / taper

  bdatvol <- rBDAT::getVolume(tree = list(spp=spp, D1=dm, H=h), bark=FALSE)
  bdtgrav <- dh[, "sw"] / bdatvol # nsur / bdat
  bdtgrav2 <- bm[,"sw"] / bdatvol # marklund / bdat

  plot(grav ~ dm, main=paste0(tprSpeciesCode(spp, "lang"), ", HBC=", hbc),
       ylim=c(range(c(grav, grav2, bdtgrav, bdtgrav2))),
       ylab = " specific gravity [kg/m3]", xlab="Dbh [cm]",
       type="l", lwd=2, col=2)
  points(x=dm, y=grav2, lty=3, type="l", lwd=2, col=2)
  points(x=dm, y=bdtgrav, lty=1, type="l", lwd=2, col=4)
  points(x=dm, y=bdtgrav2, lty=3, type="l", lwd=2, col=4)
  legend("bottomleft", legend=c("Marklund/TapeR", "NSUR/TapeR", "Marklund/BDAT", "NSUR/BDAT"),
         lty=c(3,1,3,1), col=c(2,2,4,4), lwd=2)

  ## correct TapeR-Volume by the crown expansion models
  AstDhAnteil <- predict(g2, newdata=data.frame(BHD=dm, Hoehe=h, KRO=hbc), type="response")
  AstDh <- AstDhAnteil * (HAkrovol / (1 - AstDhAnteil))
  KroVol <- HAkrovol + AstDh
  plot(KroVol ~ dm, type="l", xlab="Bhd", ylab="volume [m3]")
  points(x=dm, y=AstDh, type="l", lty=2)
  points(x=dm, y=HAkrovol, type="l", lty=3)
  bdt$A <- hbc*bdt$H
  bdt$B <- 7
  points(x=dm, y=rBDAT::getVolume(bdt, iAB=c("H", "dob"), bark=FALSE),
         type="l", lty=4)
  legend("topleft", legend=c("crown", "main axis", "branches", "BDAT-crown"),
         lty = c(1,3,2,4))

  dhvolkorr <- stavol + KroVol
  grav <- dh[, "sw"] / dhvolkorr # nsur / taper
  grav2 <- bm[,"sw"] / dhvolkorr # marklund / taper

  plot(grav ~ dm, main=paste0(tprSpeciesCode(spp, "lang"), ", HBC=", hbc),
       ylim=c(range(c(grav, grav2, bdtgrav, bdtgrav2))),
       ylab = " specific gravity [kg/m3]", xlab="Dbh [cm]",
       type="l", lwd=2, col=2)
  points(x=dm, y=grav2, lty=3, type="l", lwd=2, col=2)
  points(x=dm, y=bdtgrav, lty=1, type="l", lwd=2, col=4)
  points(x=dm, y=bdtgrav2, lty=3, type="l", lwd=2, col=4)
  legend("bottomleft", legend=c("Marklund/TapeR", "NSUR/TapeR", "Marklund/BDAT", "NSUR/BDAT"),
         lty=c(3,1,3,1), col=c(2,2,4,4), lwd=2)
}
#' doesn't look so good. The relation between volume and biomass is not better
#' than with the other models.
