ex1 <- 30 # 30cm Bhd
sx1 <- 0.5 # 0.5 cm standardabweichung
x1 <- rnorm(1000, ex1, sx1)
x2 <- rnorm(1000, ex1, sx1)
plot(x=x1, y=x1)
plot(x1 ~ x2)
cov(x1, x2)
cov(x1, x1)


## expected value of square of x, i.e. dependence between both factors
xx <- x1*x1
mean(xx)
mean(x1) * mean(x1) # too low
mean(x1) * mean(x1) + var(x1) # correct
mean(x1) * mean(x1) + cov(x1, x1) # correct, the same

## variance of square of x
var(xx)
## Wiki: unabhängige Variable Var(x*y), bw. hier angewandt als var(x*x)
# https://de.wikipedia.org/wiki/Varianz_(Stochastik)#Summen_und_Produkte
mean(x1)^2*var(x1)+mean(x1)^2*var(x1)+var(x1)*var(x1) # wrong, much too low
# Formel 12' in Kublin et al. (2013) S. 989 für Varianz der Volumenberechnung
var(x1)*var(x1) + 2*cov(x1, x1)^2 + 4*mean(x1)*mean(x1)*cov(x1, x1) # good!
# reformulated
3*var(x1)^2 + 4*mean(x1)*mean(x1)*var(x1) # good!


## expected value of product, independent x and y
mean(x1*x2)
mean(x1) * mean(x2) # almost correct, not 100% independent
mean(x1) * mean(x2) + cov(x1, x2) # correct


## unabhängige Variable
var(x1*x2)
# https://de.wikipedia.org/wiki/Varianz_(Stochastik)#Summen_und_Produkte
mean(x1)^2*var(x2) + mean(x2)^2*var(x1) + var(x1)*var(x2) # almost

# Formel 12' in Kublin et al. (2013) S. 989 für Varianz der Volumenberechnung
# bezieht sich auf COV(x^2, Y^2)
cov(x1^2, x2^2)
var(x1)*var(x2) + 2*cov(x1, x2)^2 + 4*mean(x1)*mean(x2)*cov(x1, x2) # good



