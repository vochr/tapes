## buche ####
test <- function(BHD, Hoehe){
 ## simple beech bm function from  Vonderach et al 2018
 Stockhoehe <- 0.007*Hoehe
 return((0.0315 * BHD^2.1447 * Stockhoehe^0.798)
 + (0.004 * BHD^1.9184 * Stockhoehe^0.8076)
 + (-4.6332 + 0.019 * BHD^2.0861 * Hoehe^0.9502)
 + (0.0013 * BHD^1.9596 * Hoehe^1.099)
 + (0.4006 * BHD^2.3211 * Hoehe^-0.7636))
}
test(BHD = 90.81826, Hoehe = 41.25800)
sum(nsur2(spp = 5, dbh = 90.81826, ht = 41.25800)[, -1])

## eiche ####
test <- function(BHD, Hoehe){
 ## simple oak bm function from  Vonderach et al 2018
 Stockhoehe <- 0.007*Hoehe
 return((0.0363 * BHD^2.0657 * Stockhoehe^0.7721)
        + (0.0158 * BHD^1.791 * Stockhoehe^0.9032)
        + (-3.9731 + 0.0223 * BHD^2.1012 * Hoehe^0.8647)
        + (0.0043 * BHD^1.9437 * Hoehe^0.9576)
        + (0.3028 * BHD^2.1945 * Hoehe^-0.6882))
}
test(BHD = 90.81826, Hoehe = 41.25800)
sum(nsur2(spp = 6, dbh = 90.81826, ht = 41.25800)[, -1])

## fichte ####
test <- function(BHD, Hoehe){
  ## simple fichte bm function from  Vonderach et al 2018
  Stockhoehe <- 0.007*Hoehe
  return((0.022 * BHD^2.1212 * Stockhoehe^0.6056)
         + (-0.0128 + 0.0067 * BHD^1.7268 * Stockhoehe^0.5947)
         + (0.0142 * BHD^1.7414 * Hoehe^1.2401)
         + (0.0038 * BHD^1.6076 * Hoehe^1.0528)
         + (1.8472 + 0.0243 * BHD^2.9671 * Hoehe^-0.8183)
         + (-1.6847 + 0.285 * BHD^2.1173 * Hoehe^-0.8334))
}
test(BHD = 90.81826, Hoehe = 41.25800)
sum(nsur2(spp = 1, dbh = 90.81826, ht = 41.25800)[, -1])

## Kiefer ####
test <- function(BHD, Hoehe){
  ## simple kie bm function from  Vonderach et al 2018
  Stockhoehe <- 0.007*Hoehe
  return((0.0624 * BHD^1.9322 * Stockhoehe^1.0414)
         + (0.0077 * BHD^1.8127 * Stockhoehe^0.8732)
         + (0.0173 * BHD^2.0072 * Hoehe^0.914)
         + (0.0055 * BHD^2.0108 * Hoehe^0.5374)
         + (0.1316 * BHD^2.444 * Hoehe^-0.949)
         + (0.1484 * BHD^2.332 * Hoehe^-1.2026))
}
test(BHD = 90.81826, Hoehe = 41.25800)
sum(nsur2(spp = 3, dbh = 90.81826, ht = 41.25800)[, -1])

## Douglasie ####
test <- function(BHD, Hoehe){
  ## simple dgl bm function from  Vonderach et al 2018
  Stockhoehe <- 0.007*Hoehe
  return((0.0186 * BHD^2.185 * Stockhoehe^0.7723)
         + (0.0032 * BHD^2.0357 * Stockhoehe^0.7621)
         + (0.0131 * BHD^1.9299 * Hoehe^1.0715)
         + (0.0018 * BHD^1.9099 * Hoehe^1.0306)
         + (0.2784 * BHD^3.1276 * Hoehe^-1.7984)
         + (-1.8821 + 0.2749 * BHD^2.4833 * Hoehe^-1.3051))
}
test(BHD = 90.81826, Hoehe = 41.25800)
sum(nsur2(spp = 4, dbh = 90.81826, ht = 41.25800)[, -1])

## Tanne ####
test <- function(BHD, Hoehe){
  ## simple dgl bm function from  Vonderach et al 2018
  Stockhoehe <- 0.007*Hoehe
  return((0.0121 * BHD^2.2645 * Stockhoehe^0.7596)
         + (0.0036 * BHD^2.1225 * Stockhoehe^0.7856)
         + (0.0046 * BHD^1.1917 * Hoehe^2.2103)
         + (0.0019 * BHD^1.4458 * Hoehe^1.678)
         + (0.0273 * BHD^2.2573)
         + (0.1071 * BHD^1.6952))
}
test(BHD = 90.81826, Hoehe = 41.25800)
sum(nsur2(spp = 2, dbh = 90.81826, ht = 41.25800)[, -1])

## Bergahorn ####
test <- function(BHD, Hoehe){
  ## simple dgl bm function from  Vonderach et al 2018
  Stockhoehe <- 0.007*Hoehe
  return((0.0596 * BHD^1.9934 * Stockhoehe^0.8314)
         + (0.0121 * BHD^1.8062 * Stockhoehe^0.8178)
         + (0.017 * BHD^2.071 * Hoehe^0.9317)
         + (0.004 * BHD^2.0287 * Hoehe^0.7446)
         + (0.0426 * BHD^2.1166))
}
test(BHD = 90.81826, Hoehe = 41.25800)
sum(nsur2(spp = 7, dbh = 90.81826, ht = 41.25800)[, -1])

## Esche ####
test <- function(BHD, Hoehe){
  ## simple dgl bm function from  Vonderach et al 2018
  Stockhoehe <- 0.007*Hoehe
  DH <- BHD * Hoehe
  return((0.0111 * BHD^2.4593 * Stockhoehe^0.7579)
         + (0.0367 * BHD^1.4515 * Stockhoehe^0.7298)
         + (0.022 * BHD^2.0683 * Hoehe^0.905)
         + (0.0006 * DH^1.7301)
         + (0.0966 * BHD^1.9989))
}
test(BHD = 90.81826, Hoehe = 41.25800)
sum(nsur2(spp = 8, dbh = 90.81826, ht = 41.25800)[, -1])

#> all functions return the correct result for the tested tree
