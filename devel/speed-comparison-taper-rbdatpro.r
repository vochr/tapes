#' microbenchmark TapeR vs. rBDATPRO

library(rbenchmark)
library(TapeS)
library(TapeR)
library(rBDATPRO)

## prepare TapeR ####
data("SK.par.lme")
(datT1 <- new("trees", spp=c(1L), Dm=list(c(30, 28)),
             Hm=list(c(1.3, 5)), H=c(30)))
(datT2 <- new("trees", spp=c(1L,1L), Dm=list(c(30, 28), c(40, 38)),
            Hm=list(c(1.3, 5), c(1.3, 5)), H=c(30, 40)))
d1 <- runif(1000, 10, 90)
a <- iS:::HtCoef(sp = 1)[["a"]]
b <- iS:::HtCoef(sp = 1)[["b"]]
h <- 1.3 + (a + b/d1)^-3
dm <- matrix(c(d1, 0.8*d1), nrow = length(d1), ncol = 2, byrow = FALSE)
dm <- lapply(seq_len(nrow(dm)), function(i) dm[i,])
hm <- rep(list(c(1.3, 5)), length.out=1000)
datT1k <- new("trees", spp=rep(1L, length(d1)), Dm=dm, Hm=hm, H=h)


## prepare rBDATPRO ####
datB1 <- buildTree(list(spp=1, D1=30, D2=28, H2=5, H=30))
datB2 <- buildTree(list(spp=1, D1=c(30, 40), D2=c(28, 38), H2=5, H=c(30, 40)))
d1 <- runif(1000, 10, 90)
a <- iS:::HtCoef(sp = 1)[["a"]]
b <- iS:::HtCoef(sp = 1)[["b"]]
datB1k <- buildTree(list(spp=1,D1=d1, D2=0, H2=50, H=1.3+(a+b/d1)^-3))


## benchmarking ####
## one trees - one diameter
benchmark(TapeS::getDiameter(datT1, Hx = 1.3),
          rBDATPRO::getDiameter(datB1, Hx=1.3),
          replications = 1000)[1:4]

## two trees - one diameter
benchmark(TapeS::getDiameter(datT2, Hx = 1.3),
          rBDATPRO::getDiameter(datB2, Hx=1.3),
          replications = 1000)[1:4]

## 1000 trees - one diameter
benchmark(TapeS::getDiameter(datT1k, Hx = 1.3),
          rBDATPRO::getDiameter(datB1k, Hx=1.3),
          replications = 100)[1:4]

## one trees - two diameter
benchmark(TapeS::getDiameter(datT1, Hx = c(1.3, 2)),
          rBDATPRO::getDiameter(datB1, Hx=c(1.3, 2)),
          replications = 1000)[1:4]

## two trees - two diameter
benchmark(TapeS::getDiameter(datT2, Hx = c(1.3, 2)),
          rBDATPRO::getDiameter(datB2, Hx=c(1.3, 2)),
          replications = 1000)[1:4]

## 1000 trees - two diameter
benchmark(TapeS::getDiameter(datT1k, Hx = c(1.3, 2)),
          rBDATPRO::getDiameter(datB1k, Hx=c(1.3, 2)),
          replications = 100)[1:4]

## one trees - five diameter
benchmark(TapeS::getDiameter(datT1, Hx = 1:5),
          rBDATPRO::getDiameter(datB1, Hx=1:5),
          replications = 1000)[1:4]

## two trees - five diameter
benchmark(TapeS::getDiameter(datT2, Hx = 1:5),
          rBDATPRO::getDiameter(datB2, Hx=1:5),
          replications = 1000)[1:4]

## 1000 trees - five diameter
benchmark(TapeS::getDiameter(datT1k, Hx = 1:5),
          rBDATPRO::getDiameter(datB1k, Hx=1:5),
          replications = 100)[1:4]

# rmarkdown::render("H:/BuI/Persoenlich/Vonderach/TapeS/tmp/speed-comparison-taper-rbdatpro.r")
