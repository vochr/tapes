
require(TapeS)
require(RODBC)

tpr <- tprTrees(spp=1, Dm = c(32.8, 29.3), Hm=c(1.3, 0.3*19.7), Ht=19.7, sHt = 0)

vol <- tprVolume(tpr, interval = "prediction")
n_ha <- 47.33936

vol_ha <- vol["EVol"] * n_ha
vvol <- n_ha^2 * vol["MSE"]

con <- odbcConnectAccess("J:/FVA-Projekte/P01612_KNOW/Daten/aufbereiteteDaten/weham_projections/wehambasis/allspecies/WEHAM_SzBasis_2099/WEHAM_OUTPUTXX40.mdb")
df <- sqlFetch(con, sqtable = "wehamo_wzp", max=12)
odbcClose(con)

df <- df[, c("Bhd", "D03", "Hoe", "N_ha")]
colnames(df) <- c("Bhd", "D03", "Hoehe", "N_ha")
df$BaTpr <- 1
n_ha <- df$N_ha
df$N_ha <- NULL
tpr <- nfi_as_tprtrees(df)
vol <- tprVolume(tpr, interval = "prediction")
(vol_ha <- sum(vol[, "EVol"] * n_ha))
(varvol <- sum(n_ha^2 * vol[, "MSE"]))
# interval
c(vol_ha - 2*sqrt(varvol), vol_ha, vol_ha + 2*sqrt(varvol))

## if height is estimated (sdH = 1m)
tpr@sHt <- rep(1, length(tpr))
(vol <- tprVolume(tpr, interval = "prediction"))
(vol_ha <- sum(vol[, "EVol"] * n_ha))
(varvol <- sum(n_ha^2 * vol[, "MSE"]))
# interval
c(vol_ha - 2*sqrt(varvol), vol_ha, vol_ha + 2*sqrt(varvol))

## if height is estimated (sdH = 2m)
tpr@sHt <- rep(2, length(tpr))
(vol <- tprVolume(tpr, interval = "prediction"))
(vol_ha <- sum(vol[, "EVol"] * n_ha))
(varvol <- sum(n_ha^2 * vol[, "MSE"]))
# interval
c(vol_ha - 2*sqrt(varvol), vol_ha, vol_ha + 2*sqrt(varvol))
