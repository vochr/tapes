library(TapeS)
tpr <- tprTrees()
tpr
tprDiameter(tpr, Hx = 1.3)

detach("package:TapeS", unload = TRUE)
tpr <- TapeS::tprTrees()
TapeS::tprDiameter(tpr, Hx = 1.3)
TapeS::tprAssortment(tpr)
TapeS::tprBark(tpr, Hx = 1.3)
TapeS:::SKPar
SKPar
TapeS:::BMM
data(BMM, package = "TapeS")

