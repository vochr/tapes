#' small tree volume problem
# rmarkdown::render("H:/BuI/Persoenlich/Vonderach/TapeS/devel/uniroot_problem_with_small_non_monotone_trees.r",
#                   output_format = "pdf_document", output_file = "uniroot_problem.pdf")

require(TapeS)

tpr <- tprTrees(spp=3, Dm=10, Hm=1.3, H=12)
plot(tpr)

tprDiameter(tpr, Hx=seq(0, 12, 0.1)) # OK
try(tprVolume(tpr)) # error in uniroot, because height D=7cm required
tprVolume(tpr, AB = list(A=0, B=1), iAB = "H") # OK, no diameter search necessary!
try(tprHeight(tpr, Dx=7)) # error in uniroot

pl <- TapeS::SKPar[[2]]
TapeR::E_DHx_HmDm_HT.f(Hx=1.3, Hm=1.3, Dm=10, mHt = 12, par.lme = pl) # OK
try(TapeR::E_HDx_HmDm_HT.f(Dx=7, Hm=1.3, Dm=10, mHt = 12, par.lme = pl)) # error in uniroot

# cf. TapeR::E_HDx_HmDm_HT.f
mHt <- 12
Dx <- 7
Hm <- 1.3
Dm <- 10
sHt <- 0
par.lme <- pl
try(uniroot(TapeR:::Hx_root.f, c(0, mHt), tol = 1e-05,
        Dx, Hm, Dm, mHt, sHt = sHt, par.lme))

uniroot(TapeR:::Hx_root.f, c(2, mHt), tol = 1e-05,
        Dx, Hm, Dm, mHt, sHt = sHt, par.lme) # with appropriate interval => OK!

uniroot(TapeR:::Hx_root.f, c(0, 2), tol = 1e-05,
        Dx, Hm, Dm, mHt, sHt = sHt, par.lme) # with appropriate interval => OK!
#' in BDAT Nullstellensuche nach dem Pegasusverfahren



#' was macht BDAT mit kleinen Bäumen:
install.packages("rBDAT")
require(rBDAT)
plot(buildTree(list(spp=3, D1=10, H=12)))
plot(buildTree(list(spp=3, D1=9, H=12)))
plot(buildTree(list(spp=3, D1=8, H=12)))

#' Die Evaluierung der Schaftkurve wird in BDAT mit der Funktion Fdrel()
#' ausgeführt. Allerdings wird diese idR über die Subroutine Kuwert() aufgerufen.
#' Diese Funktion korrigiert aufsteigende Kurvenbereiche (die offenbar nur
#' unterhalb des D1 resp. Bhd liegen; obs: relative Höhe 0 = Baumspitze) in dem
#' alle Werte unterhalb der D1-Messstelle auf die Messung dort (=Bhd) setzen.
#' vgl.
#' if(Hhrel > Hurel){
#'   D1rel <- Fdrel(D095,D07,H,eins)
#'   if(D1rel < Durel){
#'     Kw <- Dnorm
#'     return(Kw)
#'   } else {
#'     Kw <- Fdrel(D095,D07,H,Hhrel)*Yy*Dnorm
#'   }
#'   } else {
#'     Kw <- Fdrel(D095,D07,H,Hhrel)*Yy*Dnorm
#' }
