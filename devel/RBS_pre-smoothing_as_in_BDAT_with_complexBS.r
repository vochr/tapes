#' test flexibility of cubic B-Splines
#' depending on position and number of knots

rm(list=ls())

## test B-Splines ####
require(TapeR)
ht <- 21.4
h <- seq(0, ht, 0.01)
knt <- list(c(0, 0.1, 0.7, 1), c(0, 0.1, 1), c(0, 0.1, 0.5, 1),
            c(0, 0.1, 0.5, 0.5, 1), c(0, 0.1, 0.5, 0.5, 0.5, 1),
            c(0, 0.1, 0.5, 0.5, 0.5, 0.6, 1),
            c(0, 0.2, 0.5, 0.5, 0.5, 0.7, 1))
for(i in seq(along=knt)){
  BS <- TapeR:::XZ_BSPLINE.f(x = h/ht, knt = knt[[i]], ord = 4)
  matplot(x=h, y=BS, type = "l", main=paste(knt[[i]], collapse = "-"))
  abline(v=knt[[i]]*ht)
  # invisible(sapply(1:8, function(a) plot(x=h, y=BS[, a], type="l", main=a)))
}

# id 82604 from RBS-DB
h1 <- c(1.1, 2.1, 3.2, 4.3, 5.3, 6.4, 7.5, 8.6, 9.6, 10.7, 11.8)
d <- c(27.1, 26.2, 25.0, 23.7, 23.0, 22.3, 21.3, 20.5, 19.6, 18.8, 17.6)
h2 <- c(12.8, 13.9, 15.0, 16.1, 17.1, 18.2, 19.3, 20.3, 21.2, 21.4)
d1 <- c(10, 5, 3, 2.5, 2, 1.4, 1.0, 0.5, 0.3, 0.0) # below expected (artificial)
d2 <- c(16.6, 15.3, 13.8, 12.1, 10.3, 8.1, 5.6, 3.1, 0.6, 0.0) # expected (using TapeS)
d3 <- c(17.8, 17.6, 18.6, 17.5, 16.1, 16.4, 11.5, 5.3, 1.0, 0.0) # above expected (virtual Dm)

hx <- c(h1, h2)
dx <- c(d, d2)
plot(dx ~ hx, type="o")
abline(v=12, h=0)
points(x=c(h1, h2), y=c(d, d3), type="o", lty=2)
points(x=c(h1, h2), y=c(d, d1), type="o", lty=3)

## expected ####
hx <- c(h1, h2)
dx <- c(d, d2)

kntx <- c(0, 0.2, rep(12/ht, 3), 0.7, 1)
BS <- TapeR:::XZ_BSPLINE.f(x = hx/ht, knt = kntx, ord = 4)
m <- lm(dx ~ BS - 1)
summary(m)

plot(dx ~ hx, type="o")
points(x=hx, y=dx, pch=3, cex=2)

xpred <- seq(0, 1, 0.01)
BSp <- TapeR:::XZ_BSPLINE.f(x = xpred, knt = kntx, ord = 4)
yhat <- BSp %*% coef(m)
points(x=xpred*ht, y=yhat, type="l", lty=2, col=2, lwd=2)

## above ####
hx <- c(h1, h2)
dx <- c(d, d3)

kntx <- c(0, 0.2, rep(12/ht, 3), 0.7, 1)
BS <- TapeR:::XZ_BSPLINE.f(x = hx/ht, knt = kntx, ord = 4)
m <- lm(dx ~ BS - 1)
summary(m)

plot(dx ~ hx, type="o")
points(x=hx, y=dx, pch=3, cex=2)

xpred <- seq(0, 1, 0.01)
BSp <- TapeR:::XZ_BSPLINE.f(x = xpred, knt = kntx, ord = 4)
yhat <- BSp %*% coef(m)
points(x=xpred*ht, y=yhat, type="l", lty=2, col=2, lwd=2)

## below ####
hx <- c(h1, h2)
dx <- c(d, d1)

kntx <- c(0, 0.2, rep(12/ht, 3), 0.7, 1)
BS <- TapeR:::XZ_BSPLINE.f(x = hx/ht, knt = kntx, ord = 4)
m <- lm(dx ~ BS - 1)
summary(m)

plot(dx ~ hx, type="o")
points(x=hx, y=dx, pch=3, cex=2)

xpred <- seq(0, 1, 0.01)
BSp <- TapeR:::XZ_BSPLINE.f(x = xpred, knt = kntx, ord = 4)
yhat <- BSp %*% coef(m)
points(x=xpred*ht, y=yhat, type="l", lty=2, col=2, lwd=2)


## mit weniger Durchmessern ####
## expected ####
hx <- c(h1, h2)
dx <- c(d, d2)
d7 <- 7
(h7 <- approx(x=dx, y=hx, xout=7)$y)

hx <- c(h1, h7, ht)
dx <- c(d, 7, 0)
kntx <- c(0, 0.2, rep(12/ht, 3), 0.7, 1)
BS <- TapeR:::XZ_BSPLINE.f(x = hx/ht, knt = kntx, ord = 4)
m <- lm(dx ~ BS - 1)
summary(m)
m$coefficients[is.na(m$coefficients)] <- 0

plot(dx ~ hx, type="o")
points(x=hx, y=dx, pch=3, cex=2)
abline(h=7)

xpred <- seq(0, 1, 0.01)
BSp <- TapeR:::XZ_BSPLINE.f(x = xpred, knt = kntx, ord = 4)
yhat <- BSp %*% coef(m)
points(x=xpred*ht, y=yhat, type="l", lty=2, col=2, lwd=2)
abline(v= 12)

h7 <- approx(x=yhat, y=xpred*ht, xout=7)$y
sl <- TapeS:::slfun(A = 0, B = h7, sl = 2)
dhat <- approx(x = xpred*ht, y = yhat, xout = sl$Hx)$y
sapply(seq(along=dhat), function(a) {
  # a <- 1
  rect(xleft = sl$Hx[a] - (sl$L[a] / 2), xright = sl$Hx[a] + (sl$L[a] / 2),
       ybottom = 0, ytop = dhat[a])
  # lines(x = hh, y=dd, col="blue")
})
(volhat <- sum(pi * (dhat/200)^2 * sl$L))

## above ####
hx <- c(h1, h2)
dx <- c(d, d3)
d7 <- 7
(h7 <- approx(x=dx, y=hx, xout=7)$y)

hx <- c(h1, h7, ht)
dx <- c(d, 7, 0)
kntx <- c(0, 0.2, rep(12/ht, 3), 0.7, 1)
BS <- TapeR:::XZ_BSPLINE.f(x = hx/ht, knt = kntx, ord = 4)
m <- lm(dx ~ BS - 1)
summary(m)
m$coefficients[is.na(m$coefficients)] <- 0

plot(dx ~ hx, type="o")
points(x=hx, y=dx, pch=3, cex=2)
abline(h=7)

xpred <- seq(0, 1, 0.01)
BSp <- TapeR:::XZ_BSPLINE.f(x = xpred, knt = kntx, ord = 4)
yhat <- BSp %*% coef(m)
points(x=xpred*ht, y=yhat, type="l", lty=2, col=2, lwd=2)
abline(v= 12)

h7 <- approx(x=yhat, y=xpred*ht, xout=7)$y
sl <- TapeS:::slfun(A = 0, B = h7, sl = 2)
dhat <- approx(x = xpred*ht, y = yhat, xout = sl$Hx)$y
sapply(seq(along=dhat), function(a) {
  # a <- 1
  rect(xleft = sl$Hx[a] - (sl$L[a] / 2), xright = sl$Hx[a] + (sl$L[a] / 2),
       ybottom = 0, ytop = dhat[a])
  # lines(x = hh, y=dd, col="blue")
})
(volhat <- sum(pi * (dhat/200)^2 * sl$L))

## below ####
hx <- c(h1, h2)
dx <- c(d, d1)
d7 <- 7
h7 <- approx(x=dx, y=hx, xout=7)$y

hx <- c(h1, h7, ht)
dx <- c(d, 7, 0)
kntx <- c(0, 0.2, rep(12/ht, 3), 0.7, 1)
BS <- TapeR:::XZ_BSPLINE.f(x = hx/ht, knt = kntx, ord = 4)
m <- lm(dx ~ BS - 1)
summary(m)
m$coefficients[is.na(m$coefficients)] <- 0

plot(dx ~ hx, type="o")
points(x=hx, y=dx, pch=3, cex=2)
abline(h=7)

xpred <- seq(0, 1, 0.01)
BSp <- TapeR:::XZ_BSPLINE.f(x = xpred, knt = kntx, ord = 4)
yhat <- BSp %*% coef(m)
points(x=xpred*ht, y=yhat, type="l", lty=2, col=2, lwd=2)
abline(v= 12)

h7 <- approx(x=yhat, y=xpred*ht, xout=7)$y
sl <- TapeS:::slfun(A = 0, B = h7, sl = 2)
dhat <- approx(x = xpred*ht, y = yhat, xout = sl$Hx)$y
sapply(seq(along=dhat), function(a) {
  # a <- 1
  rect(xleft = sl$Hx[a] - (sl$L[a] / 2), xright = sl$Hx[a] + (sl$L[a] / 2),
       ybottom = 0, ytop = dhat[a])
  # lines(x = hh, y=dd, col="blue")
})
(volhat <- sum(pi * (dhat/200)^2 * sl$L))
