#' check the estimate of random effects using TapeR

library(RSQLite)
library(TapeR)
library(dplyr)

## from help of TapeR_FIT_LME.f()
# load example data
rm(list=ls())
data(DxHx.df)

## get data ####
#### Models from Vfl-Data
p <- "J:/FVA-Projekte/P01697_BWI_TapeR"
db <- file.path(p, "Daten/Ergebnisse/SektionsDB.sqlite")
conn <- RSQLite::dbConnect(RSQLite::SQLite(), db)
# dbfile <- RSQLite::dbReadTable(conn, name = "herkunft")
sql1 <- paste0("SELECT * FROM baumdaten WHERE BA = 1 AND FID == 1 AND dataOK = 1;")
bd <- DBI::dbGetQuery(conn, sql1) # Baumdaten
sql2 <- paste0("SELECT sektionsdaten.BID, rel, sekD1, sekD2, sekH, Hoehe ", 
               "FROM sektionsdaten ", 
               "INNER JOIN baumdaten ", 
               "ON sektionsdaten.BID=baumdaten.BID ", 
               "WHERE BA = 1 AND baumdaten.FID = 1 AND dataOK = 1 AND useRec = 1;")
sek <- DBI::dbGetQuery(conn, sql2) # Sektionsdaten
RSQLite::dbDisconnect(conn) # closing

#### prepare data ####
sek <- sek %>%
  rowwise() %>%
  mutate(D = ifelse(is.na(sekD1) & is.na(sekD2), NA,  # mean diameters in cm
                    mean(c(sekD1, sekD2), na.rm = TRUE) / 10),
         H = ifelse(rel==1, (sekH / 100) * (Hoehe / 10), sekH / 100), # height in m
         Hrel = (H / (Hoehe / 10))) %>% # relative measurement height
  arrange(BID, Hrel) %>% 
  dplyr::select(BID, D, H, Hrel) %>% 
  as.data.frame()

## add relative measurement of tree top, i.e. sekH=H/H and sekD=0/D05=0
sek <- bd %>% 
  mutate(D = 0, # in cm
         H = Hoehe / 10, # in cm
         Hrel = 1) %>% 
  dplyr::select(BID, D, H, Hrel) %>% 
  rbind(sek) %>% 
  arrange(BID, Hrel)

keepsek <- sek
sek <- keepsek

sek[sek$BID==sek$BID[1],]
length(unique(sek$BID)) # 6044 trees

# define the relative knot positions and order of splines
knt_x = c(0.0, 0.1, 0.75, 1.0);	ord_x = 4 # B-Spline knots: fix effects; order (cubic = 4)
knt_z = c(0.0, 0.1       ,1.0); ord_z = 4 # B-Spline knots: rnd effects

# one reference model with all data
Id = sek$BID
x = sek$Hrel 
y = sek$D
taper.model.all <- TapeR_FIT_LME.f(Id, x, y, knt_x, ord_x, knt_z, ord_z,
                                   IdKOVb = "pdSymm", 
                                   control = list(opt="optim",
                                                  maxIter=500,
                                                  msMaxIter = 500, 
                                                  msMaxEval = 500, 
                                                  msVerbose=TRUE))

## split data
stree <- sample(unique(sek$BID), size = 100) # hold out sample trees
sek2 <- sek[sek$BID %in% stree, ] 
sek <- sek[!(sek$BID %in% stree), ] # data without sample tree

(share <- c(1, 5, 10, 15, seq(20, 100, 20)))
ll <- list()
ill <- 0
for(i in seq(along=share)){
  for(iter in 1:10){
    ill <- ill + 1
    # i <- 2
    s <- share[i]
    # prepare the data (could be defined in the function directly)
    ids <- sample(unique(sek$BID), size = s/100 * length(unique(sek$BID)), replace = FALSE)
    df <- sek[sek$BID %in% ids, ] 
    Id = df$BID
    x = df$Hrel 
    y = df$D
    
    # fit the model
    taper.model <- TapeR_FIT_LME.f(Id, x, y, knt_x, ord_x, knt_z, ord_z,
                                   IdKOVb = "pdSymm", 
                                   control = list(opt="optim",
                                                  maxIter=500,
                                                  msMaxIter = 500, 
                                                  msMaxEval = 500, 
                                                  msVerbose=TRUE))
    
    ## estimate ranef of 100 hold out trees
    re <- t(sapply(unique(sek2$BID), function(a){
      # a <- unique(sek2$BID)[1]
      #   Design Matrizen X und Z zu den Kalibrierungsdaten :.........................................
      df <- subset(sek2, BID == a)
      (dbh <- bd[bd$BID == a, "Bhd"]/10)
      (ht <- bd[bd$BID == a, "Hoehe"]/10)
      (d53 <- approx(x = df$H, y = df$D, xout = 5.3))
      x_k 	= df$Hrel[df$Hrel<1]
      y_k   = df$D[df$Hrel<1]
      
      X_k = TapeR:::XZ_BSPLINE.f(x_k, taper.model$par.lme$knt_x, taper.model$par.lme$ord_x)
      Z_k = TapeR:::XZ_BSPLINE.f(x_k, taper.model$par.lme$knt_z, taper.model$par.lme$ord_z)
      
      #   Feste Effekte - Mittlere Schaftkurve in der GesamtPopulation (PA) : E[y|x] = X*b_fix:.......
      b_fix 		= taper.model$par.lme$b_fix
      KOVb_fix    = taper.model$par.lme$KOVb_fix
      
      #   Kovarianz fuer die Random Effekte (fit.lme):................................................
      KOV_b 		= taper.model$par.lme$KOVb_rnd
      Z_KOVb_Zt_k = Z_k%*%KOV_b%*%t(Z_k)                                			# fix(Z_KOVb_Zt)
      
      #   Residuen Varianz:...........................................................................
      sig2_eps = taper.model$par.lme$sig2_eps
      dfRes    = taper.model$par.lme$dfRes
      
      R_k = diag(sig2_eps, ncol(Z_KOVb_Zt_k))
      
      #   Kovarianzmatrix der Beobachtungen (Sigma.i) :...............................................
      
      KOV_y_k		= Z_KOVb_Zt_k + R_k                       	#   SIGMA_k in V&C(1997)(6.2.3)
      KOVinv_y_k  = solve(KOV_y_k);                    		#   SIGMA_k^(-1)
      
      #   EBLUP - Posterior mean (b_k) aus Einhaengung (xm,ym) berechnen :.............................
      (EBLUP_b_k 	= KOV_b%*%t(Z_k)%*%KOVinv_y_k%*%(y_k - X_k%*%b_fix))
    }))
    rownames(re) <- unique(sek2$BID)
    re <- re[order(as.numeric(rownames(re)), decreasing = F),]
    head(re)
    re.all <- ranef(taper.model.all$fit.lme)
    re.all <- re.all[as.integer(row.names(re.all)) %in% unique(sek2$BID),]
    re.all <- re.all[order(as.numeric(rownames(re.all)), decreasing = F),]
    head(re.all)
    re.diff <- re.all - re # obs - pred
    head(re.diff)
    re.sd <- apply(re.diff, MARGIN = 2, FUN = sd)
    
    ## bind results
    ll[[ ill ]] <- list(iter=iter,
                        share=s,
                        beta.all = fixef(taper.model.all$fit.lme),
                        beta.s = fixef(taper.model$fit.lme),
                        sd_eblub = re.sd,
                        sig2_eps = taper.model$fit.lme$sigma^2)
  }
}


pdf("J:/BuI/Persoenlich/Vonderach/TapeS/devel/estimate-ranef-new-tree-alld.pdf",
    paper = "a4r", width = 20, height = 10)
# fixed effect for submodels
fxd <- as.data.frame(t(sapply(1:length(ll), function(a){ll[[a]]$beta.s})))
fxd$i <- (sapply(1:length(ll), function(a){ll[[a]]$iter}))
fxd$s <- (sapply(1:length(ll), function(a){ll[[a]]$share}))
fxd$n <- as.integer((sapply(1:length(ll), function(a){ll[[a]]$share})) * nrow(bd) / 100)
fxd <- aggregate(fxd[, c("n", "X1", "X2", "X3", "X4", "X5")], by=list(s=fxd$s), FUN=mean)
head(fxd)
plot(X1 ~ s, type="n", data=fxd, ylim=range(fxd[, grepl("X", colnames(fxd))]), 
     xlab="share of data [%]", ylab="param-value")
abline(h=fixef(taper.model.all$fit.lme), col="grey")
points(x=fxd$s, y=fxd$X1, type="o", col=2)
points(x=fxd$s, y=fxd$X2, type="o", col=3)
points(x=fxd$s, y=fxd$X3, type="o", col=4)
points(x=fxd$s, y=fxd$X4, type="o", col=5)
points(x=fxd$s, y=fxd$X5, type="o", col=6)
axis(3, at=fxd$s, labels = fxd$n)
legend("topright", legend=c("X1", "X2", "X3", "X4", "X5"), col=2:6, lty=1, pch=1)

# show sd of random effects
rnd <- as.data.frame(t(sapply(1:length(ll), function(a){ll[[a]]$sd_eblub})))
rnd$i <- (sapply(1:length(ll), function(a){ll[[a]]$iter}))
rnd$s <- (sapply(1:length(ll), function(a){ll[[a]]$share}))
rnd$n <- as.integer((sapply(1:length(ll), function(a){ll[[a]]$share})) * nrow(bd) / 100)
rnd <- aggregate(rnd[, c("n", "Z1", "Z2", "Z3", "Z4")], by=list(s=rnd$s), FUN=mean)
plot(Z1 ~ s, type="n", data=rnd, ylim=range(rnd[, grepl("Z", colnames(rnd))]), 
     xlab="share of data [%]", ylab="sd(ranef)")
points(x=rnd$s, y=rnd$Z1, type="o", col=2)
points(x=rnd$s, y=rnd$Z2, type="o", col=3)
points(x=rnd$s, y=rnd$Z3, type="o", col=4)
points(x=rnd$s, y=rnd$Z4, type="o", col=5)
axis(3, at=fxd$s, labels = fxd$n)
legend("topright", legend=c("Z1", "Z2", "Z3", "Z4"), col=2:5, lty=1, pch=1)

## residual error
# # fixed effect for submodels
eps <- data.frame(eps=(sapply(1:length(ll), function(a){ll[[a]]$sig2_eps})))
eps$i <- (sapply(1:length(ll), function(a){ll[[a]]$iter}))
eps$s <- (sapply(1:length(ll), function(a){ll[[a]]$share}))
eps$n <- as.integer((sapply(1:length(ll), function(a){ll[[a]]$share})) * nrow(bd) / 100)
eps <- aggregate(eps[, c("n", "eps")], by=list(s=eps$s), FUN=mean)
head(eps)
plot(eps ~ s, type="n", data=eps, #ylim=c(0, max(eps[, "eps"])),
     xlab="share of data [%]", ylab="sd of residual error")
abline(h=taper.model.all$fit.lme$sigma^2, col="grey")
points(x=eps$s, y=eps$eps, type="o", col=2)
axis(3, at=eps$s, labels = eps$n)
legend("topright", legend=c("eps"), col=2, lty=1, pch=1)
dev.off()

## now try the same with calibrating using d13 and d53
(share <- c(1, 5, 10, 15, seq(20, 100, 20)))
# (share <- c(1, 5, 10, 15))
ll <- list()
ill <- 0
for(i in seq(along=share)){
  for(iter in 1:10){
    ill <- ill + 1
    # i <- 2
    s <- share[i]
    # prepare the data (could be defined in the function directly)
    ids <- sample(unique(sek$BID), size = s/100 * length(unique(sek$BID)), replace = FALSE)
    df <- sek[sek$BID %in% ids, ] 
    Id = df$BID
    x = df$Hrel 
    y = df$D
    
    # fit the model
    taper.model <- TapeR_FIT_LME.f(Id, x, y, knt_x, ord_x, knt_z, ord_z,
                                   IdKOVb = "pdSymm", 
                                   control = list(opt="optim",
                                                  maxIter=500,
                                                  msMaxIter = 500, 
                                                  msMaxEval = 500, 
                                                  msVerbose=TRUE))
    
    ## estimate ranef of 100 hold out trees
    re <- t(sapply(unique(sek2$BID), function(a){
      # a <- unique(sek2$BID)[1]
      #   Design Matrizen X und Z zu den Kalibrierungsdaten :.........................................
      df <- subset(sek2, BID == a)
      (dbh <- bd[bd$BID == a, "Bhd"]/10)
      (ht <- bd[bd$BID == a, "Hoehe"]/10)
      (d53 <- approx(x = df$H, y = df$D, xout = 5.3)$y)
      (x_k 	= c(1.3/ht, 5.3/ht))
      (y_k   = c(dbh, d53))
      
      X_k = TapeR:::XZ_BSPLINE.f(x_k, taper.model$par.lme$knt_x, taper.model$par.lme$ord_x)
      Z_k = TapeR:::XZ_BSPLINE.f(x_k, taper.model$par.lme$knt_z, taper.model$par.lme$ord_z)
      
      #   Feste Effekte - Mittlere Schaftkurve in der GesamtPopulation (PA) : E[y|x] = X*b_fix:.......
      b_fix 		= taper.model$par.lme$b_fix
      KOVb_fix    = taper.model$par.lme$KOVb_fix
      
      #   Kovarianz fuer die Random Effekte (fit.lme):................................................
      KOV_b 		= taper.model$par.lme$KOVb_rnd
      Z_KOVb_Zt_k = Z_k%*%KOV_b%*%t(Z_k)                                			# fix(Z_KOVb_Zt)
      
      #   Residuen Varianz:...........................................................................
      sig2_eps = taper.model$par.lme$sig2_eps
      dfRes    = taper.model$par.lme$dfRes
      
      R_k = diag(sig2_eps, ncol(Z_KOVb_Zt_k))
      
      #   Kovarianzmatrix der Beobachtungen (Sigma.i) :...............................................
      
      KOV_y_k		= Z_KOVb_Zt_k + R_k                       	#   SIGMA_k in V&C(1997)(6.2.3)
      KOVinv_y_k  = solve(KOV_y_k);                    		#   SIGMA_k^(-1)
      
      #   EBLUP - Posterior mean (b_k) aus Einhaengung (xm,ym) berechnen :.............................
      (EBLUP_b_k 	= KOV_b%*%t(Z_k)%*%KOVinv_y_k%*%(y_k - X_k%*%b_fix))
    }))
    rownames(re) <- unique(sek2$BID)
    re <- re[order(as.numeric(rownames(re)), decreasing = F),]
    head(re)
    re.all <- ranef(taper.model.all$fit.lme)
    re.all <- re.all[as.integer(row.names(re.all)) %in% unique(sek2$BID),]
    re.all <- re.all[order(as.numeric(rownames(re.all)), decreasing = F),]
    head(re.all)
    re.diff <- re.all - re # obs - pred
    head(re.diff)
    re.sd <- apply(re.diff, MARGIN = 2, FUN = sd)
    
    print(taper.model$fit.lme$sigma^2)
    ## bind results
    ll[[ ill ]] <- list(iter=iter,
                        share=s,
                        beta.all = fixef(taper.model.all$fit.lme),
                        beta.s = fixef(taper.model$fit.lme),
                        sd_eblub = re.sd,
                        sig2_eps = taper.model$fit.lme$sigma^2)
  }
}


pdf("J:/BuI/Persoenlich/Vonderach/TapeS/devel/estimate-ranef-new-tree-dd.pdf",
    paper = "a4r", width = 20, height = 10)
# fixed effect for submodels
fxd <- as.data.frame(t(sapply(1:length(ll), function(a){ll[[a]]$beta.s})))
fxd$i <- (sapply(1:length(ll), function(a){ll[[a]]$iter}))
fxd$s <- (sapply(1:length(ll), function(a){ll[[a]]$share}))
fxd$n <- as.integer((sapply(1:length(ll), function(a){ll[[a]]$share})) * nrow(bd) / 100)
fxd <- aggregate(fxd[, c("n", "X1", "X2", "X3", "X4", "X5")], by=list(s=fxd$s), FUN=mean)
head(fxd)
plot(X1 ~ s, type="n", data=fxd, ylim=range(fxd[, grepl("X", colnames(fxd))]), 
     xlab="share of data [%]", ylab="param-value fixef")
abline(h=fixef(taper.model.all$fit.lme), col="grey")
points(x=fxd$s, y=fxd$X1, type="o", col=2)
points(x=fxd$s, y=fxd$X2, type="o", col=3)
points(x=fxd$s, y=fxd$X3, type="o", col=4)
points(x=fxd$s, y=fxd$X4, type="o", col=5)
points(x=fxd$s, y=fxd$X5, type="o", col=6)
axis(3, at=fxd$s, labels = fxd$n)
legend("topright", legend=c("X1", "X2", "X3", "X4", "X5"), col=2:6, lty=1, pch=1)

# show sd of random effects
rnd <- as.data.frame(t(sapply(1:length(ll), function(a){ll[[a]]$sd_eblub})))
rnd$i <- (sapply(1:length(ll), function(a){ll[[a]]$iter}))
rnd$s <- (sapply(1:length(ll), function(a){ll[[a]]$share}))
rnd$n <- as.integer((sapply(1:length(ll), function(a){ll[[a]]$share})) * nrow(bd) / 100)
rnd <- aggregate(rnd[, c("n", "Z1", "Z2", "Z3", "Z4")], by=list(s=rnd$s), FUN=mean)
plot(Z1 ~ s, type="n", data=rnd, ylim=range(rnd[, grepl("Z", colnames(rnd))]), 
     xlab="share of data [%]", ylab="sd(diff(ranef(part), ranef(all))")
points(x=rnd$s, y=rnd$Z1, type="o", col=2)
points(x=rnd$s, y=rnd$Z2, type="o", col=3)
points(x=rnd$s, y=rnd$Z3, type="o", col=4)
points(x=rnd$s, y=rnd$Z4, type="o", col=5)
axis(3, at=rnd$s, labels = rnd$n)
legend("topright", legend=c("Z1", "Z2", "Z3", "Z4"), col=2:5, lty=1, pch=1)

## residual error
# # fixed effect for submodels
eps <- data.frame(eps=(sapply(1:length(ll), function(a){ll[[a]]$sig2_eps})))
eps$i <- (sapply(1:length(ll), function(a){ll[[a]]$iter}))
eps$s <- (sapply(1:length(ll), function(a){ll[[a]]$share}))
eps$n <- as.integer((sapply(1:length(ll), function(a){ll[[a]]$share})) * nrow(bd) / 100)
eps <- aggregate(eps[, c("n", "eps")], by=list(s=eps$s), FUN=mean)
head(eps)
plot(eps ~ s, type="n", data=eps, ylim=range(eps),
     xlab="share of data [%]", ylab="sd of residual error")
abline(h=taper.model.all$fit.lme$sigma^2, col="grey")
points(x=eps$s, y=eps$eps, type="o", col=2)
axis(3, at=eps$s, labels = eps$n)
legend("topright", legend=c("eps"), col=2, lty=1, pch=1)

dev.off()
