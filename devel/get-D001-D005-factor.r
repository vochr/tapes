f <- numeric(length = 8)
for(i in 1:8){
  # i <- 1
  xp <- c(0.01, 0.05)
  pl <- SKPar[[i]]
  X = TapeR:::XZ_BSPLINE.f(xp, pl$knt_x, pl$ord_x)
  yp_fix <- X %*% pl$b_fix
  print(f[i] <- yp_fix[1] / yp_fix[2])
}

prod((TapeR:::XZ_BSPLINE.f(c(0.01, 0.05), pl$knt_x, pl$ord_x) %*% pl$b_fix)^c(1,-1))
