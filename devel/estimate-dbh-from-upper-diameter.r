#' Prediction of Dbh from TLS-point clouds with occlusion
#' check the estimate of dbh from diameters above 3m and below crown base.

library(RSQLite)
library(TapeS)
library(dplyr)

## from help of TapeR_FIT_LME.f()
# load example data
rm(list=ls())
data(DxHx.df)

## get data ####
#### Models from Vfl-Data
p <- "J:/FVA-Projekte/P01697_BWI_TapeR"
db <- file.path(p, "Daten/Ergebnisse/SektionsDB.sqlite")
conn <- RSQLite::dbConnect(RSQLite::SQLite(), db)
# dbfile <- RSQLite::dbReadTable(conn, name = "herkunft")
# FID = 1 => "FVA-BW/Vfl-IBM"
sql1 <- paste0("SELECT * FROM baumdaten WHERE BA = 1 AND FID == 1 AND dataOK = 1;")
bd <- DBI::dbGetQuery(conn, sql1) # Baumdaten
sql2 <- paste0("SELECT sektionsdaten.BID, rel, sekD1, sekD2, sekH, Hoehe ",
               "FROM sektionsdaten ",
               "INNER JOIN baumdaten ",
               "ON sektionsdaten.BID=baumdaten.BID ",
               "WHERE BA = 1 AND baumdaten.FID = 1 AND dataOK = 1 AND useRec = 1;")
sek <- DBI::dbGetQuery(conn, sql2) # Sektionsdaten
RSQLite::dbDisconnect(conn) # closing

#### prepare data ####
sek <- sek %>%
  rowwise() %>%
  mutate(D = ifelse(is.na(sekD1) & is.na(sekD2), NA,  # mean diameters in cm
                    mean(c(sekD1, sekD2), na.rm = TRUE) / 10),
         H = ifelse(rel==1, (sekH / 100) * (Hoehe / 10), sekH / 100), # height in m
         Hrel = (H / (Hoehe / 10))) %>% # relative measurement height
  arrange(BID, Hrel) %>%
  dplyr::select(BID, D, H, Hrel) %>%
  as.data.frame()

## add relative measurement of tree top, i.e. sekH=H/H and sekD=0/D05=0
sek <- bd %>%
  mutate(D = 0, # in cm
         H = Hoehe / 10, # in cm
         Hrel = 1) %>%
  dplyr::select(BID, D, H, Hrel) %>%
  rbind(sek) %>%
  arrange(BID, Hrel)
sek <- merge(sek, bd[, c("BID", "Bhd", "Hoehe")], by = "BID")
sek$Dbh <- sek$Bhd / 10
sek$Ht <- sek$Hoehe / 10
sek$Bhd <- sek$Hoehe <- NULL
head(sek)

keepsek <- sek
sek <- keepsek

sek[sek$BID==sek$BID[1],]
length(unique(sek$BID)) # 6044 trees

cases <- expand.grid(n=seq(1, 5),  # number of observations visible
                     lower=c(2:5), # lower part of stem visible
                     upper=c(50, 60, 70)) # upper part of stem visible
cases$BIAS <- cases$RMSE <- cases$ntrees <- NA
for(i in 1:nrow(cases)){
  # i <- 1
  print(i)
  (n <- cases[i, "n"])
  (lwr <- cases[i, "lower"])
  (upr <- cases[i, "upper"])
  bd$dbh <- sapply(bd$BID, function(a){
    # a <- unique(sek$BID)[1]
    # print(a)
    # (df <- subset(sek, BID==a))
    (df <- subset(sek, BID==a & Hrel <= upr/100 & H >= lwr))
    if(nrow(df)>=n){
      df <- df[sample(1:nrow(df), size = n, replace = FALSE),]
      tpr <- tprTrees(spp=1, Dm = df$D, Hm = df$H, Ht = df$Ht[1])
      (Dbh <- Dbh(tpr))
    } else {
      Dbh <- NA
    }
    return(Dbh)
  })
  df <- bd[complete.cases(bd[, c("Bhd", "dbh")]), c("Bhd", "dbh")]
  cases[i, "ntrees"] <- nrow(df)
  cases[i, "RMSE"] <- Metrics::rmse(df$Bhd/10, df$dbh)
  cases[i, "BIAS"] <- Metrics::bias(df$Bhd/10, df$dbh)
}
write.csv(cases, file = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/estimate-dbh-from-upper-diameter.csv",
          row.names = FALSE)

View(cases)
pdf(file = "J:/BuI/Persoenlich/Vonderach/TapeS/devel/estimate-dbh-from-upper-diameter.pdf",
    paper="a4r", width = 20, height = 10)
plot(BIAS ~ RMSE, data=cases, pch=as.character(n), type="n", xlab="RMSE [cm]",
     ylab="BIAS [cm]",
     main="Dbh-estimation in Norway spruce", xlim=c(1.3, 2), las=1)
abline(h=0)
for(i in 1:5){
  # i <- 1
  tmp <- subset(cases, n==i)
  rect(xleft = min(tmp$RMSE), ybottom = min(tmp$BIAS), xright = max(tmp$RMSE),
       ytop = max(tmp$BIAS))
}

points(x=cases$RMSE, y=cases$BIAS, cex=2,
       pch=ifelse(cases$upper==50, 15, ifelse(cases$upper==60, 16, 17)),
       col=cases$lower)
points(x=cases$RMSE, y=cases$BIAS, pch=as.character(cases$n))
legend("topright", legend=c(1:5), title="nobs", pch=as.character(1:5))
legend("bottomright", legend=c(2:5), title="min.Ht [m]", pch=15,
       col=sort(unique(cases$lower)), pt.cex=2)
legend("topleft", legend=c(50, 60, 70), title="max.Ht [%]", pch=15:17,
       col=2, pt.cex=2)
dev.off()
