
library(microbenchmark)
library(TapeS)

## won't work anymore, since Function TapeS::Unvd has been updated.
# microbenchmark(unvd[1, 10, 10], TapeS:::Unvd(1, dm=10, cd=10))

# Unit: microseconds
# expr   min     lq    mean median    uq   max neval cld
# unvd[1, 10, 10]   1.2   1.30   1.875   1.40   1.7  16.1   100  a
# TapeS:::Unvd(1, dm = 10, cd = 10) 176.3 180.55 188.352 183.95 192.0 288.3   100   b

# very clear result
