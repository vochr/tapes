#' testing the crown_coarse_wood_expansion, as deduced from a comparison between
#' BDAT and TapeR
#'
#' Comparison is based on the calculated specific gravity [kg/m3].

# remotes::install_gitlab("vochr/tapes")
require(TapeS)
require(rBDAT)
require(mgcv)
load("data/crownExpansion.rdata")

#' hbc value from the mean estimated "crown base height", which is
#' deduced from the taper curve difference between BDAT and TapeR
hbc <- 0.7

for(spp in c(1, 3, 5, 8, 15, 17)){
  # spp <- 15
  dm <- 15:180
  h <- iS::estHeight(d13 = dm, sp = spp)
  n <- length(dm)
  #plot(h ~ dm, main=tprSpeciesCode(spp, "lang"))
  bdt <- rBDAT::buildTree(list(spp=spp, D1=dm, H2=50, H=h))
  t <- bdat_as_tprtrees(bdt)
  dhvol <- TapeS::tprVolume(t, bark = FALSE)
  HAkrovol <- TapeS::tprVolume(t, AB = list(A=hbc*Ht(t), B=7), iAB = c("H", "dob"),
                             bark = FALSE) # main axis crown volume
  stavol <- dhvol - HAkrovol # stem volume
  #plot(dhvol ~ dm, main=tprSpeciesCode(spp, "lang"))
  bm <- tprBiomass(t, component = c("sw", "agb"))
  #plot(bm[, "sw"] ~ dm, main=tprSpeciesCode(spp, "lang"))
  dh <- TapeS::nsur(spp = BaMap(type = 6)[rep(spp, n)], dbh = dm, ht = h,
                    sth = 0.01*h, d03 = D03(t), kl = hbc*h)
  plot(dh[, "sw"] ~ dm, main=tprSpeciesCode(spp, "lang"), type="l",
       ylab="solid wood [kg]", xlab="dbh [cm]")
  points(x=dm, y=bm[,"sw"], type="l", lty=2)
  legend("topleft", legend = c("Allometry", "Marklund"), lty=c(1,2))

  ## calculate specific gravity between volume and biomass
  grav <- dh[, "sw"] / dhvol # nsur / taper
  grav2 <- bm[,"sw"] / dhvol # marklund / taper

  bdatvol <- rBDAT::getVolume(tree = list(spp=spp, D1=dm, H=h), bark=FALSE)
  bdtgrav <- dh[, "sw"] / bdatvol # nsur / bdat
  bdtgrav2 <- bm[,"sw"] / bdatvol # marklund / bdat

  plot(grav ~ dm, main=tprSpeciesCode(spp, "lang"),
       ylim=c(range(c(grav, grav2, bdtgrav, bdtgrav2))),
       ylab = " specific gravity [kg/m3]", xlab="Dbh [cm]",
       type="l", lwd=2, col=2)
  points(x=dm, y=grav2, lty=3, type="l", lwd=2, col=2)
  points(x=dm, y=bdtgrav, lty=1, type="l", lwd=2, col=4)
  points(x=dm, y=bdtgrav2, lty=3, type="l", lwd=2, col=4)
  legend("bottomleft", legend=c("Marklund/TapeR", "NSUR/TapeR", "Marklund/BDAT", "NSUR/BDAT"),
         lty=c(3,1,3,1), col=c(2,2,4,4), lwd=2)

  ## correct TapeR-Volume by the crown expansion models
  if(spp %in% c(15, 17)){
    if(spp==15){
      g <- b15
    } else {
      g <- b17
    }
    AstDhAnteil <- predict(g, newdata=data.frame(D1=dm, H=h, H2=50), type="response")
    AstDh <- AstDhAnteil * (HAkrovol / (1 - AstDhAnteil))
    KroVol <- HAkrovol + AstDh
    plot(KroVol ~ dm, type="l", xlab="Bhd", ylab="volume [m3]")
    points(x=dm, y=AstDh, type="l", lty=2)
    points(x=dm, y=HAkrovol, type="l", lty=3)
    bdt$A <- hbc*bdt$H
    bdt$B <- 7
    points(x=dm, y=rBDAT::getVolume(bdt, iAB=c("H", "dob"), bark=FALSE),
           type="l", lty=4)
    legend("topleft", legend=c("crown", "main axis", "branches", "BDAT-crown"),
           lty = c(1,3,2,4))

    dhvolkorr <- stavol + KroVol
    grav <- dh[, "sw"] / dhvolkorr # nsur / taper
    grav2 <- bm[,"sw"] / dhvolkorr # marklund / taper

    plot(grav ~ dm, main=paste0(tprSpeciesCode(spp, "lang"), ", HBC=", hbc),
         ylim=c(range(c(grav, grav2, bdtgrav, bdtgrav2))),
         ylab = " specific gravity [kg/m3]", xlab="Dbh [cm]",
         type="l", lwd=2, col=2)
    points(x=dm, y=grav2, lty=3, type="l", lwd=2, col=2)
    points(x=dm, y=bdtgrav, lty=1, type="l", lwd=2, col=4)
    points(x=dm, y=bdtgrav2, lty=3, type="l", lwd=2, col=4)
    legend("bottomleft", legend=c("Marklund/TapeR", "NSUR/TapeR", "Marklund/BDAT", "NSUR/BDAT"),
           lty=c(3,1,3,1), col=c(2,2,4,4), lwd=2)
  }
}

# p <- "H:/BuI/Persoenlich/Vonderach/TapeS/devel"
# rmarkdown::render(file.path(p, "check_bdat_vol_biomass_large_trees.r"),
#                   output_format = "pdf_document",
#                   output_file = file.path(p, "check_bdat_vol_biomass_large_trees.pdf"))
