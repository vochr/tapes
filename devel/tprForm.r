#' @title getForm
#' @description gets the form
#' @param obj object of class 'tprTrees'
#' @param inv inventory for which information is required, if zero, the value of
#' the given tree is returned
#' @param cp cartesian product, i.e. apply all \code{Hx} to all trees, defaults
#' to TRUE : FIX: check
#' @param Rfn Rfn setting for residuals error matrix, defaults to
#' \code{list(fn="sig2")}, see \code{\link[TapeR]{resVar}}.
#' @param mono logical, defaults to true. If calibrated taper curve is
#' non-monotonic at stem base, an support diameter is added.
#' @return double bark thickness [cm]
#' @references none, yet
#' @import TapeR
#' @export
#' @examples
#' options("TapeS_Rfn") # "sig2", default in TapeS
#' tmp <- tprTrees()
#' Dm(tmp); Hm(tmp) # Dbh = D(Hx=1.3) = 30cm (measured)
#' Dbh(tmp) # estimated via EBLUP from taper curve
#' tprBark(tmp, Hx = c(1.3, 5)) # bark thickness corresponds to Dbh(tmp)
#' (d <- tprDiameter(tmp, Hx = c(1.3, 5), bark=TRUE)) ## predicted
#' bark(1, d[1], 1.3/30) # the same!
#' bark(1, d[2], 5/30) # the same!
#'
#' ## if using option TapeS_Rfn = list(fn="zero"), force taper curve through measurements
#' setTapeSoptions(Rfn = list(fn="zero"))
#' options()$TapeS_Rfn
#' tprBark(tmp, Hx = c(1.3, 5))
#' bark(1, 30, 1.3/30) # the same but different to above
#' bark(1, d[1], 1.3/30) # cf. above
#' bark(1, 28, 5/30) # the same but different to above
#' bark(1, d[2], 1.3/30) # cf. above

setGeneric("tprForm",
           function(obj, Hx, cp=TRUE, mono=TRUE, Rfn=NULL)
             standardGeneric("tprForm"))

#' @describeIn tprForm method for class 'tprTrees'
setMethod("tprForm", signature = "tprTrees",
          function(obj, inv, cp=TRUE, mono=TRUE, Rfn=NULL){
            SKspp <- BaMap(obj@spp, 1)
            if(is.null(Rfn)) Rfn <- getOption("TapeS_Rfn")
            if(isTRUE(cp)){
              ## apply all inv to all trees subsequently via inv[TRUE] => all inv
              use <- rep(TRUE, length(obj))
            } else {
              ## for each tree, use exactly one given Hx
              use <- seq(along=obj)
              # recycle inv to be at least of obj-length
              inv <- inv * rep(1, max(length(obj)*length(inv), length(inv)))
            }
            ncol <- ifelse(all(use==TRUE), length(inv), 1)
            res <- matrix(NA, nrow = length(obj@spp), ncol = ncol)
            for(i in seq(along=obj@spp)){
              if(isFALSE(obj@monotone[i]) & isTRUE(mono)){
                suppD <- attr(obj@monotone, "D005")[i] * SKPar[[ SKspp[i] ]]$q001
                suppH <- 0.01*obj@Ht[i]
              } else {
                suppD <- suppH <- numeric(0)
              }
              MwQ03 <- TapeR::BDATFormTarif()
              res[i, ] <- MwQ03
            }

            return(res[,, drop=TRUE])
          })
