require(TapeS)
obj <- tprTrees(spp = c(1L, 1L), Dm=c(30, 30), Hm=c(1.3, 1.3), Ht=c(27, 27), sHt = c(0, 3))
tprVolume(obj, AB = list(A=0, B=27), iAB = "h", interval = "none")
tprVolume(obj, AB = list(A=0, B=27), iAB = "h", interval = "confidence")
tprVolume(obj, AB = list(A=0, B=27), iAB = "h", interval = "prediction")
plot(obj, obs=TRUE)

TapeR::E_VOL_AB_HmDm_HT.f(Hm = 1.3, Dm = 30, mHt = 27, sHt = 0,
                          A = 0, B = 27, iDH = "H", par.lme = SKPar[[1]])[1:2]
TapeR::E_VOL_AB_HmDm_HT.f(Hm = 1.3, Dm = 30, mHt = 27, sHt = 3,
                          A = 0, B = 27, iDH = "H", par.lme = SKPar[[1]])[1:2]

# sHt = 0
Hx <- seq(0,27,1)
dl <- TapeR::E_DHx_HmDm_HT.f(Hx = Hx, Hm = 1.3, Dm = 30, mHt = 27, sHt = 0, par.lme = SKPar[[1]])
plot(x = Hx, y = dl$DHx, type="l", xlim=c(0, 33))
points(x=Hx, y=dl$CI_Mean[, 1], type="l", lty=2)
points(x=Hx, y=dl$CI_Mean[, 3], type="l", lty=2)

points(x=Hx, y=dl$CI_Pred[, 1], type="l", lty=3)
points(x=Hx, y=dl$CI_Pred[, 3], type="l", lty=3)

# sHt = 3
Hx <- seq(0,33,1)
dl <- TapeR::E_DHx_HmDm_HT.f(Hx = Hx, Hm = 1.3, Dm = 30, mHt = 27, sHt = 3, par.lme = SKPar[[1]])
plot(x = Hx, y = dl$DHx, type="l", xlim=c(0, 33))
points(x=Hx, y=dl$CI_Mean[, 1], type="l", lty=2)
points(x=Hx, y=dl$CI_Mean[, 3], type="l", lty=2)

points(x=Hx, y=dl$CI_Pred[, 1], type="l", lty=3)
points(x=Hx, y=dl$CI_Pred[, 3], type="l", lty=3)


##
sl <- 0.1
ab <- TapeS:::slfun(0, 27, sl=sl)
Hx <- ab$Hx
dl0 <- TapeR::E_DHx_HmDm_HT.f(Hx = Hx, Hm = 1.3, Dm = 30, mHt = 27, sHt = 0, par.lme = SKPar[[1]])
dl1 <- TapeR::E_DHx_HmDm_HT.f(Hx = Hx, Hm = 1.3, Dm = 30, mHt = 27, sHt = 3, par.lme = SKPar[[1]])
all.equal(dl0[[ "DHx" ]], dl1[[ "DHx" ]])
all.equal(dl0[[ "KOV_Mean" ]], dl1[[ "KOV_Mean" ]])
all.equal(dl0[[ "CI_Mean" ]], dl1[[ "CI_Mean" ]]) # only returned in intervals
all.equal(dl0[[ "MSE_Mean" ]], dl1[[ "MSE_Mean" ]])

## try to reproduce from intervals
## sHt = 0
dl <- dl0
D <- dl[[ "DHx" ]]
kovD <- dl[[ "KOV_Mean" ]]
(vol <- colSums(pi/4 * 1e-4 * (D^2 + diag(kovD)) * ab$L))

kovVol <- TapeS:::calcVCOVsekVol(D, kovD, ab$L) # vcov of segments

# Variance of total volume
# https://de.wikipedia.org/wiki/Varianz_(Stochastik)#Summen_und_Produkte
# Var(X + Y) = VAR(X) + VAR(Y) + 2*COV(X, Y)
varVol <- sum(diag(kovVol)) + 2*sum(kovVol[upper.tri(kovVol)])
c_alpha = qt(p=0.025, df=SKPar[[ 1 ]]$dfRes, ncp=0, lower.tail = FALSE)
lwr <- vol - c_alpha*sqrt(varVol)
upr <- vol + c_alpha*sqrt(varVol)
(c(lwr, vol, upr, varVol))
tprVolume(obj, AB = list(A=0, B=27, sl=sl), iAB = "h", interval = "confidence")
TapeR::E_VOL_AB_HmDm_HT.f(Hm = 1.3, Dm = 30, mHt = 27, sHt = 0,
                          A = 0, B = 27, iDH = "H", par.lme = SKPar[[1]])[1:2]


## sHt = 3
dl <- dl1
D <- dl[[ "DHx" ]]
c_alpha = qt(p=0.025, df=SKPar[[ 1 ]]$dfRes, ncp=0, lower.tail = FALSE)
MSE <- ((dl1$CI_Mean[, 3] - dl1$CI_Mean[, 2]) / c_alpha)^2 # Rückrechnung der MSE aus CI
(kovD <- dl[[ "KOV_Mean" ]])
corD <- cov2cor(kovD)
kovD <- propagate::cor2cov(corD, MSE)
diag(kovD)
vol <- colSums(pi/4 * 1e-4 * (D^2 + diag(kovD)) * ab$L)

kovVol <- TapeS:::calcVCOVsekVol(D, kovD, ab$L) # vcov of segments
# Variance of total volume
# https://de.wikipedia.org/wiki/Varianz_(Stochastik)#Summen_und_Produkte
# Var(X + Y) = VAR(X) + VAR(Y) + 2*COV(X, Y)
varVol <- sum(diag(kovVol)) + 2*sum(kovVol[upper.tri(kovVol)])
c_alpha = qt(p=0.025, df=SKPar[[ 1 ]]$dfRes, ncp=0, lower.tail = FALSE)
lwr <- vol - c_alpha*sqrt(varVol)
upr <- vol + c_alpha*sqrt(varVol)
(c(lwr, vol, upr, varVol))
tprVolume(obj, AB = list(A=0, B=27, sl=sl), iAB = "h", interval = "confidence")
TapeR::E_VOL_AB_HmDm_HT.f(Hm = 1.3, Dm = 30, mHt = 27, sHt = 3,
                          A = 0, B = 27, iDH = "H", par.lme = SKPar[[1]])[1:2]

## exact confidence bands from TapeR
sl <- 2
ab <- TapeS:::slfun(0, 27, sl=sl)
Hx <- ab$Hx
dl0 <- TapeR::E_DHx_HmDm_HT.f(Hx = Hx, Hm = 1.3, Dm = 30, mHt = 27, sHt = 0, par.lme = SKPar[[1]])
dl0p <- TapeR::E_DHx_HmDm_HT_CIdHt.f(Hx = Hx, Hm = 1.3, Dm = 30, mHt = 27, sHt = 0, par.lme = SKPar[[1]])
dl0$CI_Mean
dl0p #> quite similar

