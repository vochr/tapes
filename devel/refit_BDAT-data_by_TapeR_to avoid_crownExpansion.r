#' fitting a tapeR-Modell using the pre-smoothed BDAT base data (Bu_Abs*.txt)
#' with that, we might get a proper volume relation and a more stable
#' extrapolation.
#' Check this approach by deduced specific gravity.

rm(list=ls())
require(TapeR)
require(RSQLite)
require(dplyr)

p <- "H:/FVA-Projekte/P01697_BWI_TapeR"
f <- file.path(p, "Daten/Ergebnisse/SektionsDB.sqlite")

# for(ba in c(15L, 17L, 18L)){
for(ba in c(17L, 18L)){
  # ba <- 17L
  if(identical(ba, 15L)){
    ban <- "Bu"
  } else if(identical(ba, 17L)){
    ban <- "Ei"
  } else if(identical(ba, 18L)){
    ban <- "REi"
  }
  conn <- RSQLite::dbConnect(RSQLite::SQLite(), f)
  RSQLite::dbListTables(conn)
  tblF <- RSQLite::dbReadTable(conn, name = "herkunft")
  sql1 <- paste0("SELECT * FROM baumdaten WHERE BA = ", ba, " AND FID IN (2) AND dataOK = 1;")
  sql1 <- paste0("SELECT * FROM baumdaten WHERE BA = ", ba, " AND FID IN (2);")
  bd <- DBI::dbGetQuery(conn, sql1) # Baumdaten
  sql2 <- paste0("SELECT sektionsdaten.BID, rel, sekD1, sekD2, sekH, H, estKA ",
                 "FROM sektionsdaten ",
                 "INNER JOIN baumdaten ",
                 "ON sektionsdaten.BID=baumdaten.BID ",
                 "WHERE BA = ", ba, " AND baumdaten.FID IN (2) AND useRec = 1;")
  sek <- DBI::dbGetQuery(conn, sql2) # Sektionsdaten
  RSQLite::dbDisconnect(conn) # closing
  head(sek)
  length(unique(sek$BID)) == nrow(bd)

  sek <- sek %>%
    rowwise() %>%
    mutate(D = ifelse(is.na(sekD1) & is.na(sekD2), NA,  # mean diameters in cm
                      mean(c(sekD1, sekD2), na.rm = TRUE) / 10),
           Hoehe = H,
           H = sekH / 100, # height in m
           Hrel = (H / (Hoehe / 10))) %>% # relative measurement height
    arrange(BID, Hrel) %>%
    dplyr::select(BID, D, H, Hrel) %>%
    as.data.frame()

  ## add relative measurement of tree top, i.e. sekH=H/H and sekD=0/D05=0
  sek <- bd %>%
    mutate(D = 0, # in cm
           H = H / 10, # in cm
           Hrel = 1) %>%
    dplyr::select(BID, D, H, Hrel) %>%
    rbind(sek) %>%
    arrange(BID, Hrel)

  ## add below measurement taken from the BDAT-Model to avoid non monotone stem start
  for(b in unique(sek$BID)){
    # b <- sek$BID[1]
    print(b)
    ssek <- subset(sek, BID == b)
    d1 <- ssek$D[which.min(abs(ssek$H - 1.3))]
    h1 <- ssek$H[which.min(abs(ssek$H - 1.3))]
    d2 <- ssek$D[which.min(abs(ssek$Hrel - 0.3))]
    h2 <- ssek$H[which.min(abs(ssek$Hrel - 0.3))]
    h <- max(ssek$H)
    dx <- rBDAT::getDiameter(list(spp=ba, D1=d1, H1=h1, D2=d2, H2=h2, H=h, Hx=c(0.01*h, h1)))
    fq <- dx[1] / dx[2]
    sek <- rbind(sek, data.frame(BID=b, D=d1 * fq, H=0.01*h, Hrel=0.01))
  }
  sek <- sek[order(sek$BID, sek$H),]
  sek[sek$BID==sek$BID[1],]

  bd <- bd[order(bd$Bhd),]
  plot(1, type="n", xlim=c(0, 1), ylim=c(0, max(sek$D)),
       xlab="relative Höhe", ylab="Durchmesser", las=1)
  invisible(sapply(seq(1, nrow(bd), 10), function(a){
    ssek <- subset(sek, BID==bd[a, "BID"])
    points(x=ssek$Hrel, y= ssek$D, type="b")
  }))

  knt_x = c(0.0, 0.1, 0.75, 1.0)
  ord_x = 4 # B-Spline knots: fix effects; order (cubic = 4)
  knt_z = c(0.0, 0.1      , 1.0)
  ord_z = 4 # B-Spline knots: rnd effects

  #fit the model
  print("fitting the model")
  tpm <- try(TapeR_FIT_LME.f(Id = sek$BID, x = sek$Hrel, y = sek$D,
                             knt_x, ord_x, knt_z, ord_z,
                             IdKOVb = "pdSymm",
                             control = list(opt="nlminb",
                                            maxIter=500,
                                            msMaxIter = 500,
                                            msMaxEval = 500,
                                            msVerbose=TRUE)), silent = TRUE)
  save(tpm, file=file.path("devel", paste0("BDAT_reldata_", ba, "_tapeRmodel.rdata")))
}

