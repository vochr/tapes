#' @title diameter prediction E[d] for TapeR-object
#' @description Function evaluates TapeR taper curve models for given tree to
#' estimate diameter only. no height variance allowed, no intervals returned
#' @param xm relative height measured
#' @param ym diameter measured
#' @param xp relative height for which diameter is requested
#' @param par.lme taper curve model
#' @param R0 indicator to specify the residual error matrix for diameter
#' prediction
#' @details pure R function to benchmark against C++ version
#' \code{\link{lmeSKEBLUP}}
#' @return diameter at \code{xp}
#' @seealso \code{\link{tprDiameterCpp}} for a fast implementation of diameter
#' estimation if no confidence or prediction information are required and
#' \code{\link{tprDiameter}} for the more general function.
#' @export
#' @examples
#' obj <- tprTrees(spp=c(1L,3L),
#'                 Hm=list(c(1.3, 5), c(1.3, 5)),
#'                 Dm=list(c(27, 25), c(27, 25)),
#'                 Ht=c(27, 27))
#' i <- 1
#' Hx <- c(1.3, seq(2, Ht(obj[i]), 1))
#' xm=Hm(obj[i])/Ht(obj[i])
#' ym=Dm(obj[i])
#' xp=Hx/Ht(obj[i])
#' data(SKPar)
#' par.lme=SKPar[[ 1 ]]
#' R0=TRUE
#' lmeSKEBLUPR(xm, ym, xp, par.lme, R0)
#' lmeSKEBLUP(xm, ym, xp, par.lme, R0)
#'
#' \dontrun{
#' require(rbenchmark)
#' benchmark(lmeSKEBLUPR(xm, ym, xp, par.lme, R0),
#'           lmeSKEBLUP(xm, ym, xp, par.lme, R0), replications = 10000)[,1:4]
#' }

lmeSKEBLUPR <- function (xm, ym, xp, par.lme, R0){
  # no height variance allowed
  # no variances calculated
  # xm = Hm/mHt # relative height
  # ym = Dm # measured diameter in
  # xp = Hx[order(Hx)]/mHt # requested height
  xp = xp[xp <= 1]

  ## code from TapeR::SK_EBLUP_LME.f()
  EBLUP_b_k = MSE_Mean = MSE_Pred = CI_Mean = CI_Pred = NULL
  x_k = xm
  y_k = ym
  X_k = TapeR:::XZ_BSPLINE.f(x_k, par.lme$knt_x, par.lme$ord_x)
  Z_k = TapeR:::XZ_BSPLINE.f(x_k, par.lme$knt_z, par.lme$ord_z)
  b_fix = par.lme$b_fix
  KOVb_fix = par.lme$KOVb_fix
  KOV_b = par.lme$KOVb_rnd
  Z_KOVb_Zt_k = Z_k %*% KOV_b %*% t(Z_k)
  sig2_eps = par.lme$sig2_eps
  dfRes = par.lme$dfRes
  if (isTRUE(R0)) {
    R_k = diag(0, ncol(Z_KOVb_Zt_k))
  } else {
    R_k = diag(sig2_eps, ncol(Z_KOVb_Zt_k))
  }
  KOV_y_k = Z_KOVb_Zt_k + R_k
  KOVinv_y_k = solve(KOV_y_k)
  EBLUP_b_k = KOV_b %*% t(Z_k) %*% KOVinv_y_k %*% (y_k - X_k %*% b_fix)
  x_pre = xp
  x_pre = seq(0, 1, 0.01)
  X_kh = TapeR:::XZ_BSPLINE.f(x_pre, par.lme$knt_x, par.lme$ord_x)
  Z_kh = TapeR:::XZ_BSPLINE.f(x_pre, par.lme$knt_z, par.lme$ord_z)
  yp = EBLUP_y_kh = X_kh %*% b_fix + Z_kh %*% EBLUP_b_k
  if(FALSE){
    plot(yp ~ x_pre, type="n", ylim=c(0, 1))
    abline(v=par.lme$knt_x)
    for(i in 1:ncol(X_k)){
      # points(x=x_pre, y=X_kh[, i]*b_fix[i], type="l", lty=2)
      points(x=x_pre, y=X_kh[, i], type="l", lty=2)
    }
  }
  return(yp)
}
